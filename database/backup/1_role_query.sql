INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_ROLE_GROUP_SAVE', 'USER_ROLE_GROUP','USER ROLE GROUP SAVE','USER ROLE GROUP SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_ROLE_GROUP_UPDATE', 'USER_ROLE_GROUP','USER ROLE GROUP UPDATE','USER ROLE GROUP UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_ROLE_GROUP_DELETE', 'USER_ROLE_GROUP','USER ROLE GROUP DELETE','USER ROLE GROUP DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_ROLE_GROUP_VIEW', 'USER_ROLE_GROUP','USER ROLE GROUP VIEW','USER ROLE GROUP VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_ROLE_GROUP_VIEW_DELETED', 'USER_ROLE_GROUP','USER ROLE GROUP VIEW DELETED','USER ROLE GROUP VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_SAVE', 'USER','USER SAVE','USER SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_UPDATE', 'USER','USER UPDATE','USER UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_DELETE', 'USER','USER DELETE','USER DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_VIEW', 'USER','USER VIEW','USER VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('USER_VIEW_DELETED', 'USER','USER VIEW DELETED','USER VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_SAVE', 'CONTACT','CONTACT SAVE','CONTACT SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_UPDATE', 'CONTACT','CONTACT UPDATE','CONTACT UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_DELETE', 'CONTACT','CONTACT DELETE','CONTACT DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_VIEW', 'CONTACT','CONTACT VIEW','CONTACT VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_VIEW_DELETED', 'CONTACT','CONTACT VIEW DELETED','CONTACT VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('CONTACT_NOTES_DELETE', 'CONTACT','CONTACT NOTES DELETE','CONTACT NOTES DELETE');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('COUNTRY_SAVE', 'COUNTRY','COUNTRY SAVE','COUNTRY SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('COUNTRY_UPDATE', 'COUNTRY','COUNTRY UPDATE','COUNTRY UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('COUNTRY_DELETE', 'COUNTRY','COUNTRY DELETE','COUNTRY DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('COUNTRY_VIEW', 'COUNTRY','COUNTRY VIEW','COUNTRY VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('COUNTRY_VIEW_DELETED', 'COUNTRY','COUNTRY VIEW DELETED','COUNTRY VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('STATE_SAVE', 'STATE','STATE SAVE','STATE SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('STATE_UPDATE', 'STATE','STATE UPDATE','STATE UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('STATE_DELETE', 'STATE','STATE DELETE','STATE DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('STATE_VIEW', 'STATE','STATE VIEW','STATE VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('STATE_VIEW_DELETED', 'STATE','STATE VIEW DELETED','STATE VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('CITY_SAVE', 'CITY','CITY SAVE','CITY SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CITY_UPDATE', 'CITY','CITY UPDATE','CITY UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CITY_DELETE', 'CITY','CITY DELETE','CITY DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CITY_VIEW', 'CITY','CITY VIEW','CITY VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('CITY_VIEW_DELETED', 'CITY','CITY VIEW DELETED','CITY VIEW DELETED');

INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_SAVE', 'SALE','SALES SAVE','SALES SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_UPDATE', 'SALE','SALES UPDATE','SALES UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_DELETE', 'SALE','SALES DELETE','SALES DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_VIEW', 'SALE','SALES VIEW','SALES VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_VIEW_DELETED', 'SALE','SALES VIEW DELETED','SALES VIEW DELETED');


INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_ORDER_SAVE', 'SALE_ORDER','SALES ORDER SAVE','SALES ORDER SAVE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_ORDER_UPDATE', 'SALE_ORDER','SALES ORDER UPDATE','SALES ORDER UPDATE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_ORDER_DELETE', 'SALE_ORDER','SALES ORDER DELETE','SALES ORDER DELETE');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_ORDER_VIEW', 'SALE_ORDER','SALES ORDER VIEW','SALES ORDER VIEW');
INSERT INTO user_role(role, module_name, display_name, description) VALUES('SALES_ORDER_VIEW_DELETED', 'SALE_ORDER','SALES ORDER VIEW DELETED','SALES ORDER VIEW DELETED');

UPDATE user_role SET deleted = 0, IsActive = 1, createdby = 0, deletedby = 0, financialYear = 0, updatedBy = 0;


INSERT  INTO `user`(`id`,`deleted`,`IsActive`,`createdby`,`createdOn`,`deletedby`,`deletedOn`,`financialYear`,`updatedby`,`updatedOn`,`description`,`name`,`password`,`user_name`) VALUES 
(-1,0,1,-1,'2019-08-07 00:27:56',0,NULL,0,0,NULL,NULL,'admin','admin#123','admin');

INSERT  INTO `user`(`id`,`deleted`,`IsActive`,`createdby`,`createdOn`,`deletedby`,`deletedOn`,`financialYear`,`updatedby`,`updatedOn`,`description`,`name`,`password`,`user_name`) VALUES 
(-2,0,1,-1,'2019-08-07 00:27:56',0,NULL,0,0,NULL,NULL,'sa','sa#123','sa');

package com.aone.config.hibernateConvertors;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.math3.util.Precision;

@Converter
public class CustomDoubleConvertor implements AttributeConverter<Double, Double> {

	@Override
	public Double convertToDatabaseColumn(Double attribute) {
		return getDoubleValue(attribute);
	}

	@Override
	public Double convertToEntityAttribute(Double dbData) {
		return getDoubleValue(dbData);
	}
	
	private double getDoubleValue(Double value) {
		if(value == null) {
			return 0.00d;
		}
		value = Precision.round(value, 2);
		return value;
	}

}

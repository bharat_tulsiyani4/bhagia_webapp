package com.aone.config.security.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.aone.commons.CommonConstant;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.user.model.User;
import com.aone.module.user.service.IUserService;
import com.aone.module.userRole.model.UserRole;
import com.aone.module.userRole.service.IUserRoleService;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	
	private static Logger LOGGER = Logger.getLogger(CustomUserDetailsService.class);
	
	//private IUserService userService;
	private IUserRoleService userRoleService;
	private IUserService userService;
	
	public IUserService getUserService() {
		return userService;
	}

	@Autowired        
	@Qualifier(value="userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IUserRoleService getUserRoleService() {
		return userRoleService;
	}

	@Autowired
	@Qualifier(value="userRoleServiceImpl")
	public void setUserRoleService(IUserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}
	
	/*public IUserService getUserService() {
		return userService;
	}

	@Autowired
	@Qualifier(value = "userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}*/
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		String password = null;
		/*if(userName.equals("admin")) {
			password ="admin#123";
		} else if(userName.equals("sa")) {
			password ="sa@sa123";
		} else if(userName.startsWith(CommonConstant.USER)){
			User user = userService.getByUserName(userName);
			if (user == null) {
				throw new UsernameNotFoundException("Sorry! User not available");
			}
			password = user.getPassword();
		}*/
		User user = userService.getByUserName(userName);
		if (user == null) {
			throw new UsernameNotFoundException("Sorry! User not available");
		}
		password = user.getPassword();
		return new org.springframework.security.core.userdetails.User(userName, password, true,
				true, true, true, getGrantedAuthorities(userName, 0l));
	}

	private Collection<GrantedAuthority> getGrantedAuthorities(String userName, Long id) {
		try {
			List<UserRole> userRoleList = Collections.emptyList();
			if(userName.equals("sa") || userName.equals("admin")) {
				SearchData<UserRole> searchData = new SearchData<>();
				userRoleList = userRoleService.getActiveData(searchData);
			} else if(userName.startsWith(CommonConstant.USER)){				
				userRoleList.addAll(userRoleService.getByContactInfo(id,CommonConstant.USER));
			}
			
			LOGGER.info("=====================================================");
			LOGGER.info(userRoleList);
			LOGGER.info("=====================================================");
			List<GrantedAuthority> authorities = new ArrayList<>();
			for(UserRole userRole : userRoleList) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getRole()));
			}
			return authorities;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}
}

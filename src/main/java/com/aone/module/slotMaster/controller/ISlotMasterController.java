package com.aone.module.slotMaster.controller;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.module.slotMaster.model.SlotMaster;

public interface ISlotMasterController extends IBaseMasterController<SlotMaster, Long>{

}

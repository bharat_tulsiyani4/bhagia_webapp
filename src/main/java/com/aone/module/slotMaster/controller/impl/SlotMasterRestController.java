package com.aone.module.slotMaster.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.slotMaster.controller.ISlotMasterController;
import com.aone.module.slotMaster.model.SlotMaster;
import com.aone.module.slotMaster.service.ISlotMasterService;

@RestController
@RequestMapping("/rest/slotMaster")
//@SecurityClassRole(baseRoleName = SecurityRoleModule.SLOT_MASTER)
public class SlotMasterRestController extends AbstractBaseMasterRestController<SlotMaster, Long, ISlotMasterService> implements ISlotMasterController {
	
	private ISlotMasterService iSlotMasterService;
	
	@Override
	public ISlotMasterService getService() {
		return getSlotMasterService();
	}

	public ISlotMasterService getSlotMasterService() {
		return iSlotMasterService;
	}

	@Autowired        
	@Qualifier(value="slotMasterServiceImpl")
	public void setSlotMasterService(ISlotMasterService islotMasterService) {
		this.iSlotMasterService = islotMasterService;
	}
	
	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<IResponseData<SlotMaster>> getActiveData(@RequestBody RequestData<SearchData<SlotMaster>, Long> requestData) {
		return super.getActiveData(requestData);
	}
}

package com.aone.module.slotMaster.dao;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.slotMaster.model.SlotMaster;

public interface ISlotMasterDao extends IBaseMasterRepository<SlotMaster, Long>{

}

package com.aone.module.slotMaster.constant;

public class SlotMasterDbColPojoConstant {

	public static final  String NAME="name";
	public static final String DESCRIPTION ="description";
	
	private SlotMasterDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

package com.aone.module.slotMaster.service;

import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.slotMaster.model.SlotMaster;

public interface ISlotMasterService  extends IBaseMasterService<SlotMaster, Long>{

}

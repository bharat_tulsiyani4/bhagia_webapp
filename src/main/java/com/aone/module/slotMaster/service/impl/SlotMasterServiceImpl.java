package com.aone.module.slotMaster.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.slotMaster.dao.ISlotMasterDao;
import com.aone.module.slotMaster.model.SlotMaster;
import com.aone.module.slotMaster.service.ISlotMasterService;

@Lazy
@Service("slotMasterServiceImpl")
public class SlotMasterServiceImpl extends AbstractBaseMasterService<SlotMaster, Long, ISlotMasterDao> implements 
	ISlotMasterService {
	
	private ISlotMasterDao iSlotMasterDao;

	@Override
	public ISlotMasterDao getRepository() {
		return getSlotMasterDao();
	}
	
	public ISlotMasterDao getSlotMasterDao() {
		return iSlotMasterDao;
	}
	
	@Autowired
	@Qualifier(value = "slotMasterDaoImpl")
	public void setSlotMasterDao(ISlotMasterDao iSlotMasterDao) {
		this.iSlotMasterDao = iSlotMasterDao;
	}
	
	@Override
	public ResultCodeConstant validateData(SlotMaster slotMaster) {
		if (slotMaster == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		// check if engine type name already exist
		/*if (!getRepository().checkUniqueDataByField("name", slotMaster.getName(), "id", slotMaster.getId()).isEmpty()) {
			return ResultCodeConstant.SLOTMASTER_CODE_ALREADY_EXIST;
		}*/
		return ResultCodeConstant.SUCCESS;
	}
	
}

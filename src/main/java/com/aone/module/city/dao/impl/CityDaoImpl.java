package com.aone.module.city.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.module.city.dao.ICityDao;
import com.aone.module.city.model.City;

@Lazy
@Repository("cityDaoImpl")
public class CityDaoImpl extends AbstractBaseMasterRepository<City, Long> implements ICityDao {

	@Override
	public Class<City> getClazz() {
		return City.class;
	}
	
	@Override
	public boolean isPublicModule() {
		return true;
	}

	@Override
	public City getNewData() {
		City city = new City();
		city.setDefaultValueForObject();
		return city;
	}

}

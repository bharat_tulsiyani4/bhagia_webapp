package com.aone.module.city.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.city.constant.CityDbColPojoConstant;
import com.aone.module.state.model.State;

@Entity
@Table(name = "city_master")
public class City extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = CityDbColPojoConstant.NAME)
	private String name;
	
	@Column(name = CityDbColPojoConstant.STATE_ID)
	private long stateId;
	
	@Column(name = CityDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private State state;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.state == null)
			this.state = new State();
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "City [name=" + name + ", stateId=" + stateId + ", description=" + description + ", state=" + state
				+ "]";
	}

}

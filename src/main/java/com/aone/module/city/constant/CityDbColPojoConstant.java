package com.aone.module.city.constant;

public class CityDbColPojoConstant {

	public static final String NAME = "name";
	public static final String STATE_ID = "stateId";
	public static final String DESCRIPTION = "description";

	private CityDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

package com.aone.module.item.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.item.controller.IItemController;
import com.aone.module.item.model.Item;
import com.aone.module.item.service.IItemService;

@RestController
@RequestMapping("/rest/item")
//@SecurityClassRole(baseRoleName = SecurityRoleModule.ITEM)
public class ItemRestController extends AbstractBaseMasterRestController<Item, Long, IItemService> implements IItemController {
	
	private IItemService itemService;

	@Override
	@RequestMapping(value="/checkItemCode", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Boolean>> checkItemShortName(@RequestBody RequestData<Item,Long> requestData) {
		return getService().checkItemCode(requestData);
	}
	
	@Override
	public IItemService getService() {
		return getItemService();
	}

	public IItemService getItemService() {
		return itemService;
	}

	@Autowired        
	@Qualifier(value="itemServiceImpl")
	public void setItemService(IItemService itemService) {
		this.itemService = itemService;
	}

	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW"}, roleMatcher = RoleMatcher.ALL)
	public @ResponseBody ResponseEntity<IResponseData<Item>> getActiveData(@RequestBody RequestData<SearchData<Item>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().getActiveData(requestData);
	}

}

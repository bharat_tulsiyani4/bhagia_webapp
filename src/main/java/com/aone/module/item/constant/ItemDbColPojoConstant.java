package com.aone.module.item.constant;

public class ItemDbColPojoConstant {
	public static final String CODE = "code";
	public static final String NAME = "name";
	public static final String SIZE = "size";
	public static final String CATEGORY_ID = "category_id";
	public static final String LOCATION_ID = "location_id";
	public static final String RACK_ID = "rack_id";
	public static final String SLOT_ID = "slot_id";
	public static final String UOM_ID = "uom_id";
	public static final String HSN_CODE = "hsn_code";
	public static final String BRAND_ID = "brand_id";
	
	// purchase related fields
	public static final String PURCHASE_PRICE = "purchase_price";
	public static final String MRP_PURCHASE_PRICE = "mrp_purchase_price";
	public static final String PURCHASE_LEDGER_ID = "purchase_ledger_id";
	public static final String PURCHASE_GST_RATE_ID = "purchase_gst_rate_id";
	public static final String PURCHASE_DESCRIPTION = "purchase_description";
	public static final String SHOW_PURCHASE_INFORMATION = "show_purchase_information";
	
	// sale related fields
	public static final String SALE_PRICE = "sale_price";
	public static final String MRP_SALE_PRICE = "mrp_sale_price";
	public static final String SALE_LEDGER_ID = "sale_ledger_id";
	public static final String SALE_GST_RATE_ID = "sale_gst_rate_id";
	public static final String SALE_DESCRIPTION = "sale_description";
	public static final String SHOW_SALE_INFORMATION = "show_sale_information";
	
	public static final String STOCK = "stock";
	public static final String IMAGE_PATH = "image_path";
	public static final String RE_ORDER_LEVEL = "re_order_level";
	public static final String DESCRIPTION = "description";
	public static final String LAST_UPDATED_DATETIME = "last_updated_date_time";

	public static final String SYSTEM_INSERTED ="system_inserted";
	
	private ItemDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

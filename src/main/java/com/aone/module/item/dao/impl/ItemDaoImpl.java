package com.aone.module.item.dao.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.item.dao.IItemDao;
import com.aone.module.item.model.Item;

@Lazy
@Repository("itemDaoImpl")
public class ItemDaoImpl extends AbstractBaseMasterRepository<Item, Long> implements IItemDao {

	@Override
	public Class<Item> getClazz() {
		return Item.class;
	}

	@Override
	@Transactional(readOnly = false)
	public int updateItemStock(Item item) {
		StringBuilder queryBuilder = new StringBuilder(70);
		queryBuilder.append("UPDATE ");
		queryBuilder.append(getClazz().getName());
		queryBuilder.append(" SET stock = stock + :quantity, ");
		queryBuilder.append(" lastUpdatedDateTime = :lastUpdatedDateTime");
		queryBuilder.append(" where id = :id");
		Query query = getEntityManager().createQuery(queryBuilder.toString());
		query.setParameter("quantity", item.getQuantity());
		query.setParameter("lastUpdatedDateTime", item.getLastUpdatedDateTime());
		query.setParameter("id", item.getId());
		return query.executeUpdate();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Item> getActiveData(SearchData<Item> searchData) {
		Item item = searchData.getData();
		TypedQuery<Item> query = getQueryObject(searchData, false, false, getClazz());
		query.setParameter("deleted", false);
		if (item != null && item.getName() != null && !item.getName().isEmpty()) {
			String searchValue = "%" + item.getName() + "%";
			query.setParameter("itemname", searchValue);
		}
		if (item != null && item.getCode() != null && !item.getCode().isEmpty()) {
			query.setParameter("code", item.getCode());
		}
		if (item != null && item.getCategoryId() > 0) {
			long searchValue = item.getCategoryId();
			query.setParameter("categoryId", searchValue);
		}
		
		if (item != null && item.getBrandId() > 0) {
			long searchValue = item.getBrandId();
			query.setParameter("brandId", searchValue);
		}
		
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Long getActiveDataCount(SearchData<Item> searchData) {
		Item item = searchData.getData();
		TypedQuery<Long> query = getQueryObject(searchData, false, true, Long.class);
		query.setParameter("deleted", false);
		if (item != null && item.getName() != null && !item.getName().isEmpty()) {
			String searchValue = "%" + item.getName() + "%";
			query.setParameter("itemname", searchValue);
		}
		if (item != null && item.getCode() != null && !item.getCode().isEmpty()) {
			query.setParameter("code", item.getCode());
		}
		if (item != null && item.getCategoryId() > 0) {
			long searchValue = item.getCategoryId();
			query.setParameter("categoryId", searchValue);
		}
		
		if (item != null && item.getBrandId() > 0) {
			long searchValue = item.getBrandId();
			query.setParameter("brandId", searchValue);
		}
		
		return query.getSingleResult();
	}

	@Override
	protected <R> TypedQuery<R> getQueryObject(SearchData<Item> searchData, boolean isGetAll, boolean isCountQuery,
			Class<R> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);

		Item item = searchData.getData();

		if (isCountQuery) {
			queryBuilder.append(SELECT_COUNT_PREFIX);
		} else {
			queryBuilder.append(SELECT_PREFIX);
		}
		queryBuilder.append(getClazz().getName() + " e ");
		if (!isGetAll) {
			queryBuilder.append(" where ");
			if (isPublicModule()) {
				queryBuilder.append(" e.deleted = :deleted ");
				if (item != null && item.getName() != null && !item.getName().isEmpty()) {
					queryBuilder.append(" AND e.name like :itemname ");
				}
				if (item != null && item.getCode() != null && !item.getCode().isEmpty()) {
					queryBuilder.append(" AND e.code like :code ");
				}
			} else {
				/* queryBuilder.append(" e.createdBy = :createdBy AND "); */
				queryBuilder.append(" e.deleted = :deleted ");
				if (item != null && item.getName() != null && !item.getName().isEmpty()) {
					queryBuilder.append(" AND e.name like :itemname ");
				}
				if (item != null && item.getCode() != null && !item.getCode().isEmpty()) {
					queryBuilder.append(" AND e.code like :code ");
				}
				if (item != null && item.getCategoryId() > 0) {
					queryBuilder.append(" AND e.categoryId = :categoryId ");
				}
			}
		}
		if (searchData.getOrderingData() != null && !isCountQuery) {
			queryBuilder.append(" order by e." + searchData.getOrderingData().getColumn());
			if (searchData.getOrderingData().isDescending()) {
				queryBuilder.append(" desc ");
			}
		}
		TypedQuery<R> query = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if (searchData.getPaginationData() != null && !isCountQuery
				&& searchData.getPaginationData().getPageNumber() > 0) {
			query.setFirstResult(searchData.getPaginationData().getRowsPerPage()
					* (searchData.getPaginationData().getPageNumber() - 1));
			query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		return query;
	}

	@Override
	public List<Item> getItemData(List<Long> itemIdList) {
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append(SELECT_PREFIX);
		queryBuilder.append(getClazz().getName() + " e ");
		queryBuilder.append(" WHERE e.id IN :itemIdList order by e.name ");
		TypedQuery<Item> query = getEntityManager().createQuery(queryBuilder.toString(), getClazz());
		query.setParameter("itemIdList", itemIdList);
		return query.getResultList();
	}

	@Override
	public Item getNewData() {
		return new Item();
	}

}

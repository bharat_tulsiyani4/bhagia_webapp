package com.aone.module.item.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.item.model.Item;

public interface IItemDao extends IBaseMasterRepository<Item, Long>{

	public int updateItemStock(Item item);

	public List<Item> getItemData(List<Long> itemIdList);
	
}

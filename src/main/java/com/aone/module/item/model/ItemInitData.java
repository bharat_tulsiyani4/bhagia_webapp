package com.aone.module.item.model;

import java.util.List;

import com.aone.module.gstRate.model.GstRate;
import com.aone.module.itemCategory.model.ItemCategory;
import com.aone.module.location.model.Location;
import com.aone.module.rack.model.Rack;
import com.aone.module.slotMaster.model.SlotMaster;
import com.aone.module.uom.model.UOM;

public class ItemInitData {
	
	private List<ItemCategory> itemCategories;
	private List<GstRate> itemGstList;
	private List<UOM> itemUomList;
	private List<Location> itemLocationList;
	private List<SlotMaster> itemSlotList;
	private List<Rack> itemRackList;
	public List<ItemCategory> getItemCategories() {
		return itemCategories;
	}
	public void setItemCategories(List<ItemCategory> itemCategories) {
		this.itemCategories = itemCategories;
	}
	public List<GstRate> getItemGstList() {
		return itemGstList;
	}
	public void setItemGstList(List<GstRate> itemGstList) {
		this.itemGstList = itemGstList;
	}
	public List<UOM> getItemUomList() {
		return itemUomList;
	}
	public void setItemUomList(List<UOM> itemUomList) {
		this.itemUomList = itemUomList;
	}
	public List<Location> getItemLocationList() {
		return itemLocationList;
	}
	public void setItemLocationList(List<Location> itemLocationList) {
		this.itemLocationList = itemLocationList;
	}
	public List<SlotMaster> getItemSlotList() {
		return itemSlotList;
	}
	public void setItemSlotList(List<SlotMaster> itemSlotList) {
		this.itemSlotList = itemSlotList;
	}
	public List<Rack> getItemRackList() {
		return itemRackList;
	}
	public void setItemRackList(List<Rack> itemRackList) {
		this.itemRackList = itemRackList;
	}
	@Override
	public String toString() {
		return "ItemInitData [itemCategories=" + itemCategories + ", itemGstList=" + itemGstList + ", itemUomList="
				+ itemUomList + ", itemLocationList=" + itemLocationList + ", itemSlotList=" + itemSlotList
				+ ", itemRackList=" + itemRackList + "]";
	}
	
}

package com.aone.module.item.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.config.hibernateConvertors.CustomDoubleConvertor;
import com.aone.corelibrary.model.impl.AbstractBaseMasterEntity;
import com.aone.module.item.constant.ItemDbColPojoConstant;

@Entity
@Table(name = "item_master")
public class Item extends AbstractBaseMasterEntity<Long> {

	private static final long serialVersionUID = 2720214281416012405L;

	@Column(name = ItemDbColPojoConstant.CODE)
	private String code;
	
	@Column(name = ItemDbColPojoConstant.NAME)
	private String name;

	@Column(name = ItemDbColPojoConstant.SIZE)
	private String size;
	
	@Column(name = ItemDbColPojoConstant.CATEGORY_ID)
	private long categoryId;
	
	@Column(name = ItemDbColPojoConstant.LOCATION_ID)
	private long locationId;
	
	@Column(name = ItemDbColPojoConstant.RACK_ID)
	private long rackId;
	
	@Column(name = ItemDbColPojoConstant.SLOT_ID)
	private long slotId;
	
	@Column(name = ItemDbColPojoConstant.UOM_ID)
	private long uomId;
	
	@Column(name = ItemDbColPojoConstant.BRAND_ID)
	private long brandId;
	
	@Column(name = ItemDbColPojoConstant.HSN_CODE)
	private String hsnCode;
	
	// purchase related fields
	@Column(name = ItemDbColPojoConstant.PURCHASE_PRICE, precision=10, scale=2)
	@Convert(converter=CustomDoubleConvertor.class)
	private double purchasePrice;	
	@Column(name = ItemDbColPojoConstant.PURCHASE_LEDGER_ID)
	private long purchaseLedgerId;	
	@Column(name = ItemDbColPojoConstant.PURCHASE_GST_RATE_ID)
	private long purchaseGSTRateId;
	@Column(name = ItemDbColPojoConstant.PURCHASE_DESCRIPTION)
	private String purchaseDescription;
	@Column(name = ItemDbColPojoConstant.SHOW_PURCHASE_INFORMATION)
	private boolean showPurchaseInformation;
	
	// sales related fields
	@Column(name = ItemDbColPojoConstant.SALE_PRICE)
	@Convert(converter=CustomDoubleConvertor.class)
	private double salePrice;

	@Column(name = ItemDbColPojoConstant.SALE_GST_RATE_ID)
	private long saleGSTRateId;
	@Column(name = ItemDbColPojoConstant.SALE_DESCRIPTION)
	private String saleDescription;
	@Column(name = ItemDbColPojoConstant.SHOW_SALE_INFORMATION)
	private boolean showSaleInformation;
	
	@Column(name = ItemDbColPojoConstant.STOCK)
	@Convert(converter=CustomDoubleConvertor.class)
	private double stock;

	@Column(name = ItemDbColPojoConstant.IMAGE_PATH)
	private String imagePath;

	@Column(name = ItemDbColPojoConstant.RE_ORDER_LEVEL)
	@Convert(converter=CustomDoubleConvertor.class)
	private double reOrderLevel;

	@Column(name = ItemDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Column(name = ItemDbColPojoConstant.MRP_SALE_PRICE)
	@Convert(converter=CustomDoubleConvertor.class)
	private double mrpSalePrice;
	
	@Column(name = ItemDbColPojoConstant.MRP_PURCHASE_PRICE)
	@Convert(converter=CustomDoubleConvertor.class)
	private double mrpPurchasePrice;
	
	@Column(name = ItemDbColPojoConstant.SYSTEM_INSERTED)
	private String systemInserted;
	
	@Column(name = ItemDbColPojoConstant.LAST_UPDATED_DATETIME)
	private LocalDateTime lastUpdatedDateTime;
		
	@Transient
	private ItemInitData itemInitData;
	
	@Transient
	private long brandModelId;
	
	@Transient
	@Convert(converter=CustomDoubleConvertor.class)
	private double quantity;

	@Transient
	private long nQuantity;
	
	public String getSystemInserted() {
		return systemInserted;
	}

	public void setSystemInserted(String systemInserted) {
		this.systemInserted = systemInserted;
	}

	public double getMrpSalePrice() {
		return mrpSalePrice;
	}

	public void setMrpSalePrice(double mrpSalePrice) {
		this.mrpSalePrice = mrpSalePrice;
	}

	public double getMrpPurchasePrice() {
		return mrpPurchasePrice;
	}

	public void setMrpPurchasePrice(double mrpPurchasePrice) {
		this.mrpPurchasePrice = mrpPurchasePrice;
	}

	public ItemInitData getItemInitData() {
		return itemInitData;
	}

	public void setItemInitData(ItemInitData itemInitData) {
		this.itemInitData = itemInitData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getUomId() {
		return uomId;
	}

	public void setUomId(long uomId) {
		this.uomId = uomId;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public long getPurchaseLedgerId() {
		return purchaseLedgerId;
	}

	public void setPurchaseLedgerId(long purchaseLedgerId) {
		this.purchaseLedgerId = purchaseLedgerId;
	}

	public long getPurchaseGSTRateId() {
		return purchaseGSTRateId;
	}

	public void setPurchaseGSTRateId(long purchaseGSTRateId) {
		this.purchaseGSTRateId = purchaseGSTRateId;
	}
	
	public String getPurchaseDescription() {
		return purchaseDescription;
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchaseDescription = purchaseDescription;
	}

	public boolean isShowPurchaseInformation() {
		return showPurchaseInformation;
	}

	public void setShowPurchaseInformation(boolean showPurchaseInformation) {
		this.showPurchaseInformation = showPurchaseInformation;
	}

	public double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}

	public long getSaleGSTRateId() {
		return saleGSTRateId;
	}

	public void setSaleGSTRateId(long saleGSTRateId) {
		this.saleGSTRateId = saleGSTRateId;
	}

	public String getSaleDescription() {
		return saleDescription;
	}

	public void setSaleDescription(String saleDescription) {
		this.saleDescription = saleDescription;
	}

	public boolean isShowSaleInformation() {
		return showSaleInformation;
	}

	public void setShowSaleInformation(boolean showSaleInformation) {
		this.showSaleInformation = showSaleInformation;
	}

	public double getStock() {
		return stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public double getReOrderLevel() {
		return reOrderLevel;
	}

	public void setReOrderLevel(double reOrderLevel) {
		this.reOrderLevel = reOrderLevel;
	}

	public String getDescription() {
		return description;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public long getRackId() {
		return rackId;
	}

	public void setRackId(long rackId) {
		this.rackId = rackId;
	}

	public long getSlotId() {
		return slotId;
	}

	public void setSlotId(long slotId) {
		this.slotId = slotId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}

	public void setLastUpdatedDateTime(LocalDateTime lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}
	
	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public long getBrandModelId() {
		return brandModelId;
	}

	public void setBrandModelId(long brandModelId) {
		this.brandModelId = brandModelId;
	}

	
	public long getnQuantity() {
		return nQuantity;
	}

	public void setnQuantity(long nQuantity) {
		this.nQuantity = nQuantity;
	}

}

package com.aone.module.item.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.item.model.Item;

public interface IItemService  extends IBaseMasterService<Item, Long>{

	public ResponseEntity<IResponseData<Boolean>> checkItemCode(RequestData<Item,Long> requestData);

	public int updateItemStock(List<Item> itemList);

	public List<Item> getItemData(List<Long> itemIdList);
	
}

package com.aone.module.item.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.gstRate.model.GstRate;
import com.aone.module.gstRate.service.IGstRateService;
import com.aone.module.item.dao.IItemDao;
import com.aone.module.item.model.Item;
import com.aone.module.item.model.ItemInitData;
import com.aone.module.item.service.IItemService;
import com.aone.module.itemCategory.model.ItemCategory;
import com.aone.module.itemCategory.service.IItemCategoryService;
import com.aone.module.location.model.Location;
import com.aone.module.location.service.ILocationService;
import com.aone.module.rack.model.Rack;
import com.aone.module.rack.service.IRackService;
import com.aone.module.slotMaster.model.SlotMaster;
import com.aone.module.slotMaster.service.ISlotMasterService;
import com.aone.module.uom.model.UOM;
import com.aone.module.uom.service.IUOMService;

@Lazy
@Service("itemServiceImpl")
public class ItemServiceImpl extends AbstractBaseMasterService<Item, Long, IItemDao> implements IItemService {

	private IItemDao itemDao;

	private IItemCategoryService itemCategoryService;
	private IGstRateService gstRateService;
	private IUOMService uomService;
	private ILocationService locationService;
	private ISlotMasterService iSlotMasterService;
	private IRackService rackService;
	
	public IRackService getRackService() {
		return rackService;
	}

	@Autowired
	@Qualifier(value = "rackServiceImpl")
	public void setRackService(IRackService rackService) {
		this.rackService = rackService;
	}

	public ISlotMasterService getSlotMasterService() {
		return iSlotMasterService;
	}

	@Autowired
	@Qualifier(value = "slotMasterServiceImpl")
	public void setSlotMasterService(ISlotMasterService islotMasterService) {
		this.iSlotMasterService = islotMasterService;
	}

	public ILocationService getLocationService() {
		return locationService;
	}

	@Autowired
	@Qualifier(value = "locationServiceImpl")
	public void setLocationService(ILocationService locationService) {
		this.locationService = locationService;
	}

	public IUOMService getUomService() {
		return uomService;
	}

	@Autowired
	@Qualifier(value = "uomServiceImpl")
	public void setUomService(IUOMService uomService) {
		this.uomService = uomService;
	}

	public IGstRateService getGstRateService() {
		return gstRateService;
	}

	@Autowired
	@Qualifier(value = "gstRateServiceImpl")
	public void setGstRateService(IGstRateService gstRateService) {
		this.gstRateService = gstRateService;
	}

	public IItemCategoryService getItemCategoryService() {
		return itemCategoryService;
	}

	@Autowired
	@Qualifier(value = "itemCategoryServiceImpl")
	public void setItemCategoryService(IItemCategoryService itemCategoryService) {
		this.itemCategoryService = itemCategoryService;
	}

	@Override
	public IItemDao getRepository() {
		return getItemDao();
	}

	public IItemDao getItemDao() {
		return itemDao;
	}

	@Autowired
	@Qualifier(value = "itemDaoImpl")
	public void setItemDao(IItemDao itemDao) {
		this.itemDao = itemDao;
	}

	/*@Override
	public ResponseEntity<IResponseData<Boolean>> checkItemCode(RequestData<Item, Long> requestData) {
		IResponseData<Boolean> responseData = new ResponseData<>(!getRepository().checkUniqueDataByField("code", requestData.getData().getCode(), "id", requestData.getData().getId())
				.isEmpty(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}*/

	@Override
	public int updateItemStock(List<Item> itemList) {
		int count = 0;
		for (Item item : itemList) {
			item.setLastUpdatedDateTime(LocalDateTime.now());
			if (getRepository().updateItemStock(item) > 0) {
				count++;
			}
		}
		return count;
	}

	@Override
	public ResultCodeConstant validateData(Item item) {
		if (item == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}

		/*// validate name
		if (item.getCode() == null || item.getCode().isEmpty()) {
			return ResultCodeConstant.ITEM_CODE_NOT_FOUND;
		}
		// check if category name already exist
		if (!getRepository().checkUniqueDataByField("code", item.getName(), "id", item.getId()).isEmpty()) {
			return ResultCodeConstant.ITEM_CODE_ALREADY_EXIST;
		}

		if (item.getCategoryId() == 0) {
			return ResultCodeConstant.ITEM_CATEGORY_REQURIED;
		}

		if (item.getUomId() == 0) {
			return ResultCodeConstant.ITEM_UOM_REQURIED;
		}

		if (item.getPurchasePrice() < 0) {
			return ResultCodeConstant.ITEM_PURCHASE_GT_ZERO;
		}

		if (item.getSalePrice() < 0) {
			return ResultCodeConstant.ITEM_SALE_GT_ZERO;
		}

		if (item.getReOrderLevel() < 0) {
			return ResultCodeConstant.ITEM_ROL_GT_ZERO;
		}*/
		return ResultCodeConstant.SUCCESS;
	}

	@Override
	public ResponseEntity<IResponseData<Item>> save(RequestData<Item, Long> requestData) {
		Item item = requestData.getData();

		// validate contact data
		ResultCodeConstant resultCodeConstant = validateData(item);
		if (ResultCodeConstant.SUCCESS != resultCodeConstant) {
			IResponseData<Item> responseData = new ResponseData<>(requestData.getData(), Collections.emptyList(), resultCodeConstant);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}

		// save contact detail first
		item = getRepository().save(item);

		// return the response object to client
		IResponseData<Item> responseData = new ResponseData<>(item, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<Item>> update(RequestData<Item, Long> requestData) {

		// save purchase master
		Item item = getRepository().update(requestData.getData());

		IResponseData<Item> responseData = new ResponseData<>(item, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<Item>> getById(RequestData<Item, Long> requestData) {
		ResponseEntity<IResponseData<Item>> responseEntity = super.getById(requestData);
		IResponseData<Item> responseData = responseEntity.getBody();

		if (responseData.getResponseCode() != ResultCodeConstant.SUCCESS.getResultCode()) {
			return responseEntity;
		}

		Item item = responseData.getData();

		// fetch ItemCategory list
		SearchData<ItemCategory> searchData = new SearchData<>();
		List<ItemCategory> itemCategoryList = itemCategoryService.getActiveData(searchData);

		// fetch ItemGstRate list
		SearchData<GstRate> searchDataGstRate = new SearchData<>();
		List<GstRate> gstRateList = gstRateService.getActiveData(searchDataGstRate);

		// fetch UOM list
		SearchData<UOM> searchDataUom = new SearchData<>();
		List<UOM> uomList = uomService.getActiveData(searchDataUom);

		// fetch fuelTypeSearchData list
		SearchData<Rack> rackDetailsReqData = new SearchData<>();
		List<Rack> rackDetailsList = rackService.getActiveData(rackDetailsReqData);

		// fetch fuelTypeSearchData list
		SearchData<SlotMaster> slotMasterDetailsReqData = new SearchData<>();
		List<SlotMaster> slotMasterDetailsList = iSlotMasterService.getActiveData(slotMasterDetailsReqData);

		// fetch fuelTypeSearchData list
		SearchData<Location> locationDetailsReqData = new SearchData<>();
		List<Location> locationDetailsList = locationService.getActiveData(locationDetailsReqData);

		ItemInitData itemInitData = new ItemInitData();
		itemInitData.setItemCategories(itemCategoryList);
		itemInitData.setItemGstList(gstRateList);
		itemInitData.setItemUomList(uomList);
		itemInitData.setItemLocationList(locationDetailsList);
		itemInitData.setItemSlotList(slotMasterDetailsList);
		itemInitData.setItemRackList(rackDetailsList);

		item.setItemInitData(itemInitData);
		return responseEntity;
	}

	@Override
	public List<Item> getItemData(List<Long> itemIdList) {
		if(itemIdList == null || itemIdList.isEmpty()) {
			return new ArrayList<>(); 
		}
		return itemDao.getItemData(itemIdList);
	}

	@Override
	public ResponseEntity<IResponseData<Boolean>> checkItemCode(RequestData<Item, Long> requestData) {
		return null;
	}
}
package com.aone.module.salesOrder.constant;

public class SalesOrderDbColPojoConstant {
	
	public static final String SYNCED_IN_DESKTOP="synced_in_desktop";
	public static final String SALES_ORDER_ID="sales_order_id";
	public static final String DESKTOP_SALES_ORDER_ID = "desktop_sales_order_id";
	public static final String BILL_DATE="bill_date";
	public static final String CONTACT_ID="contact_id";
	public static final String ORDER_DATE="order_date";
	public static final String SUB_TOTAL="sub_total";
	public static final String GRAND_TOTAL="grand_total";
	public static final String DESCRIPTION ="description";
	
	private SalesOrderDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

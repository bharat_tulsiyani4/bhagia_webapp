package com.aone.module.salesOrder.dao.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.dao.IContactDao;
import com.aone.module.contact.model.Contact;
import com.aone.module.salesOrder.dao.ISalesOrderDao;
import com.aone.module.salesOrder.model.SalesOrder;

@Lazy
@Repository("salesOrderDaoImpl")
public class SalesOrderDaoImpl extends AbstractBaseMasterRepository<SalesOrder, Long> implements ISalesOrderDao {

	private IContactDao contactDao;
	public static final int COUNT_QUERY = 1;
	public static final int TOTAL_QUERY = 2;
	public static final int DATA_QUERY = 3;
	public static final int BASE_TOTAL_QUERY = 4;
	
	public IContactDao getContactDao() {
		return contactDao;
	}

	@Autowired
	@Qualifier(value = "contactDaoImpl")
	public void setContactDao(IContactDao contactDao) {
		this.contactDao = contactDao;
	}
	
	@Override
	public Class<SalesOrder> getClazz() {
		return SalesOrder.class;
	}
	

	@Override
	public List<SalesOrder> getActiveData(SearchData<SalesOrder> searchData) {
		TypedQuery<SalesOrder> typedQuery = prepareQueryObject(DATA_QUERY, searchData, getClazz());
		return typedQuery.getResultList();
	}
	
	@Override
	public Long getActiveDataCount(SearchData<SalesOrder> searchData) {
		TypedQuery<Long> typedQuery = prepareQueryObject(COUNT_QUERY, searchData, Long.class);
		return typedQuery.getSingleResult();
	}
	
	private <A> TypedQuery<A> prepareQueryObject(int queryType, SearchData<SalesOrder> searchData, Class<A> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);
		if(queryType == COUNT_QUERY) {
			queryBuilder.append("select count (e.id) from ");
		} else if (queryType == TOTAL_QUERY) {
			queryBuilder.append("select sum (e.subTotal) from ");
		} else if (queryType == BASE_TOTAL_QUERY) {
			queryBuilder.append("select sum (e.basePriceNetTotal) from ");
		} else {
			queryBuilder.append("select e from ");
		}
		queryBuilder.append(getClazz().getName() + " e ");
		
		SalesOrder salesOrder = searchData.getData();
		boolean isCompanyNameSearch = false;
		LocalDateTime orderDate = null;
		if(salesOrder != null) {
			if(!StringUtils.isBlank(salesOrder.getFullName())) {
				queryBuilder.append(" INNER JOIN ").append(Contact.class.getName()).append(" c ON c.id = e.contactId ");
				queryBuilder.append(" where e.deleted = :deleted ");
			} else {
				queryBuilder.append(" where e.deleted = :deleted ");
			}
			
		} else {
			queryBuilder.append(" where e.deleted = :deleted ");
		}
		
		long createdBy = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		/*if(createdBy > 0) {
			queryBuilder.append(" and e.createdBy = :createdBy ");
		}*/
		
		if(!StringUtils.isBlank(salesOrder.getSyncedInDesktop())) {
			queryBuilder.append(" and e.syncedInDesktop = :syncedInDesktop ");
		}
		
		if(!StringUtils.isBlank(salesOrder.getFullName())) {
			queryBuilder.append(" and c.fullName = :fullName ");
		}
		
		/*if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			queryBuilder.append(" and orderDate >= :startDate ");
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			queryBuilder.append(" and orderDate <= :endDate ");
		}*/
		
		if(searchData.getOrderingData() != null && queryType == DATA_QUERY) {
			queryBuilder.append(" order by e."+searchData.getOrderingData().getColumn());
			if(searchData.getOrderingData().isDescending()) {
				queryBuilder.append(" desc ");
			}
		}
		
		TypedQuery<A> typedQuery = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if(searchData.getPaginationData() != null && queryType == DATA_QUERY) {
			typedQuery.setFirstResult(searchData.getPaginationData().getRowsPerPage() * (searchData.getPaginationData().getPageNumber() - 1));
			typedQuery.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		typedQuery.setParameter("deleted", false);
		/*if(createdBy > 0) {
			typedQuery.setParameter("createdBy", createdBy);
		}*/
	/*	if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			typedQuery.setParameter("startDate", searchData.getData().getStartDate());
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			typedQuery.setParameter("endDate", searchData.getData().getEndDate());
		}*/
		
		if(!StringUtils.isBlank(salesOrder.getFullName())) {
			typedQuery.setParameter("fullName", salesOrder.getFullName());
		}
		
		if(!StringUtils.isBlank(salesOrder.getSyncedInDesktop())) {
			typedQuery.setParameter("syncedInDesktop", salesOrder.getSyncedInDesktop());
		}
		return typedQuery;
	}

	@Override
	public int updateSalesOrderDesktopSyncData(Long orderId, String syncedInDesktop) {
		if (orderId == null) {
			return 0;
		}
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append("UPDATE ");
		queryBuilder.append(getClazz().getName());
		queryBuilder.append(" SET syncedInDesktop = '" + syncedInDesktop+ "'");
		queryBuilder.append(" where id = "+ orderId);
		
		return getEntityManager().createQuery(queryBuilder.toString()).executeUpdate();
	}

	@Override
	public SalesOrder getNewData() {
		SalesOrder salesOrder = new SalesOrder();
		salesOrder.setDefaultValueForObject();
		return salesOrder;
	}

}

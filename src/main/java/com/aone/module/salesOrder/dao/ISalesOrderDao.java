package com.aone.module.salesOrder.dao;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.salesOrder.model.SalesOrder;

public interface ISalesOrderDao extends IBaseMasterRepository<SalesOrder, Long>{

	public int updateSalesOrderDesktopSyncData(Long orderIds, String syncedInDesktop);

}

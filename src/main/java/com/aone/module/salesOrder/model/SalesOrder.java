package com.aone.module.salesOrder.model;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.contact.model.Contact;
import com.aone.module.salesOrder.constant.SalesOrderDbColPojoConstant;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

@Entity
@Table(name = "sales_order")
public class SalesOrder extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = 7895104325035517069L;
	
	@Column(name = SalesOrderDbColPojoConstant.SYNCED_IN_DESKTOP)
	private String syncedInDesktop;
	
	@Column(name = SalesOrderDbColPojoConstant.SALES_ORDER_ID)
	private long salesOrderId;
	
	@Column(name = SalesOrderDbColPojoConstant.DESKTOP_SALES_ORDER_ID)
	private long desktopSalesOrderId;
	
	@Column(name = SalesOrderDbColPojoConstant.BILL_DATE)
	private LocalDateTime orderDate;
	
	@Column(name = SalesOrderDbColPojoConstant.CONTACT_ID)
	private long contactId;
	
	@Column(name = SalesOrderDbColPojoConstant.SUB_TOTAL)
	private double subTotal;
	
	@Column(name = SalesOrderDbColPojoConstant.GRAND_TOTAL)
	private double grandTotal;
	
	@Column(name = SalesOrderDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private Contact contact; 
	
	@Transient
	private List<SalesOrderDetails> salesOrderDetailsList;
	
	@Transient
	private LocalDateTime startDate;
	
	@Transient
	private LocalDateTime endDate;
	
	@Transient
	private SalesOrderInitData salesOrderInitData;
	
	@Transient
	private String fullName;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.contact == null)
			this.contact = new Contact();
		if(this.salesOrderDetailsList == null)
			this.salesOrderDetailsList = Collections.emptyList();
		if(this.salesOrderInitData == null)
			this.salesOrderInitData = new SalesOrderInitData();
		return true;
	}
	
	public String getSyncedInDesktop() {
		return syncedInDesktop;
	}

	public void setSyncedInDesktop(String syncedInDesktop) {
		this.syncedInDesktop = syncedInDesktop;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public long getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(long salesOrderId) {
		this.salesOrderId = salesOrderId;
	}
	
	public long getDesktopSalesOrderId() {
		return desktopSalesOrderId;
	}

	public void setDesktopSalesOrderId(long desktopSalesOrderId) {
		this.desktopSalesOrderId = desktopSalesOrderId;
	}

	public LocalDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}
	
	public SalesOrderInitData getSalesOrderInitData() {
		return salesOrderInitData;
	}

	public void setSalesOrderInitData(SalesOrderInitData salesOrderInitData) {
		this.salesOrderInitData = salesOrderInitData;
	}

	public List<SalesOrderDetails> getSalesOrderDetailsList() {
		return salesOrderDetailsList;
	}

	public void setSalesOrderDetailsList(List<SalesOrderDetails> salesOrderDetailsList) {
		this.salesOrderDetailsList = salesOrderDetailsList;
	}

	@Override
	public String toString() {
		return "SalesOrder [syncedInDesktop=" + syncedInDesktop + ", salesOrderId=" + salesOrderId
				+ ", desktopSalesOrderId=" + desktopSalesOrderId + ", orderDate=" + orderDate + ", contactId="
				+ contactId + ", subTotal=" + subTotal + ", grandTotal=" + grandTotal + ", description=" + description
				+ ", contact=" + contact + ", salesOrderDetailsList=" + salesOrderDetailsList + ", startDate="
				+ startDate + ", endDate=" + endDate + ", salesOrderInitData=" + salesOrderInitData + ", fullName="
				+ fullName + "]";
	}
	
}

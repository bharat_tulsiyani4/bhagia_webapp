package com.aone.module.salesOrder.model;

import java.util.List;

public class SalesOrderListData {

	private List<SalesOrder> salesOrderList;
	private double totalAmount;
	private double totalBaseAmount;
	public List<SalesOrder> getSalesOrderList() {
		return salesOrderList;
	}
	public void setSalesOrderList(List<SalesOrder> salesOrderList) {
		this.salesOrderList = salesOrderList;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalBaseAmount() {
		return totalBaseAmount;
	}
	public void setTotalBaseAmount(double totalBaseAmount) {
		this.totalBaseAmount = totalBaseAmount;
	}
	@Override
	public String toString() {
		return "SalesOrderListData [salesOrderList=" + salesOrderList + ", totalAmount=" + totalAmount
				+ ", totalBaseAmount=" + totalBaseAmount + "]";
	}
	
}

package com.aone.module.salesOrder.service;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrder.model.SalesOrderIdModel;
import com.aone.module.salesOrder.model.SalesOrderListData;

public interface ISalesOrderService extends IBaseMasterService<SalesOrder, Long> {

	public ResponseEntity<IResponseData<SalesOrderListData>> getActiveDataWithTotal(
			RequestData<SearchData<SalesOrder>, Long> requestData);

	public ResponseEntity<IResponseData<SalesOrderIdModel>> updateSyncDesktopStatus(RequestData<SalesOrderIdModel, Long> requestData);
}

package com.aone.module.salesOrder.service.impl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.commons.exceptions.AuthenticationFailedException;
import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.contact.model.Contact;
import com.aone.module.contact.service.IContactService;
import com.aone.module.salesOrder.dao.ISalesOrderDao;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrder.model.SalesOrderIdModel;
import com.aone.module.salesOrder.model.SalesOrderInitData;
import com.aone.module.salesOrder.model.SalesOrderListData;
import com.aone.module.salesOrder.service.ISalesOrderService;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;
import com.aone.module.salesOrderDetails.service.ISalesOrderDetailsService;

@Lazy
@Service("salesOrderServiceImpl")
public class SalesOrderServiceImpl extends AbstractBaseMasterService<SalesOrder, Long, ISalesOrderDao> implements
		ISalesOrderService {
	
	private ISalesOrderDao salesOrderDao;
	private ISalesOrderDetailsService salesOrderDetailsService;
	private IContactService contactService;
	
	public static final String SYNCED_YES = "yes";
	public static final String SYNCED_NO = "no";
	
	@Override
	public boolean isSetDefaultRequired() {
		return true;
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrder>> save(RequestData<SalesOrder, Long> requestData) {
		
		// save SalesOrder master
		SalesOrder salesOrder=requestData.getData();
        salesOrder.setActive(false);
        salesOrder.setSyncedInDesktop(SYNCED_NO);
        salesOrder = getRepository().save(salesOrder);
					
		long agentId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(agentId == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}
		// save sales details
		List<SalesOrderDetails> salesOrderDetailsList = requestData.getData().getSalesOrderDetailsList();

		if(salesOrderDetailsList!=null) {			
			for(SalesOrderDetails salesOrderDetails : salesOrderDetailsList) {
				salesOrderDetails.setSalesOrderId(salesOrder.getId());
				salesOrderDetails.setDesktopSalesOrderId(salesOrder.getDesktopSalesOrderId());
				salesOrderDetails.setTransactionDate(LocalDateTime.now());
				salesOrderDetails.setContactId(salesOrder.getContactId());
			}
		}
		
		// save sales details list
		getSalesOrderDetailsService().saveOrUpdateAll(salesOrderDetailsList);
		
		IResponseData<SalesOrder> responseData = new ResponseData<>(salesOrder, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrder>> update(RequestData<SalesOrder, Long> requestData) {
		
		SalesOrder salesOrder = requestData.getData();
		long agentId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(agentId == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}
		
		// update sales master
		SalesOrder dataTemp  = getRepository().getById(salesOrder.getId());
		salesOrder.setCreatedBy(dataTemp.getCreatedBy());
		salesOrder.setCreatedOn(dataTemp.getCreatedOn());
		
		salesOrder = getRepository().update(salesOrder);
		
		int count = getSalesOrderDetailsService().deleteSalesOrderDetailsBySalesOrderId(salesOrder.getId());
		
		List<SalesOrderDetails> salesOrderDetailsList = requestData.getData().getSalesOrderDetailsList();

		if(salesOrderDetailsList!=null) {
			for(SalesOrderDetails salesOrderDetails : salesOrderDetailsList) {
				salesOrderDetails.setSalesOrderId(salesOrder.getId());
				salesOrderDetails.setDesktopSalesOrderId(salesOrder.getDesktopSalesOrderId());
				salesOrderDetails.setTransactionDate(LocalDateTime.now());
				salesOrderDetails.setContactId(salesOrder.getContactId());
			}
			
		}		
		// save sales details list
		getSalesOrderDetailsService().saveOrUpdateAll(salesOrderDetailsList);
		
		IResponseData<SalesOrder> responseData = new ResponseData<>(salesOrder, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrderListData>> getActiveDataWithTotal(
			RequestData<SearchData<SalesOrder>, Long> requestData) {
		
		SalesOrderListData salesOrderListData = new SalesOrderListData();
		salesOrderListData.setSalesOrderList(this.getActiveData(requestData).getBody().getDataList());
		
		IResponseData<SalesOrderListData> responseData = new ResponseData<>(salesOrderListData, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setTotalRecords(getRepository().getActiveDataCount(requestData.getData()));
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrder>> getActiveData(RequestData<SearchData<SalesOrder>, Long> requestData) {
		
		// save sales master
		List<SalesOrder> salesList = getRepository().getActiveData(requestData.getData());
		
		List<SalesOrderDetails> salesOrderDetailsList = null;
		// sales sales items details
		Contact contact = new Contact();
		for(SalesOrder salesOrder : salesList) {
			// set item details
			salesOrderDetailsList = getSalesOrderDetailsService().getSalesOrderDetailsBySalesOrderId(salesOrder.getId());
			salesOrder.setSalesOrderDetailsList(salesOrderDetailsList);
			
			//set contact details
			contact.setId(salesOrder.getContactId());
			salesOrder.setContact(contactService.getById(contact));
			
			// set deault value for object
			salesOrder.setDefaultValueForObject();
		}
		
		IResponseData<SalesOrder> responseData = new ResponseData<>(getRepository().getNewData(), salesList, ResultCodeConstant.SUCCESS);
		responseData.setTotalRecords(getRepository().getActiveDataCount(requestData.getData()));
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrder>> getById(RequestData<SalesOrder, Long> requestData) {
		SalesOrder salesOrder = new SalesOrder();
		salesOrder.setId(requestData.getId());
		salesOrder = getById(salesOrder);
		IResponseData<SalesOrder> responseData = new ResponseData<>(salesOrder, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public SalesOrder getById(SalesOrder salesOrder) {
		if(salesOrder == null) {
			throw new RuntimeException("Sales order object can not be null");
		}
		return this.getById(salesOrder.getId());
	}
	
	@Override
	public SalesOrder getById(Long salesOrderId) {
		
		SalesOrder salesOrder = salesOrderDao.getById(salesOrderId);
		// set sales item details
		salesOrder.setSalesOrderDetailsList(getSalesOrderDetailsService().getSalesOrderDetailsBySalesOrderId(salesOrder.getId()));
		
		//set contact details
		Contact contact = new Contact();
		contact.setId(salesOrder.getContactId());
		salesOrder.setContact(contactService.getById(contact));
		
		// set deault value for object
		salesOrder.setDefaultValueForObject();
		
		SearchData<Contact> contactSearchObject = new SearchData<>();
		List<Contact> contactsList = contactService.getActiveData(contactSearchObject);
		
		SalesOrderInitData salesOrderInitData = new SalesOrderInitData();
		salesOrderInitData.setContactList(contactsList);
	
		salesOrder.setSalesOrderInitData(salesOrderInitData);
		
		return salesOrder;
	}
	
	@Override
	public ISalesOrderDao getRepository() {
		return getSalesOrderDao();
	}
	
	public ISalesOrderDao getSalesOrderDao() {
		return salesOrderDao;
	}

	@Autowired
	@Qualifier(value = "salesOrderDaoImpl")
	public void setSalesOrderDao(ISalesOrderDao salesOrderDao) {
		this.salesOrderDao = salesOrderDao;
	}
	
	public ISalesOrderDetailsService getSalesOrderDetailsService() {
		return salesOrderDetailsService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsServiceImpl")
	public void setSalesOrderDetailsService(ISalesOrderDetailsService salesOrderDetailsService) {
		this.salesOrderDetailsService = salesOrderDetailsService;
	}

	/**
	 * @return the contactService
	 */
	public IContactService getContactService() {
		return contactService;
	}

	/**
	 * @param contactService the contactService to set
	 */
	@Autowired        
	@Qualifier(value="contactServiceImpl")
	public void setContactService(IContactService contactService) {
		this.contactService = contactService;
	}

	@Override
	public ResponseEntity<IResponseData<SalesOrderIdModel>> updateSyncDesktopStatus(RequestData<SalesOrderIdModel, Long> requestData) {
		
		SalesOrderIdModel salesOrderIdModel = requestData.getData();
		if(salesOrderIdModel == null) {
			IResponseData<SalesOrderIdModel> responseData = new ResponseData<>(new SalesOrderIdModel(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		for(Long salesId : salesOrderIdModel.getIdArray()) {
			int count = salesOrderDao.updateSalesOrderDesktopSyncData(salesId, salesOrderIdModel.getSyncStatus());
		}
		
		IResponseData<SalesOrderIdModel> responseData = new ResponseData<>(new SalesOrderIdModel(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

}
package com.aone.module.salesOrder.controller;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.sales.model.SalesIdModel;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrder.model.SalesOrderIdModel;
import com.aone.module.salesOrder.model.SalesOrderListData;

public interface ISalesOrderController extends IBaseMasterController<SalesOrder, Long>{

	ResponseEntity<IResponseData<SalesOrderListData>> getActiveDataWithTotal(
			RequestData<SearchData<SalesOrder>, Long> requestData);

	ResponseEntity<IResponseData<SalesOrderIdModel>> updateSyncDesktopStatus(RequestData<SalesOrderIdModel, Long> requestData);

}

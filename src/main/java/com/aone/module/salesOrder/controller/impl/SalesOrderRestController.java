package com.aone.module.salesOrder.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.salesOrder.controller.ISalesOrderController;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrder.model.SalesOrderIdModel;
import com.aone.module.salesOrder.model.SalesOrderListData;
import com.aone.module.salesOrder.service.ISalesOrderService;

@RestController
@RequestMapping(value = {"/rest/salesOrder", "/web/rest/salesOrder"})
@SecurityClassRole(baseRoleName = SecurityRoleModule.SALES_ORDER)
public class SalesOrderRestController extends AbstractBaseMasterRestController<SalesOrder, Long, ISalesOrderService> implements ISalesOrderController {
	
	private ISalesOrderService salesOrderService;
	
	@Override
	public ISalesOrderService getService() {
		return getSalesOrderService();
	}

	public ISalesOrderService getSalesOrderService() {
		return salesOrderService;
	}

	@Autowired        
	@Qualifier(value="salesOrderServiceImpl")
	public void setSalesOrderService(ISalesOrderService salesOrderService) {
		this.salesOrderService = salesOrderService;
	}
	
	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<SalesOrder>> getActiveData(@RequestBody RequestData<SearchData<SalesOrder>, Long> requestData) {
		return super.getActiveData(requestData);
	}
	
	@Override
	@RequestMapping(value="/getActiveDataWithTotal", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<SalesOrderListData>> getActiveDataWithTotal(@RequestBody RequestData<SearchData<SalesOrder>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return salesOrderService.getActiveDataWithTotal(requestData);
	}
	
	@Override
	@RequestMapping(value="/updateSyncDesktopStatus", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
//	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ALL)
	public @ResponseBody ResponseEntity<IResponseData<SalesOrderIdModel>> updateSyncDesktopStatus(@RequestBody RequestData<SalesOrderIdModel,Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().updateSyncDesktopStatus(requestData);
	}

}

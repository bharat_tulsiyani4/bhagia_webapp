package com.aone.module.userRoleGroupMapping.constant;

public class UserRoleGroupMappingDbColPojoConstant {

	public static final String CONTACT_ID="contact_id";
	public static final String USER_ROLE_GROUP_ID="user_role_group_id";
	public static final String DESCRIPTION ="description";
	public static final String MODULE_NAME ="module_name";
	
	private UserRoleGroupMappingDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

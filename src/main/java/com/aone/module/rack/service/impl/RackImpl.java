package com.aone.module.rack.service.impl;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.rack.dao.IRackDao;
import com.aone.module.rack.model.Rack;
import com.aone.module.rack.service.IRackService;

@Lazy
@Service("rackServiceImpl")
public class RackImpl extends AbstractBaseMasterService<Rack, Long, IRackDao>
		implements IRackService {
	
	private IRackDao rackDao;

	@Override
	public IRackDao getRepository() {
		return getRackDao();
	}

	public IRackDao getRackDao() {
		return rackDao;
	}

	@Autowired
	@Qualifier(value = "rackDaoImpl")
	public void setRackDao(IRackDao rackDao) {
		this.rackDao = rackDao;
	}
	
	@Override
	public ResponseEntity<IResponseData<Boolean>> checkRackName(RequestData<Rack, Long> requestData) {
		IResponseData<Boolean> responseData = new ResponseData<>(!getRepository().checkUniqueDataByField("name", requestData.getData().getName(), "id", requestData.getData().getId()).isEmpty(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant validateData(Rack rack) {
		if(rack == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		
		// validate name
		/*if(rack.getName() == null || rack.getName().isEmpty()) {
			return ResultCodeConstant.RACK_NAME_NOT_FOUND;
		}
		// check if category name already exist
		if(!getRepository().checkUniqueDataByField("name", rack.getName(), "id", rack.getId()).isEmpty()) {
			return ResultCodeConstant.RACK_ALREADY_EXIST;
		}*/
		return ResultCodeConstant.SUCCESS;
	}
	
}
package com.aone.module.rack.service;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.rack.model.Rack;

public interface IRackService  extends IBaseMasterService<Rack , Long>{

	public ResponseEntity<IResponseData<Boolean>> checkRackName(RequestData<Rack, Long> requestData);
}

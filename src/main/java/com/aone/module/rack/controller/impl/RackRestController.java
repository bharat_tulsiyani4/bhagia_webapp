package com.aone.module.rack.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.rack.controller.IRackController;
import com.aone.module.rack.model.Rack;
import com.aone.module.rack.service.IRackService;

@RestController
@RequestMapping("/rest/rack")
//@SecurityClassRole(baseRoleName = SecurityRoleModule.RACK)
public class RackRestController extends AbstractBaseMasterRestController<Rack , Long, IRackService> implements IRackController {
	
	private IRackService rackService;
	
	@Override
	public IRackService getService() {
		return getRackService();
	}

	public IRackService getRackService() {
		return rackService;
	}

	@Autowired        
	@Qualifier(value="rackServiceImpl")
	public void setRackService(IRackService rackService) {
		this.rackService = rackService;
	}
	
	@Override
	@RequestMapping(value="/checkRackName", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Boolean>> checkRackName(@RequestBody RequestData<Rack,Long> requestData) {
		return getService().checkRackName(requestData);
	}

	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<IResponseData<Rack>> getActiveData(@RequestBody RequestData<SearchData<Rack>, Long> requestData) {
		return super.getActiveData(requestData);
	}
	
}

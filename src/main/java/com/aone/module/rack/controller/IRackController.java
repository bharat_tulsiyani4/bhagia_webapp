package com.aone.module.rack.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.module.rack.model.Rack;

public interface IRackController extends IBaseMasterController<Rack , Long>{

	public @ResponseBody ResponseEntity<IResponseData<Boolean>> checkRackName(@RequestBody RequestData<Rack,Long> requestData);
}

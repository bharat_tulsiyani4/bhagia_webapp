package com.aone.module.rack.dao;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.rack.model.Rack;

public interface IRackDao extends IBaseMasterRepository<Rack , Long>{

}

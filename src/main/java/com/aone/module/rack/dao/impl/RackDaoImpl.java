package com.aone.module.rack.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.rack.dao.IRackDao;
import com.aone.module.rack.model.Rack;

@Lazy
@Repository("rackDaoImpl")
public class RackDaoImpl extends AbstractBaseMasterRepository<Rack , Long> implements IRackDao {

	@Override
	public Class<Rack> getClazz() {
		return Rack.class;
	}
	@Override
	@Transactional(readOnly = true)
	public List<Rack> getActiveData(SearchData<Rack> searchData) {
		TypedQuery<Rack> query = getQueryObject(searchData, false, false, getClazz());
		query.setParameter("deleted", false);
		if (searchData != null) {
			Rack rack = searchData.getData();
			if (rack != null) {
				if (rack.getName() != null && !rack.getName().isEmpty()) {
					query.setParameter("name", "%" + rack.getName() + "%");
				}
			}
		}
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Long getActiveDataCount(SearchData<Rack> searchData) {
		if (!isPublicModule()) {
		
		}
		TypedQuery<Long> query = getQueryObject(searchData, false, true, Long.class);
		query.setParameter("deleted", false);
		if (searchData != null) {
			Rack rack = searchData.getData();
			if (rack != null) {
				if (rack.getName() != null && !rack.getName().isEmpty()) {
					query.setParameter("name", "%" + rack.getName() + "%");
				}
			}

		}
		return query.getSingleResult();
	}

	@Override
	protected <R> TypedQuery<R> getQueryObject(SearchData<Rack> searchData, boolean isGetAll, boolean isCountQuery,
			Class<R> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);

		if (isCountQuery) {
			queryBuilder.append(SELECT_COUNT_PREFIX);
		} else {
			queryBuilder.append(SELECT_PREFIX);
		}
		queryBuilder.append(getClazz().getName() + " e Where 1=1  ");
		if (!isGetAll) {
			if (isPublicModule()) {
				queryBuilder.append("  AND e.deleted = :deleted ");
			} else {
				/* queryBuilder.append(" e.createdBy = :createdBy AND "); */
				if (searchData != null) {
					Rack rack = searchData.getData();
					if (rack != null) {
						if (rack.getName() != null && !rack.getName().isEmpty()) {
							queryBuilder.append(" AND e.name like :name ");
						}					}
					queryBuilder.append(" AND e.deleted = :deleted ");
				}
			}
		}
		if (searchData != null) {
			if (searchData.getOrderingData() != null && !isCountQuery) {
				queryBuilder.append(" order by e." + searchData.getOrderingData().getColumn());
				if (searchData.getOrderingData().isDescending()) {
					queryBuilder.append(" desc ");
				}
			}
		}
		TypedQuery<R> query = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if (searchData != null) {
			if (searchData.getPaginationData() != null && !isCountQuery) {
				query.setFirstResult(searchData.getPaginationData().getRowsPerPage()
						* (searchData.getPaginationData().getPageNumber() - 1));
				query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
			}
		}
		return query;
	}
	@Override
	public Rack getNewData() {
		return new Rack();
	}
	
}

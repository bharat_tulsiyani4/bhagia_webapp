package com.aone.module.rack.constant;

public class RackDbColPojoConstant {

	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";

	private RackDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

package com.aone.module.country.constant;

public class CountryDbColPojoConstant {

	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";

	private CountryDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

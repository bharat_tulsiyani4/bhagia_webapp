package com.aone.module.country.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.country.constant.CountryDbColPojoConstant;

@Entity
@Table(name = "country_master")
public class Country extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = CountryDbColPojoConstant.NAME)
	private String name;
		
	@Column(name = CountryDbColPojoConstant.DESCRIPTION)
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Country [name=" + name + ", description=" + description + "]";
	}

}

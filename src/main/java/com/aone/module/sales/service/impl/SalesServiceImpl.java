package com.aone.module.sales.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.commons.exceptions.AuthenticationFailedException;
import com.aone.commons.exceptions.OperationFailedException;
import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.contact.model.Contact;
import com.aone.module.contact.service.IContactService;
import com.aone.module.sales.dao.ISalesDao;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportQtyByAgent;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;
import com.aone.module.sales.model.SalesIdModel;
import com.aone.module.sales.model.SalesInitData;
import com.aone.module.sales.model.SalesListData;
import com.aone.module.sales.service.ISalesService;
import com.aone.module.salesDetails.model.SalesDetails;
import com.aone.module.salesDetails.service.ISalesDetailsService;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;
import com.aone.module.salesOrderDetails.service.ISalesOrderDetailsService;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;
import com.aone.module.salesOrderDetailsQtyUpdateLog.service.ISalesOrderDetailsQtyUpdateLogService;

@Lazy
@Service("salesServiceImpl")
public class SalesServiceImpl extends AbstractBaseMasterService<Sales, Long, ISalesDao> implements
		ISalesService {
	
	public static final String SYNCED_YES = "yes";
	public static final String SYNCED_NO = "no";
	public static final String ORDER_TYPE_TEMP = "Temp";
	public static final String ORDER_TYPE_PERMANENT = "Permanent";
	
	private ISalesDao salesDao;
	private ISalesDetailsService salesDetailsService;
	private ISalesOrderDetailsService salesOrderDetailsService;
	private IContactService contactService;
	private ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService;
	
	public ISalesOrderDetailsQtyUpdateLogService getSalesOrderDetailsQtyUpdateLogService() {
		return salesOrderDetailsQtyUpdateLogService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsQtyUpdateLogServiceImpl")
	public void setSalesOrderDetailsQtyUpdateLogService(ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService) {
		this.salesOrderDetailsQtyUpdateLogService = salesOrderDetailsQtyUpdateLogService;
	}
	
	
	@Override
	public boolean isSetDefaultRequired() {
		return true;
	}
	
	@Override
	public ResponseEntity<IResponseData<Sales>> save(RequestData<Sales, Long> requestData) {
		// save sales master
        Sales sale=requestData.getData();
        
        ResultCodeConstant resultCodeConstant = this.save(sale);
		
		IResponseData<Sales> responseData = new ResponseData<>(sale, getRepository().getNewDataList(), resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant save(Sales sale) {
		sale.setActive(false);
        sale.setSyncedInDesktop(SYNCED_NO);
        Sales sales = getRepository().save(sale);
					
		long agentId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(agentId == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}
		// save sales details
		updateRemainingQtyStockDetails(sales);
		
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResponseEntity<IResponseData<Sales>> update(RequestData<Sales, Long> requestData) {
		
		Sales sales = requestData.getData();
		long agentId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(agentId == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}
		
		// update sales master
		Sales dataTemp  = getRepository().getById(sales.getId());
		sales.setCreatedBy(dataTemp.getCreatedBy());
		sales.setCreatedOn(dataTemp.getCreatedOn());
		sales.setSyncedInDesktop(SYNCED_NO);
		
		sales = getRepository().update(sales);
		
		// revert all remaining qty details
		revertRemainingQtyDetailsBySalesId(sales);
		
		int count = getSalesDetailsService().deleteSalesDetailsBySalesId(sales.getId());
		
		// save sales details
		updateRemainingQtyStockDetails(sales);
		
		IResponseData<Sales> responseData = new ResponseData<>(sales, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesListData>> getActiveDataWithTotal(
			RequestData<SearchData<Sales>, Long> requestData) {
		
		SalesListData salesListData = new SalesListData();
		salesListData.setSales(this.getActiveData(requestData).getBody().getDataList());
		Double totalAmount = salesDao.getTotalOfData(requestData.getData());
		if(totalAmount != null) {
			salesListData.setTotalAmount(totalAmount);
		}
		
		Double baseTotalAmount = salesDao.getBaseTotalOfData(requestData.getData());
		if(baseTotalAmount != null) {
			salesListData.setTotalBaseAmount(baseTotalAmount);
		}
		
		IResponseData<SalesListData> responseData = new ResponseData<>(salesListData, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setTotalRecords(getRepository().getActiveDataCount(requestData.getData()));
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<Sales>> getActiveData(RequestData<SearchData<Sales>, Long> requestData) {
		
		// delete all sales bill where contactId is zero
		salesDao.deleteSalesByContactId(0);
		
		// save sales master
		List<Sales> salesList = getRepository().getActiveData(requestData.getData());
		
		List<SalesDetails> salesDetailsList = null;
		// sales sales items details
		Contact contact = new Contact();
		for(Sales sales : salesList) {
			// set item details
			salesDetailsList = getSalesDetailsService().getSalesDetailsBySalesId(sales.getId());
			sales.setSalesDetailsList(salesDetailsList);
			
			//set contact details
			contact.setId(sales.getContactId());
			sales.setContact(contactService.getById(contact));
			
			// set deault value for object
			sales.setDefaultValueForObject();
		}
		
		IResponseData<Sales> responseData = new ResponseData<>(getRepository().getNewData(), salesList, ResultCodeConstant.SUCCESS);
		responseData.setTotalRecords(getRepository().getActiveDataCount(requestData.getData()));
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<Sales>> getById(RequestData<Sales, Long> requestData) {
		Sales sales = new Sales();
		sales.setId(requestData.getId());
		sales = getById(sales);
		IResponseData<Sales> responseData = new ResponseData<>(sales, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public Sales getById(Sales sales) {
		if(sales == null) {
			throw new RuntimeException("Sales object can not be null");
		}
		return this.getById(sales.getId());
	}
	
	@Override
	public Sales getById(Long salesId) {
		
		if(salesId == null || salesId == 0) {
			Sales newSalesObj = new Sales();
			newSalesObj.setDefaultValueForObject();
			newSalesObj.setSyncedInDesktop(SYNCED_NO);
			newSalesObj.setOrderDate(LocalDateTime.now());
			newSalesObj.setLatitude("");
			newSalesObj.setLongtitude("");
			newSalesObj.setLocation("");
			newSalesObj.setDescription("");
			newSalesObj = salesDao.save(newSalesObj);
			return newSalesObj;
		}
		
		Sales sales = salesDao.getById(salesId);
		// set sales item details
		sales.setSalesDetailsList(getSalesDetailsService().getSalesDetailsBySalesId(sales.getId()));
		
		//set contact details
		Contact contact = new Contact();
		contact.setId(sales.getContactId());
		sales.setContact(contactService.getById(contact));
		
		// set deault value for object
		sales.setDefaultValueForObject();
		
		SearchData<Contact> contactSearchObject = new SearchData<>();
		contactSearchObject.setData(contact);
		List<Contact> contactsList = contactService.getCurrentOrderExistData(contactSearchObject);
		
		SalesInitData salesInitData = new SalesInitData();
		salesInitData.setContactList(contactsList);
	
		sales.setSalesInitData(salesInitData);
		
		return sales;
	}
	
	@Override
	public ISalesDao getRepository() {
		return getSalesDao();
	}

	public ISalesDao getSalesDao() {
		return salesDao;
	}

	@Autowired
	@Qualifier(value = "salesDaoImpl")
	public void setSalesDao(ISalesDao salesDao) {
		this.salesDao = salesDao;
	}

	public ISalesDetailsService getSalesDetailsService() {
		return salesDetailsService;
	}

	@Autowired        
	@Qualifier(value="salesDetailsServiceImpl")
	public void setSalesDetailsService(ISalesDetailsService salesDetailsService) {
		this.salesDetailsService = salesDetailsService;
	}

	/**
	 * @return the contactService
	 */
	public IContactService getContactService() {
		return contactService;
	}

	/**
	 * @param contactService the contactService to set
	 */
	@Autowired        
	@Qualifier(value="contactServiceImpl")
	public void setContactService(IContactService contactService) {
		this.contactService = contactService;
	}
	
	public ISalesOrderDetailsService getSalesOrderDetailsService() {
		return salesOrderDetailsService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsServiceImpl")
	public void setSalesOrderDetailsService(ISalesOrderDetailsService salesOrderDetailsService) {
		this.salesOrderDetailsService = salesOrderDetailsService;
	}

	@Override
	public ResponseEntity<IResponseData<ReportSale>> getOrderSaleData(
			RequestData<SearchData<Sales>, Long> requestData) {
		List<ReportSale> topSaleList = salesDao.getOrderSaleData(requestData.getData());
		IResponseData<ReportSale> responseData = new ResponseData<>(new ReportSale(), topSaleList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		responseData.setTotalRecords(salesDao.getActiveDataCount(requestData.getData()));
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<ReportQty>> getOrderQtyData(RequestData<SearchData<Sales>, Long> requestData) {
		List<ReportQty> topQuantityList = salesDao.getOrderQtyData(requestData.getData());
		IResponseData<ReportQty> responseData = new ResponseData<>(new ReportQty(), topQuantityList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<ReportQtyByAgent>> getOrderQtyByAgentData(
			RequestData<SearchData<Sales>, Long> requestData) {
		List<ReportQtyByAgent> topQuantityByAgentList = salesDao.getOrderQtyByAgentData(requestData.getData());
		IResponseData<ReportQtyByAgent> responseData = new ResponseData<>(new ReportQtyByAgent(), topQuantityByAgentList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<SalesIdModel>> updateSyncDesktopStatus(RequestData<SalesIdModel, Long> requestData) {
		
		SalesIdModel salesIdModel = requestData.getData();
		if(salesIdModel == null) {
			IResponseData<SalesIdModel> responseData = new ResponseData<>(new SalesIdModel(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		for(Long salesId : salesIdModel.getIdArray()) {
			int count = salesDao.updateSalesDesktopSyncData(salesId, salesIdModel.getSyncStatus());
		}
		
		IResponseData<SalesIdModel> responseData = new ResponseData<>(new SalesIdModel(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	
	private boolean revertRemainingQtyDetailsBySalesId(Sales sales) {
		// revert stock details based on previous sales details data
		List<SalesOrderDetailsQtyUpdateLog> currentQtyUpdateLogsList = salesOrderDetailsQtyUpdateLogService.getSalesOrderDetailsQtyUpdateLogBySalesId(sales.getId());
		if(currentQtyUpdateLogsList != null) {
			for(SalesOrderDetailsQtyUpdateLog qtyUpdateLog : currentQtyUpdateLogsList) {
				salesOrderDetailsService.updateRemainingQty(qtyUpdateLog.getSalesOrderDetailsId(), qtyUpdateLog.getQuantity());
			}
		}
		salesOrderDetailsQtyUpdateLogService.deleteSalesOrderDetailsQtyUpdateLogBySalesId(sales.getId());
		return true;
	}
	
	@Override
	public boolean updateRemainingQtyStockDetails(Sales sales) {
		SearchData<SalesOrderDetails> salesOrderDetailsSearchData  = new SearchData<>();
		SalesOrderDetails salesOrderDetails = new SalesOrderDetails();
		salesOrderDetails.setContactId(sales.getContactId());
		salesOrderDetails.setDesktopSalesOrderId(sales.getDesktopSalesOrderId());
		salesOrderDetailsSearchData.setData(salesOrderDetails);
		List<SalesOrderDetails> remainingSalesOrderDetailsList = salesOrderDetailsService.getRemainingQtySalesOrderDetails(salesOrderDetailsSearchData);
		Map<String, List<SalesOrderDetails>> itemNameWiseRemainingSalesOrderDetails = new HashMap<>();
		List<SalesOrderDetails> tempSalesOrderDtList;
		for(SalesOrderDetails orderDetails : remainingSalesOrderDetailsList) {
			tempSalesOrderDtList = itemNameWiseRemainingSalesOrderDetails.get(orderDetails.getItemName());
			if(tempSalesOrderDtList == null) {
				tempSalesOrderDtList = new ArrayList<>();
				itemNameWiseRemainingSalesOrderDetails.put(orderDetails.getItemName(), tempSalesOrderDtList);
			}
			tempSalesOrderDtList.add(orderDetails);
		}
		
		List<SalesDetails> salesDetailsList = sales.getSalesDetailsList();
		List<SalesOrderDetails> remainItemStockToUpdate;
		List<SalesOrderDetailsQtyUpdateLog> qtyUpdateLogsList = new ArrayList<>(salesDetailsList.size());
		SalesOrderDetailsQtyUpdateLog qtyUpdateLog;
		
		List<SalesOrderDetails> salesOrderListToUpdate = new ArrayList<>();
		
		for(SalesDetails salesDetails : salesDetailsList) {
			salesDetails.setId(0L);
			salesDetails.setSalesId(sales.getId());
			salesDetails.setTransactionDate(LocalDateTime.now());
			salesDetailsService.save(salesDetails);
		}
		
		if(salesDetailsList!=null) {			
			for(SalesDetails salesDetails : salesDetailsList) {
				remainItemStockToUpdate =  itemNameWiseRemainingSalesOrderDetails.get(salesDetails.getItemName());
				double totalQtyToUpdate =  salesDetails.getQuantity();
				for(SalesOrderDetails orderDetailsToUpdate : remainItemStockToUpdate) {
					double remainingQty =  orderDetailsToUpdate.getRemainingQty();
					qtyUpdateLog = new SalesOrderDetailsQtyUpdateLog();
					qtyUpdateLog.setSalesId(salesDetails.getSalesId());
					qtyUpdateLog.setSalesDetailsId(salesDetails.getId());
					qtyUpdateLog.setSalesOrderDetailsId(orderDetailsToUpdate.getId());
					qtyUpdateLog.setDate(LocalDateTime.now());
					qtyUpdateLog.setItemName(salesDetails.getItemName());
					
					if(totalQtyToUpdate > remainingQty) {
						totalQtyToUpdate = totalQtyToUpdate - remainingQty;
						qtyUpdateLog.setQuantity(remainingQty);
						orderDetailsToUpdate.setRemainingQty(0d);
					} else {
						orderDetailsToUpdate.setRemainingQty(remainingQty - totalQtyToUpdate);
						qtyUpdateLog.setQuantity(totalQtyToUpdate);
						totalQtyToUpdate = 0d;
					}
					qtyUpdateLogsList.add(qtyUpdateLog);
					salesOrderListToUpdate.add(orderDetailsToUpdate);
					if(totalQtyToUpdate <= 0) {
						break;
					}
				}
				if(totalQtyToUpdate > 0) {
					throw new RuntimeException("Error occurred while updating remaining stock as total quatity is greater than order quatity for item :" + salesDetails.getItemName());
				}
			}
		}
		
		// save sales details list
		//getSalesDetailsService().saveOrUpdateAll(salesDetailsList);
		// save sales order details list
		getSalesOrderDetailsService().saveOrUpdateAll(salesOrderListToUpdate);
		// save sales order update qty logs
		getSalesOrderDetailsQtyUpdateLogService().saveOrUpdateAll(qtyUpdateLogsList);
		
		return true;
	}
	
	@Override
	public ResponseEntity<IResponseData<Sales>> delete(RequestData<Sales, Long> requestData) {
		Sales sales = requestData.getData();
		int count = this.delete(sales);
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		if(isCachingEnabled()) {
			removeCacheEntry(requestData.getData().getId());
		}
		IResponseData<Sales> responseData = new ResponseData<>(sales, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public Integer delete(Sales sales) {
		int count = salesDao.delete(sales);
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		// revert all qty details 
		revertRemainingQtyDetailsBySalesId(sales);
		
		if(isCachingEnabled()) {
			removeCacheEntry(sales.getId());
		}
		return count;
	}
	
	@Override
	public Sales setContactId(long salesId, long contactId, long desktopSalesOrderId) {
		if(salesId < 0) {
			return null;
		}
		Sales sales = salesDao.getById(salesId);
		sales.setContactId(contactId);
		sales.setDesktopSalesOrderId(desktopSalesOrderId);
		sales = salesDao.update(sales);
		return sales;
	}
	
}


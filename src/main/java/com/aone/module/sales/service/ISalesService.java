package com.aone.module.sales.service;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportQtyByAgent;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;
import com.aone.module.sales.model.SalesIdModel;
import com.aone.module.sales.model.SalesListData;

public interface ISalesService extends IBaseMasterService<Sales, Long> {

	public ResponseEntity<IResponseData<SalesListData>> getActiveDataWithTotal(
			RequestData<SearchData<Sales>, Long> requestData);

	public ResponseEntity<IResponseData<ReportSale>> getOrderSaleData(RequestData<SearchData<Sales>, Long> requestData);

	public ResponseEntity<IResponseData<ReportQty>> getOrderQtyData(RequestData<SearchData<Sales>, Long> requestData);

	public ResponseEntity<IResponseData<ReportQtyByAgent>> getOrderQtyByAgentData(
			RequestData<SearchData<Sales>, Long> requestData);

	public ResponseEntity<IResponseData<SalesIdModel>> updateSyncDesktopStatus(RequestData<SalesIdModel, Long> requestData);
	
	public boolean updateRemainingQtyStockDetails(Sales sales);

	public Sales setContactId(long salesId, long contactId, long desktopSalesOrderId);
}

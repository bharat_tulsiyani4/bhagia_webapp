package com.aone.module.sales.model;

import java.util.Collections;
import java.util.List;

import com.aone.module.contact.model.Contact;

public class SalesInitData {
	
	private List<Contact> contactList = Collections.emptyList();

	public List<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}

	@Override
	public String toString() {
		return "SalesInitData [contactList=" + contactList + "]";
	}

}

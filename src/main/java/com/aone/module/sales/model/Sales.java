package com.aone.module.sales.model;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.contact.model.Contact;
import com.aone.module.sales.constant.SalesDbColPojoConstant;
import com.aone.module.salesDetails.model.SalesDetails;

@Entity
@Table(name = "sales")
public class Sales extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = 7895104325035517069L;
	
	@Column(name = SalesDbColPojoConstant.SYNCED_IN_DESKTOP)
	private String syncedInDesktop;
	
	@Column(name = SalesDbColPojoConstant.DESKTOP_SALES_ORDER_NO)
	private long desktopSalesOrderId;
	
	@Column(name = SalesDbColPojoConstant.BILL_DATE)
	private LocalDateTime orderDate;
	
	@Column(name = SalesDbColPojoConstant.CONTACT_ID)
	private long contactId;
	
	@Column(name = SalesDbColPojoConstant.SUB_TOTAL)
	private double subTotal;
	
	@Column(name = SalesDbColPojoConstant.GRAND_TOTAL)
	private double grandTotal;
	
	@Column(name = SalesDbColPojoConstant.BASE_PRICE_NET_TOTAL)
	private double basePriceNetTotal;
	
	@Column(name = SalesDbColPojoConstant.LATITUDE)
	private String latitude;
	
	@Column(name = SalesDbColPojoConstant.LONGTITUDE)
	private String longtitude;
	
	@Column(name = SalesDbColPojoConstant.LOCATION)
	private String location;
	
	@Column(name = SalesDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private Contact contact; 
	
	@Transient
	private List<SalesDetails> salesDetailsList;
	
	@Transient
	private LocalDateTime startDate;
	
	@Transient
	private LocalDateTime endDate;
	
	@Transient
	private SalesInitData salesInitData;
	
	@Transient
	private long itemId;
	
	@Transient
	private String fullName;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.contact == null)
			this.contact = new Contact();
		if(this.salesDetailsList == null)
			this.salesDetailsList = Collections.emptyList();
		if(this.salesInitData == null)
			this.salesInitData = new SalesInitData();
		return true;
	}
	
	public String getSyncedInDesktop() {
		return syncedInDesktop;
	}

	public void setSyncedInDesktop(String syncedInDesktop) {
		this.syncedInDesktop = syncedInDesktop;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public long getDesktopSalesOrderId() {
		return desktopSalesOrderId;
	}

	public void setDesktopSalesOrderId(long desktopSalesOrderId) {
		this.desktopSalesOrderId = desktopSalesOrderId;
	}

	public LocalDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public double getBasePriceNetTotal() {
		return basePriceNetTotal;
	}

	public void setBasePriceNetTotal(double basePriceNetTotal) {
		this.basePriceNetTotal = basePriceNetTotal;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<SalesDetails> getSalesDetailsList() {
		return salesDetailsList;
	}

	public void setSalesDetailsList(List<SalesDetails> salesDetailsList) {
		this.salesDetailsList = salesDetailsList;
	}
	
	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}
	
	public SalesInitData getSalesInitData() {
		return salesInitData;
	}

	public void setSalesInitData(SalesInitData salesInitData) {
		this.salesInitData = salesInitData;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "Sales [syncedInDesktop=" + syncedInDesktop + ", desktopSalesOrderId=" + desktopSalesOrderId
				+ ", orderDate=" + orderDate + ", contactId=" + contactId + ", subTotal=" + subTotal + ", grandTotal="
				+ grandTotal + ", basePriceNetTotal=" + basePriceNetTotal + ", latitude=" + latitude + ", longtitude="
				+ longtitude + ", location=" + location + ", description=" + description + ", contact=" + contact
				+ ", salesDetailsList=" + salesDetailsList + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", salesInitData=" + salesInitData + ", itemId=" + itemId + ", fullName=" + fullName + "]";
	}

}

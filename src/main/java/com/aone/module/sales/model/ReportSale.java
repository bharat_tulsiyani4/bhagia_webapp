package com.aone.module.sales.model;

public class ReportSale {

	String distributorName, agentName, customerName;
	double grandTotal;
	
	public ReportSale() {
		
	}
	
	public ReportSale(String distributorName, String agentName, double grandTotal) {
		super();
		this.distributorName = distributorName;
		this.agentName = agentName;
		this.grandTotal = grandTotal;
	}
	
	public String getDistributorName() {
		return distributorName;
	}
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	
	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	@Override
	public String toString() {
		return "ReportSale [distributorName=" + distributorName + ", agentName=" + agentName + ", customerName="
				+ customerName + ", grandTotal=" + grandTotal + "]";
	}
	
	
	
}

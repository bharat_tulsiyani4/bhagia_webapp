package com.aone.module.sales.model;

public class GstWiseDetails {
	private String description;
	private double cgstAmount;
	private double cgstRate;
	private double sgstAmount;
	private double sgstRate;
	private double igstAmount;
	private double igstRate;
	private double netTotal;
	private double grandTotal;
	private double discount;
	
	public double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public double getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(double cgstRate) {
		this.cgstRate = cgstRate;
	}
	public double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public double getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(double sgstRate) {
		this.sgstRate = sgstRate;
	}
	public double getIgstAmount() {
		return igstAmount;
	}
	public void setIgstAmount(double igstAmount) {
		this.igstAmount = igstAmount;
	}
	public double getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(double igstRate) {
		this.igstRate = igstRate;
	}
	public double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(double netTotal) {
		this.netTotal = netTotal;
	}
	public double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		return "SalesDetailsGstWise [description=" + description + ", cgstAmount=" + cgstAmount + ", cgstRate="
				+ cgstRate + ", sgstAmount=" + sgstAmount + ", sgstRate=" + sgstRate + ", igstAmount=" + igstAmount
				+ ", igstRate=" + igstRate + ", netTotal=" + netTotal + ", grandTotal=" + grandTotal + ", discount="
				+ discount + "]";
	}
}

package com.aone.module.sales.model;

import java.util.List;

public class SalesIdModel {
	
	private List<Long> idArray;
	private String syncStatus; 

	public List<Long> getIdArray() {
		return idArray;
	}

	public void setIdArray(List<Long> idArray) {
		this.idArray = idArray;
	}

	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	@Override
	public String toString() {
		return "SalesIdModel [idArray=" + idArray + ", syncStatus=" + syncStatus + "]";
	}
	
}

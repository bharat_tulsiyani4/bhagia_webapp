package com.aone.module.sales.model;

import java.util.List;

public class SalesListData {

	private List<Sales> sales;
	private double totalAmount;
	private double totalBaseAmount;
	public List<Sales> getSales() {
		return sales;
	}
	public void setSales(List<Sales> sales) {
		this.sales = sales;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalBaseAmount() {
		return totalBaseAmount;
	}
	public void setTotalBaseAmount(double totalBaseAmount) {
		this.totalBaseAmount = totalBaseAmount;
	}
	@Override
	public String toString() {
		return "SalesListData [sales=" + sales + ", totalAmount=" + totalAmount + ", totalBaseAmount=" + totalBaseAmount
				+ "]";
	}
	
}

package com.aone.module.sales.model;

import java.time.LocalDateTime;

public class ReportQtyByAgent {
	
	 String custName;
	 LocalDateTime saleDate; 
	 double quantity;
	 
	 public ReportQtyByAgent() {
		 
	 }
	public ReportQtyByAgent(LocalDateTime saleDate,String custName, double quantity) {
		super();
		this.saleDate = saleDate;
		this.custName = custName;
		this.quantity = quantity;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public LocalDateTime getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(LocalDateTime saleDate) {
		this.saleDate = saleDate;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "ReportQtyByAgent [custName=" + custName + ", saleDate=" + saleDate + ", quantity=" + quantity + "]";
	}

	

}

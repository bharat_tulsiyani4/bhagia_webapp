package com.aone.module.sales.model;

public class ReportQty {
	
	 String agentName, itemName;
	 double quantity;
	 long agentId, itemId;
	public ReportQty(long agentId,long itemId, String agentName, String itemName, double quantity) {
		super();
		this.agentName = agentName;
		this.itemName = itemName;
		this.quantity = quantity;
		this.agentId = agentId;
		this.itemId = itemId;
	} 
	public ReportQty() {
		
	}
	
	public long getAgentId() {
		return agentId;
	}
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "ReportQty [agentName=" + agentName + ", itemName=" + itemName + ", quantity=" + quantity + "]";
	}
	 

}

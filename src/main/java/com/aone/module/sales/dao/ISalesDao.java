package com.aone.module.sales.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportQtyByAgent;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;

public interface ISalesDao extends IBaseMasterRepository<Sales, Long>{

	public Double getTotalOfData(SearchData<Sales> searchData);

	public Double getBaseTotalOfData(SearchData<Sales> searchData);

	public List<ReportSale> getOrderSaleData(SearchData<Sales> data);

	public List<ReportQty> getOrderQtyData(SearchData<Sales> searchData);

	public Long getOrderSaleDataTotal(SearchData<Sales> searchData);

	public List<ReportQtyByAgent> getOrderQtyByAgentData(SearchData<Sales> data);

	public int updateSalesDesktopSyncData(Long orderIds, String syncedInDesktop);

	public int deleteSalesByContactId(long contactId);

}

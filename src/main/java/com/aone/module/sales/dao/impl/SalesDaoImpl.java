package com.aone.module.sales.dao.impl;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.dao.IContactDao;
import com.aone.module.contact.model.Contact;
import com.aone.module.item.model.Item;
import com.aone.module.sales.dao.ISalesDao;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportQtyByAgent;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;
import com.aone.module.salesDetails.model.SalesDetails;

@Lazy
@Repository("salesDaoImpl")
public class SalesDaoImpl extends AbstractBaseMasterRepository<Sales, Long> implements ISalesDao {

	private IContactDao contactDao;
	public static final int COUNT_QUERY = 1;
	public static final int TOTAL_QUERY = 2;
	public static final int DATA_QUERY = 3;
	public static final int BASE_TOTAL_QUERY = 4;
	
	public IContactDao getContactDao() {
		return contactDao;
	}

	@Autowired
	@Qualifier(value = "contactDaoImpl")
	public void setContactDao(IContactDao contactDao) {
		this.contactDao = contactDao;
	}
	
	@Override
	public Class<Sales> getClazz() {
		return Sales.class;
	}
	

	@Override
	public List<Sales> getActiveData(SearchData<Sales> searchData) {
		TypedQuery<Sales> typedQuery = prepareQueryObject(DATA_QUERY, searchData, getClazz());
		return typedQuery.getResultList();
	}
	
	@Override
	public Double getTotalOfData(SearchData<Sales> searchData) {
		TypedQuery<Double> typedQuery = prepareQueryObject(TOTAL_QUERY, searchData, Double.class);
		return typedQuery.getSingleResult();
	}
	
	@Override
	public Double getBaseTotalOfData(SearchData<Sales> searchData) {
		TypedQuery<Double> typedQuery = prepareQueryObject(BASE_TOTAL_QUERY, searchData, Double.class);
		return typedQuery.getSingleResult();
	}
	
	@Override
	public Long getActiveDataCount(SearchData<Sales> searchData) {
		TypedQuery<Long> typedQuery = prepareQueryObject(COUNT_QUERY, searchData, Long.class);
		return typedQuery.getSingleResult();
	}
	
	private <A> TypedQuery<A> prepareQueryObject(int queryType, SearchData<Sales> searchData, Class<A> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);
		if(queryType == COUNT_QUERY) {
			queryBuilder.append("select count (e.id) from ");
		} else if (queryType == TOTAL_QUERY) {
			queryBuilder.append("select sum (e.subTotal) from ");
		} else if (queryType == BASE_TOTAL_QUERY) {
			queryBuilder.append("select sum (e.basePriceNetTotal) from ");
		} else {
			queryBuilder.append("select e from ");
		}
		queryBuilder.append(getClazz().getName() + " e ");
		
		Sales sales = searchData.getData();
		if(sales != null) {
			if(!StringUtils.isBlank(sales.getFullName())) {
				queryBuilder.append(" INNER JOIN ").append(Contact.class.getName()).append(" c ON c.id = e.contactId ");
				queryBuilder.append(" where e.deleted = :deleted ");
			} else {
				queryBuilder.append(" where e.deleted = :deleted ");
			}
			
		} else {
			queryBuilder.append(" where e.deleted = :deleted ");
		}
		
		long createdBy = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		/*if(createdBy > 0) {
			queryBuilder.append(" and e.createdBy = :createdBy ");
		}*/
		
		if(!StringUtils.isBlank(sales.getSyncedInDesktop())) {
			queryBuilder.append(" and e.syncedInDesktop = :syncedInDesktop ");
		}
		
		if(!StringUtils.isBlank(sales.getFullName())) {
			queryBuilder.append(" and c.fullName = :fullName ");
		}
		
		/*if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			queryBuilder.append(" and orderDate >= :startDate ");
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			queryBuilder.append(" and orderDate <= :endDate ");
		}*/
		
		if(searchData.getOrderingData() != null && queryType == DATA_QUERY) {
			queryBuilder.append(" order by e."+searchData.getOrderingData().getColumn());
			if(searchData.getOrderingData().isDescending()) {
				queryBuilder.append(" desc ");
			}
		}
		
		TypedQuery<A> typedQuery = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if(searchData.getPaginationData() != null && queryType == DATA_QUERY) {
			typedQuery.setFirstResult(searchData.getPaginationData().getRowsPerPage() * (searchData.getPaginationData().getPageNumber() - 1));
			typedQuery.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		typedQuery.setParameter("deleted", false);
		/*if(createdBy > 0) {
			typedQuery.setParameter("createdBy", createdBy);
		}*/
	/*	if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			typedQuery.setParameter("startDate", searchData.getData().getStartDate());
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			typedQuery.setParameter("endDate", searchData.getData().getEndDate());
		}*/
		
		if(!StringUtils.isBlank(sales.getFullName())) {
			typedQuery.setParameter("fullName", sales.getFullName());
		}
		
		if(!StringUtils.isBlank(sales.getSyncedInDesktop())) {
			typedQuery.setParameter("syncedInDesktop", sales.getSyncedInDesktop());
		}
		return typedQuery;
	}

	@Override
	public Sales getNewData() {
		Sales sales = new Sales();
		sales.setDefaultValueForObject();
		return sales;
	}
	
	private static String ORDER_SALES_SELECT_CLAUSE = "SELECT new com.aone.module.sales.model.ReportSale( d.name AS distributorName, a.name AS agentName, sum(s.subTotal) AS grandTotal)"; 
	private static String ORDER_SALES_SELECT_COUNT = "SELECT count(*) ";
	
	@Override
	public List<ReportSale> getOrderSaleData(SearchData<Sales> searchData) {

		TypedQuery<ReportSale> typedQuery = getOrderSaleQueryObject(ORDER_SALES_SELECT_CLAUSE, searchData, ReportSale.class);
		if(searchData.getPaginationData() != null ) {
			typedQuery.setFirstResult(searchData.getPaginationData().getRowsPerPage() * (searchData.getPaginationData().getPageNumber() - 1));
			typedQuery.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		return typedQuery.getResultList();
	}
	
	@Override
	public Long getOrderSaleDataTotal(SearchData<Sales> searchData) {
		TypedQuery<Long> typedQuery = getOrderSaleQueryObject(ORDER_SALES_SELECT_COUNT, searchData, Long.class);
		return typedQuery.getSingleResult();
	}
	
	private <A> TypedQuery<A> getOrderSaleQueryObject(String selectClause, SearchData<Sales> searchData, Class<A> clazz) {
		StringBuilder queryBuilder =new StringBuilder(selectClause)
				.append(" FROM ").append(Sales.class.getName()).append(" s ");
				queryBuilder.append(" WHERE s.deleted = :deleted ");
				Sales sales = searchData.getData();
				if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
					queryBuilder.append(" and s.orderDate >= :startDate ");
				}
				
				if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
					queryBuilder.append(" and s.orderDate <= :endDate ");
				}
				if(sales.getCreatedBy()>0) {
					queryBuilder.append(" and s.createdBy = :createdBy ");
				}
				queryBuilder.append(" group by a.id, d.id ORDER BY s.grandTotal DESC ");
				 	
				TypedQuery<A> typedQuery = getEntityManager().createQuery(queryBuilder.toString(), clazz);
				typedQuery.setParameter("deleted", false);
				if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
					typedQuery.setParameter("startDate", searchData.getData().getStartDate());
				}
				
				if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
					typedQuery.setParameter("endDate", searchData.getData().getEndDate());
				}
				if(sales.getCreatedBy()>0) {
					typedQuery.setParameter("createdBy", sales.getCreatedBy() );
				}
				return typedQuery;
	}
	
	@Override
	public List<ReportQty> getOrderQtyData(SearchData<Sales> searchData) {

		StringBuilder queryBuilder =new StringBuilder("SELECT new com.aone.module.sales.model."
				+ "ReportQty(a.id as agentId, im.id as itemId, a.name AS agentName,im.name as itemName, SUM(sd.quantity) AS quantity)"
				+ " FROM "
		+SalesDetails.class.getName()+" sd INNER JOIN "+Item.class.getName()+
		  " im ON im.id = sd.itemId AND sd.deleted = 0");
		Sales sales = searchData.getData();
		queryBuilder.append(" INNER JOIN "+Sales.class.getName()+ " s ON s.id = sd.salesId ");
		if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			queryBuilder.append(" and s.orderDate >= :startDate ");
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			queryBuilder.append(" and s.orderDate <= :endDate ");
		}
		if(sales.getCreatedBy()>0) {
			queryBuilder.append(" and s.createdBy = :createdBy ");
		}

		queryBuilder.append(" GROUP BY sd.itemId,a.id ORDER BY quantity DESC");
		
		 	
		TypedQuery<ReportQty> typedQuery = getEntityManager().createQuery(queryBuilder.toString(), ReportQty.class);
		if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			typedQuery.setParameter("startDate", searchData.getData().getStartDate());
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			typedQuery.setParameter("endDate", searchData.getData().getEndDate());
		}
		if(sales.getCreatedBy()>0) {
			typedQuery.setParameter("createdBy", sales.getCreatedBy() );
		}
		return typedQuery.getResultList();
	}

	@Override
	public List<ReportQtyByAgent> getOrderQtyByAgentData(SearchData<Sales> searchData) {

		StringBuilder queryBuilder =new StringBuilder("SELECT new com.aone.module.sales.model."
				+ "ReportQtyByAgent(s.orderDate as saleDate, CONCAT(c.fname,' ',c.lname) AS custName,sd.quantity AS quantity) "
				+ " FROM "
		+Sales.class.getName()+" s INNER JOIN "+SalesDetails.class.getName()+
		  " sd ON s.id = sd.salesId ");
		Sales sales = searchData.getData();
		if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			queryBuilder.append(" and s.orderDate >= :startDate ");
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			queryBuilder.append(" and s.orderDate <= :endDate ");
		}
		if(sales.getCreatedBy()>0) {
			queryBuilder.append(" and s.createdBy = :createdBy ");
		}else {
			throw new InvalidParameterException();
		}
		
		queryBuilder.append(" INNER JOIN "+Sales.class.getName()+ " s ON s.id = sd.salesId ");
		if(sales.getItemId() > 0) {
			queryBuilder.append(" and sd.itemId = :itemId ");
		}
		queryBuilder.append(" INNER JOIN "+Contact.class.getName()+ " c ON s.contactId = c.id ");

		queryBuilder.append(" ORDER BY quantity DESC");
		
		 	
		TypedQuery<ReportQtyByAgent> typedQuery = getEntityManager().createQuery(queryBuilder.toString(), ReportQtyByAgent.class);
		if(searchData.getData() != null && searchData.getData().getStartDate() != null) {
			typedQuery.setParameter("startDate", searchData.getData().getStartDate());
		}
		
		if(searchData.getData() != null && searchData.getData().getEndDate() != null) {
			typedQuery.setParameter("endDate", searchData.getData().getEndDate());
		}
		if(sales.getCreatedBy()>0) {
			typedQuery.setParameter("createdBy", sales.getCreatedBy() );
		}
		if(sales.getItemId() > 0) {
			typedQuery.setParameter("itemId", sales.getItemId() );
		}
		return typedQuery.getResultList();
}
	
	@Override
	public int updateSalesDesktopSyncData(Long orderId, String syncedInDesktop) {
		if (orderId == null) {
			return 0;
		}
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append("UPDATE ");
		queryBuilder.append(getClazz().getName());
		queryBuilder.append(" SET syncedInDesktop = '" + syncedInDesktop+ "'");
		queryBuilder.append(" where id = "+ orderId);
		
		return getEntityManager().createQuery(queryBuilder.toString()).executeUpdate();
	}
	
	@Override
	public int deleteSalesByContactId(long contactId) {
		String whereClause = " WHERE contactId = :contactId";
		Map<String, Object> params = new HashMap<>();
		params.put("contactId", contactId);
		return super.deleteByQuery(whereClause, params);
	}
}

package com.aone.module.sales.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.sales.controller.ISalesController;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportQtyByAgent;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;
import com.aone.module.sales.model.SalesIdModel;
import com.aone.module.sales.model.SalesListData;
import com.aone.module.sales.service.ISalesService;

@RestController
@RequestMapping(value = {"/rest/sales", "/web/rest/sales"})
@SecurityClassRole(baseRoleName = SecurityRoleModule.SALES)
public class SalesRestController extends AbstractBaseMasterRestController<Sales, Long, ISalesService> implements ISalesController {
	
	private ISalesService salesService;
	
	@Override
	public ISalesService getService() {
		return getSalesService();
	}

	public ISalesService getSalesService() {
		return salesService;
	}

	@Autowired        
	@Qualifier(value="salesServiceImpl")
	public void setSalesService(ISalesService salesService) {
		this.salesService = salesService;
	}
	
	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<Sales>> getActiveData(@RequestBody RequestData<SearchData<Sales>, Long> requestData) {
		return super.getActiveData(requestData);
	}
	
	@Override
	@RequestMapping(value="/getActiveDataWithTotal", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<SalesListData>> getActiveDataWithTotal(@RequestBody RequestData<SearchData<Sales>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return salesService.getActiveDataWithTotal(requestData);
	}
	
	@Override
	@RequestMapping(value="/getOrderSaleData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "REPORT_SALE_VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<ReportSale>> getOrderSaleData(@RequestBody RequestData<SearchData<Sales>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return salesService.getOrderSaleData(requestData);
	}
	
	@Override
	@RequestMapping(value="/getOrderQtyData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "REPORT_QTY_VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<ReportQty>> getOrderQtyData(@RequestBody RequestData<SearchData<Sales>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return salesService.getOrderQtyData(requestData);
	}
	
	@Override
	@RequestMapping(value="/getOrderQtyByAgentData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "REPORT_QTY_VIEW" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<ReportQtyByAgent>> getOrderQtyByAgentData(@RequestBody RequestData<SearchData<Sales>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return salesService.getOrderQtyByAgentData(requestData);
	}

	@Override
	@RequestMapping(value="/updateSyncDesktopStatus", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
//	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ALL)
	public @ResponseBody ResponseEntity<IResponseData<SalesIdModel>> updateSyncDesktopStatus(@RequestBody RequestData<SalesIdModel,Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().updateSyncDesktopStatus(requestData);
	}

}

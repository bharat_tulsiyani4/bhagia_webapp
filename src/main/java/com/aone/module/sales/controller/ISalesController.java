package com.aone.module.sales.controller;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.sales.model.ReportQty;
import com.aone.module.sales.model.ReportSale;
import com.aone.module.sales.model.Sales;
import com.aone.module.sales.model.SalesIdModel;
import com.aone.module.sales.model.SalesListData;

public interface ISalesController extends IBaseMasterController<Sales, Long>{

	ResponseEntity<IResponseData<SalesListData>> getActiveDataWithTotal(
			RequestData<SearchData<Sales>, Long> requestData);

	ResponseEntity<IResponseData<ReportSale>> getOrderSaleData(RequestData<SearchData<Sales>, Long> requestData);

	ResponseEntity<IResponseData<ReportQty>> getOrderQtyData(RequestData<SearchData<Sales>, Long> requestData);

	ResponseEntity<IResponseData<com.aone.module.sales.model.ReportQtyByAgent>> getOrderQtyByAgentData(
			RequestData<SearchData<Sales>, Long> requestData);

	ResponseEntity<IResponseData<SalesIdModel>> updateSyncDesktopStatus(RequestData<SalesIdModel, Long> requestData);

}

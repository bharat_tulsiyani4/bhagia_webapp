package com.aone.module.sales.constant;

public class SalesDbColPojoConstant {
	
	public static final String SYNCED_IN_DESKTOP="synced_in_desktop";
	public static final String DESKTOP_SALES_ORDER_NO="desktop_sales_order_no";
	public static final String BILL_DATE="bill_date";
	public static final String CONTACT_ID="contact_id";
	public static final String SUB_TOTAL="sub_total";
	public static final String GRAND_TOTAL="grand_total";
	public static final String BASE_PRICE_NET_TOTAL="base_price_net_total";
	public static final String LATITUDE="latitude";
	public static final String LONGTITUDE="longtitude";
	public static final String LOCATION="location";
	public static final String ORDER_TYPE = "order_type";	
	public static final String DESCRIPTION ="description";
	
	private SalesDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

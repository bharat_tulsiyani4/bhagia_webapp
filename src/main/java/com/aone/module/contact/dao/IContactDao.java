package com.aone.module.contact.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.model.Contact;

public interface IContactDao extends IBaseMasterRepository<Contact, Long>{

	public List<Contact> getSearchActiveData(SearchData<Contact> data);

	public Long getSearchActiveDataCount(SearchData<Contact> searchData);

	public List<Contact> getByMobile1Number(String mobile1);

	public List<Contact> getCurrentOrderExistData(SearchData<Contact> searchData);

}

package com.aone.module.contact.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.dao.IContactDao;
import com.aone.module.contact.model.Contact;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

@Lazy
@Repository("contactDaoImpl")
public class ContactDaoImpl extends AbstractBaseMasterRepository<Contact, Long> implements IContactDao  {

	@Override
	public Class<Contact> getClazz() {
		return Contact.class;
	}
	
	@Override
	public boolean isPublicModule() {
		return true;
	}

	@Override
	public List<Contact> getSearchActiveData(SearchData<Contact> searchData) {
		/*long createdBy = LoginServiceImpl.getCurrentLoginUserId();
		if(createdBy == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}*/
		Contact contact = searchData.getData();
		
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append("select e from ");
		queryBuilder.append(getClazz().getName() + " e ");
		queryBuilder.append("where e.deleted = :deleted ");
		if(contact != null) {
			if(contact.getCreatedBy() != 0) {
				queryBuilder.append(" AND e.createdBy = :createdBy ");
			}
			
			if(contact.getFullName() != null && !contact.getFullName().isEmpty()) {
				queryBuilder.append(" and  e.fullName like :fullName ");
			}
			
			if(contact.getMobile1() != null && !contact.getMobile1().isEmpty()) {
				queryBuilder.append(" and  e.mobile1 like :mobile1 ");
			}
			
			if(contact.getMobile2() != null && !contact.getMobile2().isEmpty()) {
				queryBuilder.append(" and  e.mobile2 like :mobile2 ");
			}
			
			if(contact.getStartDate() != null) {
				queryBuilder.append(" and  e.createdOn >= :startDate ");
			}
			
			if(contact.getEndDate() != null) {
				queryBuilder.append(" and  e.createdOn <= :endDate ");
			}
		}
		
		if(searchData.getOrderingData() != null) {
			queryBuilder.append(" order by e."+searchData.getOrderingData().getColumn());
			if(searchData.getOrderingData().isDescending()) {
				queryBuilder.append(" desc ");
			}
		}
		TypedQuery<Contact> query = getEntityManager().createQuery(queryBuilder.toString(), getClazz());
		if(searchData.getPaginationData() != null) {
			query.setFirstResult(searchData.getPaginationData().getRowsPerPage() * (searchData.getPaginationData().getPageNumber() - 1));
			query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		
		query.setParameter("deleted", false);
		if(contact != null) {
			if(contact.getCreatedBy() != 0) {
				query.setParameter("createdBy", contact.getCreatedBy());
			}
			
			if(contact.getFullName() != null && !contact.getFullName().isEmpty()) {
				query.setParameter("fullName", "%" + contact.getFullName() + "%");
			}
			
			if(contact.getMobile1() != null && !contact.getMobile1().isEmpty()) {
				query.setParameter("mobile1", "%" + contact.getMobile1() + "%");
			}
			
			if(contact.getMobile2() != null && !contact.getMobile2().isEmpty()) {
				query.setParameter("mobile2", "%" + contact.getMobile2() + "%");
			}
			
			if(contact.getStartDate() != null) {
				query.setParameter("startDate", contact.getStartDate());
			}
			
			if(contact.getEndDate() != null) {
				query.setParameter("endDate", contact.getEndDate());
			}
		}
		
		return query.getResultList();
	}
	
	@Override
	public Long getSearchActiveDataCount(SearchData<Contact> searchData) {
		/*long createdBy = LoginServiceImpl.getCurrentLoginUserId();
		if(createdBy == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}*/
		Contact contact = searchData.getData();
		
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append("select count (e.id) from ");
		queryBuilder.append(getClazz().getName() + " e ");
		queryBuilder.append("where e.deleted = :deleted ");
		if(contact != null) {
			if(contact.getCreatedBy() != 0) {
				queryBuilder.append(" AND e.createdBy = :createdBy ");
			}
			
			if(contact.getFullName() != null && !contact.getFullName().isEmpty()) {
				queryBuilder.append(" and  e.fullName like :fullName ");
			}
			
			if(contact.getMobile1() != null && !contact.getMobile1().isEmpty()) {
				queryBuilder.append(" and  e.mobile1 like :mobile1 ");
			}
			
			if(contact.getMobile2() != null && !contact.getMobile2().isEmpty()) {
				queryBuilder.append(" and  e.mobile2 like :mobile2 ");
			}
			
			if(contact.getStartDate() != null) {
				queryBuilder.append(" and  e.createdOn >= :startDate ");
			}
			
			if(contact.getEndDate() != null) {
				queryBuilder.append(" and  e.createdOn <= :endDate ");
			}
		}
		
		TypedQuery<Long> query = getEntityManager().createQuery(queryBuilder.toString(), Long.class);
		query.setParameter("deleted", false);
		/*if(createdBy != -1) {
			query.setParameter("createdBy", createdBy);
		}*/
		if(contact != null) {
			if(contact.getCreatedBy() != 0) {
				query.setParameter("createdBy", contact.getCreatedBy());
			}
			
			if(contact.getFullName() != null && !contact.getFullName().isEmpty()) {
				query.setParameter("fullName", "%" + contact.getFullName() + "%");
			}
			
			if(contact.getMobile1() != null && !contact.getMobile1().isEmpty()) {
				query.setParameter("mobile1", "%" + contact.getMobile1() + "%");
			}
			
			if(contact.getMobile2() != null && !contact.getMobile2().isEmpty()) {
				query.setParameter("mobile2", "%" + contact.getMobile2() + "%");
			}
			
			if(contact.getStartDate() != null) {
				query.setParameter("startDate", contact.getStartDate());
			}
			
			if(contact.getEndDate() != null) {
				query.setParameter("endDate", contact.getEndDate());
			}
		}
		return query.getSingleResult();
	}

	@Override
	public Contact getNewData() {
		Contact contact = new Contact();
		contact.setDefaultValueForObject();
		return contact;
	}
	
	@Override
	public List<Contact> getByMobile1Number(String mobile1){
		return getByFieldName("mobile1", mobile1);
	}
	
	@Override
	public List<Contact> getCurrentOrderExistData(SearchData<Contact> searchData) {
		StringBuilder queryBuilder = new StringBuilder(50);
		queryBuilder.append("select distinct e from ");
		queryBuilder.append(getClazz().getName() + " e ");
		queryBuilder.append(" INNER JOIN ").append(SalesOrder.class.getName()).append(" so ON so.contactId = e.id ");
		queryBuilder.append(" INNER JOIN ").append(SalesOrderDetails.class.getName()).append(" sod ON sod.salesOrderId = so.id ");
		queryBuilder.append(" where (e.deleted = :deleted ");
		queryBuilder.append(" AND sod.remainingQty > 0 and so.deleted = :deleted ) ");
		
		Contact contact = searchData.getData();
		if(contact !=null && contact.getId() != null && contact.getId() > 0 ) {
			queryBuilder.append(" OR e.id =:id) ");
		}
		
		TypedQuery<Contact> query = getEntityManager().createQuery(queryBuilder.toString(), getClazz());
		query.setParameter("deleted", false);
		if(contact !=null && contact.getId() != null && contact.getId() > 0 ) {
			query.setParameter("id", contact.getId());
		}
		return query.getResultList();
	}
}

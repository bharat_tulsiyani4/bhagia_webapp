package com.aone.module.contact.controller.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.controller.IContactController;
import com.aone.module.contact.model.Contact;
import com.aone.module.contact.service.IContactService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = {"/rest/contact", "/web/rest/contact"})
@SecurityClassRole(baseRoleName = SecurityRoleModule.CONTACT)
public class ContactRestController extends AbstractBaseMasterRestController<Contact, Long, IContactService > implements IContactController  {
	
	private IContactService contactService;
	
	@Override
	public IContactService getService() {
		return getContactService();
	}

	/**
	 * @return the contactService
	 */
	public IContactService getContactService() {
		return contactService;
	}

	/**
	 * @param contactService the contactService to set
	 */
	@Autowired        
	@Qualifier(value="contactServiceImpl")
	public void setContactService(IContactService contactService) {
		this.contactService = contactService;
	}
	
	@Override
	@RequestMapping(value="/saveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Contact>> saveData(HttpServletRequest request, @RequestBody RequestData<Contact,Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		String requestURI = request.getRequestURI();
		String calledFrom = "";
		if(requestURI.contains("/web/rest/")) {
			calledFrom = "Web";
		} else {
			calledFrom = "App";
		}
		return getService().save(requestData, calledFrom);
	}
	
	@Override
	@RequestMapping(value="/updateData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Contact>> updateData(HttpServletRequest request, @RequestBody RequestData<Contact,Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		String requestURI = request.getRequestURI();
		String calledFrom = "";
		if(requestURI.contains("/web/rest/")) {
			calledFrom = "Web";
		} else {
			calledFrom = "App";
		}
		return getService().update(requestData, calledFrom);
	}

	@Override
	@RequestMapping(value="/saveWithFile", headers=("content-type=multipart/*"), method=RequestMethod.POST, consumes= {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<Contact>> saveWithFile(HttpServletRequest request, @RequestParam("bodyData") String bodyData,@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		RequestData<Contact, Long> requestData = mapper.readValue(bodyData, new TypeReference<RequestData<Contact, Long>>() {});
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		String requestURI = request.getRequestURI();
		String calledFrom = "";
		if(requestURI.contains("/web/rest/")) {
			calledFrom = "Web";
		} else {
			calledFrom = "App";
		}
		return contactService.saveWithFile(requestData, uploadingFiles, calledFrom);
	}
	
	@Override
	@RequestMapping(value="/updateWithFile", headers=("content-type=multipart/*"), method=RequestMethod.POST, consumes= {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public ResponseEntity<IResponseData<Contact>> updateWithFile(HttpServletRequest request, @RequestParam("bodyData") String bodyData,@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		RequestData<Contact, Long> requestData = mapper.readValue(bodyData, new TypeReference<RequestData<Contact, Long>>() {});
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		String requestURI = request.getRequestURI();
		String calledFrom = "";
		if(requestURI.contains("/web/rest/")) {
			calledFrom = "Web";
		} else {
			calledFrom = "App";
		}
		return contactService.updateWithFile(requestData, uploadingFiles, calledFrom);
	}
	
	@Override
	@RequestMapping(value="/getSearchActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Contact>> getSearchActiveData(@RequestBody RequestData<SearchData<Contact>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().getSearchActiveData(requestData);
	}
	
	@Override
	@RequestMapping(value="/deleteData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "DELETE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Contact>> deleteData(HttpServletRequest request, @RequestBody RequestData<Contact,Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		String requestURI = request.getRequestURI();
		String calledFrom = "";
		if(requestURI.contains("/web/rest/")) {
			calledFrom = "Web";
		} else {
			calledFrom = "App";
		}
		return getService().deleteData(requestData, calledFrom);
	}
	
	@Override
	@RequestMapping(value="/getByMobile1Number", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public @ResponseBody ResponseEntity<IResponseData<Contact>> getByMobile1Number(@RequestBody RequestData<SearchData<Contact>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().getByMobile1Number(requestData);
	}
	
	@Override
	@RequestMapping(value="/getCurrentOrderExistData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Contact>> getCurrentOrderExistData(@RequestBody RequestData<SearchData<Contact>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().getCurrentOrderExistData(requestData);
	}
	
}

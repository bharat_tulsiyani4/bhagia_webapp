package com.aone.module.contact.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.contact.model.Contact;

public interface IContactController extends IBaseMasterController<Contact, Long>{
	
	public ResponseEntity<IResponseData<Contact>> saveWithFile(HttpServletRequest request, String bodyData, MultipartFile[] uploadingFiles)
			throws IOException;
	
	public ResponseEntity<IResponseData<Contact>> updateWithFile(HttpServletRequest request, String bodyData, MultipartFile[] uploadingFiles)
			throws IOException;

	public ResponseEntity<IResponseData<Contact>> getSearchActiveData(RequestData<SearchData<Contact>, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> saveData(HttpServletRequest request, RequestData<Contact, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> updateData(HttpServletRequest request, RequestData<Contact, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> deleteData(HttpServletRequest request, RequestData<Contact, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> getByMobile1Number(RequestData<SearchData<Contact>, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> getCurrentOrderExistData(RequestData<SearchData<Contact>, Long> requestData);
}

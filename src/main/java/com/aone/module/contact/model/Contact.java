package com.aone.module.contact.model;


import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.address.model.Address;
import com.aone.module.contact.constant.ContactDbColPojoConstant;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;
import com.aone.module.files.model.FilesData;

/**
 * The Class Contact.
 */
@Entity
@Table(name = "contact_master")
public class Contact extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = -5483791619804079369L;
	
	@Column(name = ContactDbColPojoConstant.COMPANY_NAME)
	private String companyName;
	
	@Column(name = ContactDbColPojoConstant.MOBILE_1)
	private String mobile1;
	
	@Column(name = ContactDbColPojoConstant.MOBILE_2)
	private String mobile2;

	@Column(name = ContactDbColPojoConstant.FULL_NAME)
	private String fullName;
	
	@Column(name = ContactDbColPojoConstant.EMAIL_ID)
	private String emailId;
	
	@Column(name = ContactDbColPojoConstant.AMOUNT)
	private double amount;
	
	@Column(name = ContactDbColPojoConstant.PRODUCT)
	private String product;
	
	@Column(name = ContactDbColPojoConstant.REFERRED_BY)
	private String referredBy;
	
	@Column(name = ContactDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private LocalDateTime startDate;
	
	@Transient
	private LocalDateTime endDate;
	
	@Transient
	private Address shopAddress;
	
	@Transient
	private List<FilesData> filesData;
	
	@Transient
	private List<ContactNotesDetails> contactNotesDetailsList; 
	
	@Transient
	private ContactInitData contactInitData;
	
	@Transient
	private String createdByUserName;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.shopAddress == null)
			this.shopAddress = new Address();
		if(this.filesData == null)
			this.filesData = Collections.emptyList();
		if(this.contactNotesDetailsList == null)
			this.contactNotesDetailsList = Collections.emptyList();
		return true;
	}

	public String getMobile1() {
		return mobile1;
	}

	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public Address getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(Address shopAddress) {
		this.shopAddress = shopAddress;
	}

	public List<FilesData> getFilesData() {
		return filesData;
	}

	public void setFilesData(List<FilesData> filesData) {
		this.filesData = filesData;
	}
	
	public List<ContactNotesDetails> getContactNotesDetailsList() {
		return contactNotesDetailsList;
	}

	public void setContactNotesDetailsList(List<ContactNotesDetails> contactNotesDetailsList) {
		this.contactNotesDetailsList = contactNotesDetailsList;
	}

	public ContactInitData getContactInitData() {
		return contactInitData;
	}

	public void setContactInitData(ContactInitData contactInitData) {
		this.contactInitData = contactInitData;
	}

	public String getCreatedByUserName() {
		return createdByUserName;
	}

	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	@Override
	public String toString() {
		return "Contact [mobile1=" + mobile1 + ", mobile2=" + mobile2 + ", fullName=" + fullName + ", emailId="
				+ emailId + ", amount=" + amount + ", product=" + product + ", referredBy=" + referredBy
				+ ", description=" + description + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", shopAddress=" + shopAddress + ", filesData=" + filesData + ", contactNotesDetailsList="
				+ contactNotesDetailsList + ", contactInitData=" + contactInitData + ", createdByUserName="
				+ createdByUserName + "]";
	}

}

package com.aone.module.contact.model;

import java.util.List;

import com.aone.module.city.model.City;
import com.aone.module.country.model.Country;
import com.aone.module.state.model.State;

public class ContactInitData {
	
	private List<City> cityList;
	private List<State> stateList;
	private List<Country> countryList;
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	public List<State> getStateList() {
		return stateList;
	}
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}
	public List<Country> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}

}

package com.aone.module.contact.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aone.commons.utils.FileUtils;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.address.constant.ModuleName;
import com.aone.module.address.model.Address;
import com.aone.module.address.service.IAddressService;
import com.aone.module.city.model.City;
import com.aone.module.city.service.ICityService;
import com.aone.module.contact.dao.IContactDao;
import com.aone.module.contact.model.Contact;
import com.aone.module.contact.model.ContactInitData;
import com.aone.module.contact.service.IContactService;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;
import com.aone.module.contactNotesDetails.service.IContactNotesDetailsService;
import com.aone.module.country.model.Country;
import com.aone.module.country.service.ICountryService;
import com.aone.module.files.constant.FilesDataModuleName;
import com.aone.module.files.constant.FilesDataPathConstant;
import com.aone.module.files.model.FilesData;
import com.aone.module.files.service.IFilesDataService;
import com.aone.module.login.service.impl.LoginServiceImpl;
import com.aone.module.state.model.State;
import com.aone.module.state.service.IStateService;
import com.aone.module.user.model.User;
import com.aone.module.user.service.IUserService;

@Lazy
@Service("contactServiceImpl")
public class ContactServiceImpl extends AbstractBaseMasterService<Contact, Long, IContactDao>
		implements IContactService {

	private IContactDao contactDao;
	private IAddressService addressService;
	private IFilesDataService filesDataService;
	private ICountryService countryService;
	private IStateService stateService;
	private ICityService cityService;
	private IContactNotesDetailsService contactNotesDetailsService;
	private IUserService userService;
	
	public IUserService getUserService() {
		return userService;
	}

	@Autowired        
	@Qualifier(value="userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	public IContactNotesDetailsService getContactNotesDetailsService() {
		return contactNotesDetailsService;
	}

	@Autowired        
	@Qualifier(value="contactNotesDetailsServiceImpl")
	public void setContactNotesDetailsService(IContactNotesDetailsService contactNotesDetailsService) {
		this.contactNotesDetailsService = contactNotesDetailsService;
	}
	
	public ICityService getCityService() {
		return cityService;
	}

	@Autowired        
	@Qualifier(value="cityServiceImpl")
	public void setCityService(ICityService cityService) {
		this.cityService = cityService;
	}
	
	public IStateService getStateService() {
		return stateService;
	}

	@Autowired        
	@Qualifier(value="stateServiceImpl")
	public void setStateService(IStateService stateService) {
		this.stateService = stateService;
	}
	
	public ICountryService getCountryService() {
		return countryService;
	}

	@Autowired
	@Qualifier(value="countryServiceImpl")
	public void setCountryService(ICountryService countryService) {
		this.countryService = countryService;
	}
	
	@Scheduled(cron="0 0 0 * * ?")
	public boolean cacheContactData() {
		System.out.println("Running cron job to cache contact data");
		
		System.out.println("Removing all cache entry");
		removeAllCacheEntry();
		
		System.out.println("Caching all data");
		SearchData<Contact> searchData =  new SearchData<>();
		this.getActiveData(searchData);
		return true;
	}
	

	@Override
	public IContactDao getRepository() {
		return getContactDao();
	}
	
	@Override
	public boolean isSetDefaultRequired() {
		return true;
	}

	public IContactDao getContactDao() {
		return contactDao;
	}

	@Autowired
	@Qualifier(value = "contactDaoImpl")
	public void setContactDao(IContactDao contactDao) {
		this.contactDao = contactDao;
	}
	
	public IAddressService getAddressService() {
		return addressService;
	}

	@Autowired
	@Qualifier(value="addressServiceImpl")
	public void setAddressService(IAddressService addressService) {
		this.addressService = addressService;
	}
	
	public IFilesDataService getFilesDataService() {
		return filesDataService;
	}

	@Autowired
	@Qualifier(value="filesDataServiceImpl")
	public void setFilesDataService(IFilesDataService filesDataService) {
		this.filesDataService = filesDataService;
	}
	
	@Override
	public boolean isCachingEnabled() {
		return false;
	}
	
	@Override
	public ResultCodeConstant validateData(Contact contact) {
		if(contact == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		
		// validate name
		if(contact.getFullName() == null || contact.getFullName().isEmpty()) {
			return ResultCodeConstant.CONTACT_FULL_NAME_NOT_FOUND;
		}
		// check if UserRoleGroup name already exist
		
		if(!getRepository().checkUniqueDataByField("fullName", contact.getFullName(), "id", contact.getId()).isEmpty()) {
			return ResultCodeConstant.CONTACT_FULL_NAME_ALREADY_EXIST;
		}
		
		if(!getRepository().checkUniqueDataByField("mobile1", contact.getMobile1(), "id", contact.getId()).isEmpty()) {
			return ResultCodeConstant.CONTACT_MOBILE1_ALREADY_EXIST;
		}
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> init(RequestData<Contact, Long> requestData) {
		Contact contact = new Contact();
		contact.setContactInitData(getContactInitData());
		
		IResponseData<Contact> responseData = new ResponseData<>(contact, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<Contact>> save(RequestData<Contact, Long> requestData) {
		Contact contact = requestData.getData();
		
		// validate contact data and save
		ResultCodeConstant resultCodeConstant = this.save(contact);
		
		if(isCachingEnabled()) {
			addCacheEntry(contact.getId(), contact);
		}
		// return the response object to client
		IResponseData<Contact> responseData = new ResponseData<>(contact, getRepository().getNewDataList(), resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> update(RequestData<Contact, Long> requestData) {
		Contact contact = requestData.getData();
		
		// validate contact data and update it
		ResultCodeConstant resultCodeConstant = this.update(contact);
		
		if(isCachingEnabled()) {
			addCacheEntry(contact.getId(), contact);
		}
		// return the response object to client
		IResponseData<Contact> responseData = new ResponseData<>(contact, getRepository().getNewDataList(), resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> saveOrUpdate(RequestData<Contact, Long> requestData) {
		Contact contact = requestData.getData();
		
		if(contact.getId() != null && contact.getId() > 0) {
			return this.update(requestData);
		}
		return this.save(requestData);
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> saveOrUpdateAll(RequestData<Contact, Long> requestData) {
		List<Contact> contactsList = requestData.getDataList();
		
		if(contactsList == null || contactsList.isEmpty()) {
			IResponseData<Contact> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.PARAMETER_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		for(Contact contact : contactsList) {
			if(this.saveOrUpdate(contact) == null) {
				IResponseData<Contact> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.INTERNAL_ERROR);
				responseData.setRequestToken(requestData.getRequestToken());
				return new ResponseEntity<>(responseData, HttpStatus.OK);
			}
		}
		
		// return the response object to client
		IResponseData<Contact> responseData = new ResponseData<>(getRepository().getNewData(), contactsList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> getSearchActiveData(RequestData<SearchData<Contact>, Long> requestData) {
		List<Contact> contactList = getSearchActiveData(requestData.getData(), true);
		IResponseData<Contact> responseData = new ResponseData<>(getRepository().getNewData(), contactList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		responseData.setTotalRecords(getRepository().getSearchActiveDataCount(requestData.getData()));
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public List<Contact> getSearchActiveData(SearchData<Contact> searchData, boolean loadFullData){
		List<Contact> contactList = contactDao.getSearchActiveData(searchData);

		if(loadFullData) {
			setContactData(contactList);
		}
		return contactList;
	}
	
	
	
	@Override
	public ResponseEntity<IResponseData<Contact>> getActiveData(RequestData<SearchData<Contact>, Long> requestData) {
		List<Contact> contactList = getActiveData(requestData.getData());
		IResponseData<Contact> responseData = new ResponseData<>(getRepository().getNewData(), contactList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		responseData.setTotalRecords(getRepository().getSearchActiveDataCount(requestData.getData()));
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public List<Contact> getActiveData(SearchData<Contact> searchData){
		List<Contact> contactList = Collections.emptyList();
		if(isCachingEnabled()) {
			contactList = new ArrayList<>(getAllCacheEntry());
		}
		
		if(contactList == null || contactList.isEmpty()) {
			contactList = contactDao.getActiveData(searchData);
			setContactData(contactList);
		}
		
		return contactList;
	}
	
	private boolean setContactData(List<Contact> contactList) {
		// billing address references
		Address shopAddress = new Address();
		shopAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
		List<Address> shopAddressDataList = null;
		FilesData filesData = new FilesData();
		filesData.setModuleName(FilesDataModuleName.CONTACT.getValue());
		Contact contact = null;
		
		Map<Long, User> userIdWiseMap = new HashMap<>();
		User user = null;
		for (int index = 0; index < contactList.size(); index++) {
			contact = contactList.get(index);
			if(isCachingEnabled()) {
				Contact contactTemp = getCacheEntry(contact.getId());
				if(contactTemp != null) {
					contactList.set(index, contactTemp);
					continue;
				}
			}
			// setting contact address details
			shopAddress.setForeinKey(contact.getId());
			shopAddressDataList = addressService.getAddressByContact(shopAddress);
			if (!shopAddressDataList.isEmpty()) {
				contact.setShopAddress(shopAddressDataList.get(0));
			}
			
			// set files data
			/*filesData.setForeinKey(contact.getId());
			try {
				contact.setFilesData(filesDataService.getFileData(filesData).getBody().getDataList());
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			// set user details
			user = userIdWiseMap.get(contact.getCreatedBy());
			if(user == null) {
				user = userService.getById(contact.getCreatedBy());
				userIdWiseMap.put(user.getId(), user);
			}
			contact.setCreatedByUserName(user.getName());
			
			// set default values
			if(isSetDefaultRequired()) {
				contact.setDefaultValueForObject();
			}
			if(isCachingEnabled()) {
				addCacheEntry(contact.getId(), contact);
			}
			
			
		}
		return true;
	}
	
	@Override
	public ResultCodeConstant save(Contact contact) {
		ResultCodeConstant resultCodeConstant = validateData(contact);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		contact.setActive(false);
		if(contact.getContactNotesDetailsList() != null && !contact.getContactNotesDetailsList().isEmpty()) {
			contact.setCreatedOn(contact.getContactNotesDetailsList().get(0).getNotesDate());
		}
		contact = getRepository().save(contact);
		
		// save billing address
		Address billingAddress =  contact.getShopAddress();
		if (billingAddress != null) {
			billingAddress.setForeinKey(contact.getId());
			billingAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
			addressService.save(billingAddress);
		}
		
		Address shopAddress = new Address();
		shopAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
		shopAddress.setForeinKey(contact.getId());
		List<Address> shopAddressDataList = addressService.getAddressByContact(shopAddress);
		if (shopAddressDataList != null && !shopAddressDataList.isEmpty()) {
			contact.setShopAddress(shopAddressDataList.get(0));
		}
		
		// save contact notes details
		User user = userService.getByUserName(LoginServiceImpl.getCurrentLoginUserName());
		List<ContactNotesDetails> contactNotesDetailsList = contact.getContactNotesDetailsList();
		if(contactNotesDetailsList != null) {
			for(ContactNotesDetails contactNotesDetails : contactNotesDetailsList) {
				contactNotesDetails.setContactId(contact.getId());
				if(contactNotesDetails.getUserId() == 0) {
					contactNotesDetails.setUserId(user.getId());
				}
			}
			contactNotesDetailsService.saveOrUpdateAll(contactNotesDetailsList);
		}
		
		contact.setDefaultValueForObject();
		
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResultCodeConstant update(Contact contact) {
		ResultCodeConstant resultCodeConstant = validateData(contact);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		// update contact details
		Contact dataTemp  = getRepository().getById(contact.getId());
		contact.setCreatedBy(dataTemp.getCreatedBy());
		if(contact.getContactNotesDetailsList() != null && !contact.getContactNotesDetailsList().isEmpty()) {
			contact.setCreatedOn(contact.getContactNotesDetailsList().get(0).getNotesDate());
		} else {
			contact.setCreatedOn(dataTemp.getCreatedOn());
		}
		
		contact = getRepository().update(contact);
		
		// save billing address
		Address billingAddress =  contact.getShopAddress();
		if (billingAddress != null) {
			billingAddress.setForeinKey(contact.getId());
			billingAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
			addressService.update(billingAddress);
		}
		
		Address shopAddress = new Address();
		shopAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
		shopAddress.setForeinKey(contact.getId());
		List<Address> shopAddressDataList = addressService.getAddressByContact(shopAddress);
		if (shopAddressDataList != null && !shopAddressDataList.isEmpty()) {
			contact.setShopAddress(shopAddressDataList.get(0));
		}
		
		User user = userService.getByUserName(LoginServiceImpl.getCurrentLoginUserName());
		// update (first delete and then re-insert) contact notes details
		contactNotesDetailsService.deleteByContactId(contact.getId());
		List<ContactNotesDetails> contactNotesDetailsList = contact.getContactNotesDetailsList();
		if(contactNotesDetailsList != null) {
			for(ContactNotesDetails contactNotesDetails : contactNotesDetailsList) {
				contactNotesDetails.setContactId(contact.getId());
				if(contactNotesDetails.getUserId() == 0) {
					contactNotesDetails.setUserId(user.getId());
				}
			}
			contactNotesDetailsService.saveOrUpdateAll(contactNotesDetailsList);
		}
		
		contact.setDefaultValueForObject();
		
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResultCodeConstant saveOrUpdate(Contact contact) {
		if(contact.getId() != null && contact.getId() > 0) {
			return this.update(contact);
		}
		return this.save(contact);
	}
	
	@Override
	public List<Contact> saveOrUpdateAll(List<Contact> contactList) {
		// check if list is empty then return the result code with empty list message
		if(contactList == null  || contactList.isEmpty()) {
			return Collections.emptyList();
		}
		
		// validate all the data 
		for(Contact data : contactList) {
			ResultCodeConstant resultCodeConstant = validateData(data);
			if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
				return Collections.emptyList();
			}
		}
				
		// save the data list
		Iterator<Contact> iterator = contactList.iterator();
		List<Contact> result = new ArrayList<>();
		while (iterator.hasNext()) {
			Contact entity = iterator.next();
			
			if (entity.getId() == null) {
				this.save(entity);
			} else {
				this.update(entity);
			}
			result.add(entity);
		}
		return result;
	}

	@Override
	public ResponseEntity<IResponseData<Contact>> saveWithFile(RequestData<Contact, Long> requestData,
			MultipartFile[] uploadingFiles, String calledFrom) throws IOException {
		ResponseEntity<IResponseData<Contact>> responseEntity = save(requestData);

		if (ResultCodeConstant.SUCCESS.getResultCode() != responseEntity.getBody().getResponseCode().intValue()) {
			IResponseData<Contact> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(),ResultCodeConstant.getResultCodeConstant(responseEntity.getBody().getResponseCode()));
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}

		FilesData filesData = null;
		String filePath = null;
		String baseFilePath = null;
		Contact contact = responseEntity.getBody().getData();
		long contactId = contact.getId();
		for (MultipartFile uploadedFile : uploadingFiles) {
			baseFilePath = FilesDataPathConstant.CONTACT_IMAGES.getPath() + File.separator + FileUtils.getFileName(uploadedFile.getOriginalFilename());
			filePath = filesDataService.getBasePath() + File.separator + baseFilePath;
			File file = new File(filePath);
			uploadedFile.transferTo(file);

			// save files details
			filesData = new FilesData();
			filesData.setImagePath(baseFilePath);
			filesData.setForeinKey(contactId);
			filesData.setModuleName(FilesDataModuleName.CONTACT.getValue());
			filesDataService.save(filesData);
		}
		
		if(calledFrom.equals("App")) {
			IResponseData<Contact> responseData = responseEntity.getBody();
			SearchData<Contact> searchData = new SearchData<>();
			responseData.setDataList(getSearchActiveData(searchData, true));
		}
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> updateWithFile(RequestData<Contact, Long> requestData,
			MultipartFile[] uploadingFiles, String calledFrom) throws IOException {
		ResponseEntity<IResponseData<Contact>> responseEntity = update(requestData);

		if (ResultCodeConstant.SUCCESS.getResultCode() != responseEntity.getBody().getResponseCode().intValue()) {
			IResponseData<Contact> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(),ResultCodeConstant.getResultCodeConstant(responseEntity.getBody().getResponseCode()));
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		// delete existing file data
		FilesData filesDataDelete = new FilesData();
		filesDataDelete.setForeinKey(responseEntity.getBody().getData().getId());
		filesDataDelete.setModuleName(FilesDataModuleName.CONTACT.getValue());
		try {
			filesDataService.deleteFileData(filesDataDelete);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FilesData filesData = null;
		String filePath = null;
		String baseFilePath = null;
		long contactId = responseEntity.getBody().getData().getId();
		for (MultipartFile uploadedFile : uploadingFiles) {
			baseFilePath = FilesDataPathConstant.CONTACT_IMAGES.getPath() + File.separator + FileUtils.getFileName(uploadedFile.getOriginalFilename());
			filePath = filesDataService.getBasePath() + File.separator + baseFilePath;
			File file = new File(filePath);
			uploadedFile.transferTo(file);

			// save files details
			filesData = new FilesData();
			filesData.setImagePath(baseFilePath);
			filesData.setForeinKey(contactId);
			filesData.setModuleName(FilesDataModuleName.CONTACT.getValue());
			filesDataService.save(filesData);
		}
		
		if(calledFrom.equals("App")) {
			IResponseData<Contact> responseData = responseEntity.getBody();
			SearchData<Contact> searchData = new SearchData<>();
			responseData.setDataList(getSearchActiveData(searchData, true));
		}
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> getById(RequestData<Contact, Long> requestData) {
		Contact contact = this.getById(requestData.getId());
		
		IResponseData<Contact> responseData = new ResponseData<>(contact, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	
	@Override
	public Contact getById(Contact contact) {
		return getById(contact.getId());
	}
	
	@Override
	public Contact getById(Long contactId) {
		if(isCachingEnabled()) {
			Contact contactTemp = getCacheEntry(contactId);
			if(contactTemp != null) {
				return contactTemp;
			}
		}
		
		Contact contact = contactDao.getById(contactId);
		if(contact == null) {
			return null;
		}
		
		// set all pre-requisite data
		contact.setContactInitData(getContactInitData());
		
		Address shopAddress = new Address();
		shopAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
		List<Address> shopAddressDataList = null;
		FilesData filesData = new FilesData();
		filesData.setModuleName(FilesDataModuleName.CONTACT.getValue());
		// setting contact address details
		shopAddress.setForeinKey(contact.getId());
		shopAddressDataList = addressService.getAddressByContact(shopAddress);
		if (!shopAddressDataList.isEmpty()) {
			contact.setShopAddress(shopAddressDataList.get(0));
		}
		
		// set files data
		filesData.setForeinKey(contact.getId());
		try {
			contact.setFilesData(filesDataService.getFileData(filesData).getBody().getDataList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// set Contact Notes Details
		contact.setContactNotesDetailsList(contactNotesDetailsService.getByContactId(contact.getId()));
		return contact;
	}

	@Override
	public ResponseEntity<IResponseData<Contact>> save(RequestData<Contact, Long> requestData, String calledFrom) {
		ResponseEntity<IResponseData<Contact>> responseEntity = this.save(requestData);
		if(calledFrom.equals("App")) {
			IResponseData<Contact> responseData = responseEntity.getBody();
			SearchData<Contact> searchData = new SearchData<>();
			responseData.setDataList(getSearchActiveData(searchData, true));
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<IResponseData<Contact>> update(RequestData<Contact, Long> requestData, String calledFrom) {
		ResponseEntity<IResponseData<Contact>> responseEntity = this.update(requestData);
		if(calledFrom.equals("App")) {
			IResponseData<Contact> responseData = responseEntity.getBody();
			SearchData<Contact> searchData = new SearchData<>();
			responseData.setDataList(getSearchActiveData(searchData, true));
		}
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<IResponseData<Contact>> deleteData(RequestData<Contact, Long> requestData, String calledFrom) {
		ResponseEntity<IResponseData<Contact>> responseEntity = super.delete(requestData);
		if(calledFrom.equals("App")) {
			IResponseData<Contact> responseData = responseEntity.getBody();
			SearchData<Contact> searchData = new SearchData<>();
			responseData.setDataList(getSearchActiveData(searchData, true));
		}
		return responseEntity;
	}


	@Override
	public ResponseEntity<IResponseData<Contact>> getByMobile1Number(
			RequestData<SearchData<Contact>, Long> requestData) {
		SearchData<Contact> searchData = requestData.getData();
		if(searchData == null) {
			IResponseData<Contact> responseData = new ResponseData<>(null, getRepository().getNewDataList(),ResultCodeConstant.PARAMETER_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK); 
		}
		
		Contact searchContact =  searchData.getData();
		if(searchContact == null) {
			IResponseData<Contact> responseData = new ResponseData<>(null, getRepository().getNewDataList(),ResultCodeConstant.PARAMETER_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		List<Contact> contactList = getRepository().getByMobile1Number(searchContact.getMobile1());
		if(contactList == null || contactList.isEmpty()) {
			IResponseData<Contact> responseData = new ResponseData<>(searchContact, getRepository().getNewDataList(),ResultCodeConstant.RESULT_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		// set all data
		Contact contact = contactList.get(0);
		
		Address shopAddress = new Address();
		shopAddress.setModuleName(ModuleName.CONTACT_SHOP_ADDRESS.getValue());
		List<Address> shopAddressDataList = null;
		FilesData filesData = new FilesData();
		filesData.setModuleName(FilesDataModuleName.CONTACT.getValue());
		// setting contact address details
		shopAddress.setForeinKey(contact.getId());
		shopAddressDataList = addressService.getAddressByContact(shopAddress);
		if (!shopAddressDataList.isEmpty()) {
			contact.setShopAddress(shopAddressDataList.get(0));
		}
		
		// set files data
		filesData.setForeinKey(contact.getId());
		try {
			contact.setFilesData(filesDataService.getFileData(filesData).getBody().getDataList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// set Contact Notes Details
		contact.setContactNotesDetailsList(contactNotesDetailsService.getByContactId(contact.getId()));
		
		IResponseData<Contact> responseData = new ResponseData<>(contact, getRepository().getNewDataList(),ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	private ContactInitData getContactInitData() {
		ContactInitData contactInitData = new ContactInitData();
		SearchData<Country> searchDataCountry = new SearchData<>();
		contactInitData.setCountryList(countryService.getActiveData(searchDataCountry));
		
		SearchData<State> searchDataState = new SearchData<>();
		contactInitData.setStateList(stateService.getActiveData(searchDataState));
		
		SearchData<City> searchDataCity = new SearchData<>();
		contactInitData.setCityList(cityService.getActiveData(searchDataCity));
		
		return contactInitData;
	}

	@Override
	public ResponseEntity<IResponseData<Contact>> getCurrentOrderExistData(RequestData<SearchData<Contact>, Long> requestData) {
		List<Contact> contactList = getCurrentOrderExistData(requestData.getData());
		IResponseData<Contact> responseData = new ResponseData<>(getRepository().getNewData(), contactList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public List<Contact> getCurrentOrderExistData(SearchData<Contact> searchData){
		List<Contact> contactList = Collections.emptyList();
		if(contactList == null || contactList.isEmpty()) {
			contactList = contactDao.getCurrentOrderExistData(searchData);
			if(contactList != null) {
				for(Contact contact : contactList) {
					contact.setDefaultValueForObject();
				}
			}
			//setContactData(contactList);
		}
		return contactList;
	}

}

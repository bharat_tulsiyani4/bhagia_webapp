package com.aone.module.contact.service;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.contact.model.Contact;

public interface IContactService  extends IBaseMasterService<Contact, Long>{

	public ResponseEntity<IResponseData<Contact>> saveWithFile(RequestData<Contact, Long> requestData, MultipartFile[] uploadingFiles, String calledFrom) throws IOException;

	public ResponseEntity<IResponseData<Contact>> updateWithFile(RequestData<Contact, Long> requestData,
			MultipartFile[] uploadingFiles, String calledFrom) throws IOException;

	public ResponseEntity<IResponseData<Contact>> getSearchActiveData(RequestData<SearchData<Contact>, Long> requestData);

	public List<Contact> getSearchActiveData(SearchData<Contact> searchData, boolean loadFullData);

	public ResponseEntity<IResponseData<Contact>> save(RequestData<Contact, Long> requestData, String calledFrom);

	public ResponseEntity<IResponseData<Contact>> update(RequestData<Contact, Long> requestData, String calledFrom);

	public ResponseEntity<IResponseData<Contact>> deleteData(RequestData<Contact, Long> requestData, String calledFrom);

	public ResponseEntity<IResponseData<Contact>> getByMobile1Number(RequestData<SearchData<Contact>, Long> requestData);

	public ResponseEntity<IResponseData<Contact>> getCurrentOrderExistData(
			RequestData<SearchData<Contact>, Long> requestData);

	public List<Contact> getCurrentOrderExistData(SearchData<Contact> searchData);

}

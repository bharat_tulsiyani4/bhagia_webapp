package com.aone.module.contact.constant;

public class ContactDbColPojoConstant {

	public static final String COMPANY_NAME="company_name";
	public static final String MOBILE_1="mobile_1";
	public static final String MOBILE_2="mobile_2";
	public static final String FULL_NAME="full_name";
	public static final String EMAIL_ID="email_id";
	public static final String AMOUNT="amount";
	public static final String PRODUCT="product";
	public static final String REFERRED_BY="referred_by";
	public static final String DESCRIPTION="description";
	
	private ContactDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

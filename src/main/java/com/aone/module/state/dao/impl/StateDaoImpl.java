package com.aone.module.state.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.module.state.dao.IStateDao;
import com.aone.module.state.model.State;

@Lazy
@Repository("stateDaoImpl")
public class StateDaoImpl extends AbstractBaseMasterRepository<State, Long> implements IStateDao {

	@Override
	public Class<State> getClazz() {
		return State.class;
	}
	
	@Override
	public boolean isPublicModule() {
		return true;
	}

	@Override
	public State getNewData() {
		State state = new State();
		state.setDefaultValueForObject();
		return state;
	}

}

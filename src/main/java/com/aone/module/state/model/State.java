package com.aone.module.state.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.country.model.Country;
import com.aone.module.state.constant.StateDbColPojoConstant;

@Entity
@Table(name = "state_master")
public class State extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = StateDbColPojoConstant.NAME)
	private String name;
	
	@Column(name = StateDbColPojoConstant.COUNTRY_ID)
	private long countryId;

	@Column(name = StateDbColPojoConstant.CODE)
	private long code;

	@Column(name = StateDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private Country country;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.country == null)
			this.country = new Country();
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "State [name=" + name + ", countryId=" + countryId + ", code=" + code + ", description=" + description
				+ ", country=" + country + "]";
	}

}

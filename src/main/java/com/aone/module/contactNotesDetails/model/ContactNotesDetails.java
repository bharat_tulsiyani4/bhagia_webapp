package com.aone.module.contactNotesDetails.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterEntity;
import com.aone.module.contactNotesDetails.constant.ContactNotesDetailsDbColPojoConstant;

@Entity
@Table(name = "contact_notes_details")
public class ContactNotesDetails extends AbstractBaseMasterEntity<Long> {

	private static final long serialVersionUID = 7895104325035517069L;
	
	@Column(name = ContactNotesDetailsDbColPojoConstant.NOTES_DATE)
	private LocalDateTime notesDate;
	
	@Column(name = ContactNotesDetailsDbColPojoConstant.NOTES_DATE_FORMATTED)
	private String notesDateFormatted;
	
	@Column(name = ContactNotesDetailsDbColPojoConstant.CONTACT_ID)
	private long contactId;
	
	@Column(name = ContactNotesDetailsDbColPojoConstant.USER_ID)
	private long userId;
	
	@Column(name = ContactNotesDetailsDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private String userName;
	
	

	public LocalDateTime getNotesDate() {
		return notesDate;
	}

	public void setNotesDate(LocalDateTime notesDate) {
		this.notesDate = notesDate;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotesDateFormatted() {
		return notesDateFormatted;
	}

	public void setNotesDateFormatted(String notesDateFormatted) {
		this.notesDateFormatted = notesDateFormatted;
	}

	@Override
	public String toString() {
		return "ContactNotesDetails [notesDate=" + notesDate + ", notesDateFormatted=" + notesDateFormatted
				+ ", contactId=" + contactId + ", userId=" + userId + ", description=" + description + ", userName="
				+ userName + "]";
	}

}

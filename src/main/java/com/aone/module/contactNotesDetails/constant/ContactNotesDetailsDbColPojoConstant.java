package com.aone.module.contactNotesDetails.constant;

public class ContactNotesDetailsDbColPojoConstant {

	public static final String NOTES_DATE="notes_date";
	public static final String NOTES_DATE_FORMATTED="notes_date_formatted";
	public static final String CONTACT_ID="contact_id";
	public static final String USER_ID="user_id";
	public static final String DESCRIPTION ="description";
	
	private ContactNotesDetailsDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

package com.aone.module.contactNotesDetails.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.module.contactNotesDetails.controller.IContactNotesDetailsController;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;
import com.aone.module.contactNotesDetails.service.IContactNotesDetailsService;

@RestController
@RequestMapping("/rest/contactNotesDetails")
public class ContactNotesDetailsRestController extends AbstractBaseMasterRestController<ContactNotesDetails , Long, IContactNotesDetailsService> implements IContactNotesDetailsController {
	
	private IContactNotesDetailsService contactNotesDetailsService;
	
	@Override
	public IContactNotesDetailsService getService() {
		return getContactNotesDetailsService();
	}

	public IContactNotesDetailsService getContactNotesDetailsService() {
		return contactNotesDetailsService;
	}

	@Autowired        
	@Qualifier(value="contactNotesDetailsServiceImpl")
	public void setContactNotesDetailsService(IContactNotesDetailsService contactNotesDetailsService) {
		this.contactNotesDetailsService = contactNotesDetailsService;
	}

}

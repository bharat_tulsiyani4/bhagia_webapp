package com.aone.module.contactNotesDetails.controller;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;

public interface IContactNotesDetailsController extends IBaseMasterController<ContactNotesDetails , Long>{

}

package com.aone.module.contactNotesDetails.dao;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;

public interface IContactNotesDetailsDao extends IBaseMasterRepository<ContactNotesDetails, Long>{

	public int deleteByContactId(long contactId);

}

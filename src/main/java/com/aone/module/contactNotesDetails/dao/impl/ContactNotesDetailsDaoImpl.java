package com.aone.module.contactNotesDetails.dao.impl;

import javax.persistence.Query;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.module.contactNotesDetails.dao.IContactNotesDetailsDao;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;

@Lazy
@Repository("contactNotesDetailsDaoImpl")
public class ContactNotesDetailsDaoImpl extends AbstractBaseMasterRepository<ContactNotesDetails, Long> implements IContactNotesDetailsDao {

	@Override
	public Class<ContactNotesDetails> getClazz() {
		return ContactNotesDetails.class;
	}
	
	@Override
	@Transactional(readOnly = false)
	public int deleteByContactId(long contactId) {
		StringBuilder queryBuilder = new StringBuilder(70);
		queryBuilder.append("UPDATE ");
		queryBuilder.append(getClazz().getName());
		queryBuilder.append(" SET deleted = :deleted ");
		queryBuilder.append(" where contactId = :contactId");
		Query query = getEntityManager().createQuery(queryBuilder.toString());
		query.setParameter("contactId", contactId);
		query.setParameter("deleted", true);
		return query.executeUpdate();
	}

	@Override
	public ContactNotesDetails getNewData() {
		ContactNotesDetails contactNotesDetails = new ContactNotesDetails();
		contactNotesDetails.setDefaultValueForObject();
		return contactNotesDetails;
	}

}

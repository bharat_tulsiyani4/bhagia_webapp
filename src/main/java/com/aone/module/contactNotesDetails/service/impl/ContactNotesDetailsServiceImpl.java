package com.aone.module.contactNotesDetails.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.contactNotesDetails.dao.IContactNotesDetailsDao;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;
import com.aone.module.contactNotesDetails.service.IContactNotesDetailsService;
import com.aone.module.user.model.User;
import com.aone.module.user.service.IUserService;

@Lazy
@Service("contactNotesDetailsServiceImpl")
public class ContactNotesDetailsServiceImpl extends AbstractBaseMasterService<ContactNotesDetails , Long, IContactNotesDetailsDao> implements
		IContactNotesDetailsService {
	
	private IContactNotesDetailsDao contactNotesDetailsDao;
	private IUserService userService;
	
	public IUserService getUserService() {
		return userService;
	}

	@Autowired        
	@Qualifier(value="userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public IContactNotesDetailsDao getRepository() {
		return getContactNotesDetailsDao();
	}

	public IContactNotesDetailsDao getContactNotesDetailsDao() {
		return contactNotesDetailsDao;
	}

	@Autowired
	@Qualifier(value = "contactNotesDetailsDaoImpl")
	public void setContactNotesDetailsDao(IContactNotesDetailsDao contactNotesDetailsDao) {
		this.contactNotesDetailsDao = contactNotesDetailsDao;
	}
	
	@Override
	public List<ContactNotesDetails> getByContactId(long contactId){
		Map<String, Object> params = new HashMap<>();
		params.put("contactId", contactId);
		params.put("deleted", false);
		List<ContactNotesDetails> contactNotesDetailsList = getRepository().getByQueryData(params, Collections.emptyMap());
		User user = null;
		Map<Long, User> userIdWiseMap = new HashMap<>();
		for(ContactNotesDetails contactNotesDetails : contactNotesDetailsList) {
			user = userIdWiseMap.get(contactNotesDetails.getUserId());
			if(user == null) {
				user = userService.getById(contactNotesDetails.getUserId());
				userIdWiseMap.put(user.getId(), user);
			}
			contactNotesDetails.setUserName(user.getName());
		}
		return contactNotesDetailsList;
	}
	
	@Override
	public int deleteByContactId(long contactId){
		return getRepository().deleteByContactId(contactId);
	}
	
}
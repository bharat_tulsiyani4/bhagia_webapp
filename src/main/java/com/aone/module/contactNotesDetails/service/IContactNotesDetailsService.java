package com.aone.module.contactNotesDetails.service;

import java.util.List;

import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.contactNotesDetails.model.ContactNotesDetails;

public interface IContactNotesDetailsService  extends IBaseMasterService<ContactNotesDetails , Long>{

	public List<ContactNotesDetails> getByContactId(long contactId);

	public int deleteByContactId(long contactId);

}

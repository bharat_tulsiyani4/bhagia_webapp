package com.aone.module.salesDetails.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.salesDetails.constant.SalesDetailsDbColPojoConstant;

@Entity
@Table(name = "sales_details")
public class SalesDetails extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = 7895104325035517069L;
	
	@Column(name = SalesDetailsDbColPojoConstant.SALES_ID)
	private long salesId;
	
	@Column(name = SalesDetailsDbColPojoConstant.TRANSACTION_DATE)
	private LocalDateTime transactionDate;
	
	@Column(name = SalesDetailsDbColPojoConstant.ITEM_NAME)
	private String itemName;
	
	@Column(name = SalesDetailsDbColPojoConstant.QUANTITY)
	private double quantity;
	
	@Column(name = SalesDetailsDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private int contactId;
	
	@Transient
	private long desktopSalesOrderId;

	public long getSalesId() {
		return salesId;
	}

	public void setSalesId(long salesId) {
		this.salesId = salesId;
	}

	public String getItemName() {                                                      
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}
	
	public long getDesktopSalesOrderId() {
		return desktopSalesOrderId;
	}

	public void setDesktopSalesOrderId(long desktopSalesOrderId) {
		this.desktopSalesOrderId = desktopSalesOrderId;
	}

	@Override
	public String toString() {
		return "SalesDetails [ id=" + this.getId() + ", salesId=" + salesId + ", transactionDate=" + transactionDate + ", itemName=" + itemName
				+ ", quantity=" + quantity + ", description=" + description + ", contactId=" + contactId
				+ ", desktopSalesOrderId=" + desktopSalesOrderId + "]";
	}

}

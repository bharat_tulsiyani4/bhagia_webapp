package com.aone.module.salesDetails.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.commons.exceptions.AuthenticationFailedException;
import com.aone.commons.exceptions.OperationFailedException;
import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.item.service.IItemService;
import com.aone.module.sales.model.Sales;
import com.aone.module.sales.service.ISalesService;
import com.aone.module.salesDetails.dao.ISalesDetailsDao;
import com.aone.module.salesDetails.model.SalesDetails;
import com.aone.module.salesDetails.service.ISalesDetailsService;
import com.aone.module.salesOrderDetails.service.ISalesOrderDetailsService;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;
import com.aone.module.salesOrderDetailsQtyUpdateLog.service.ISalesOrderDetailsQtyUpdateLogService;

@Lazy
@Service("salesDetailsServiceImpl")
public class SalesDetailsServiceImpl extends AbstractBaseMasterService<SalesDetails , Long, ISalesDetailsDao> implements
		ISalesDetailsService {
	
	private ISalesDetailsDao salesDetailsDao;
	private IItemService itemService;
	private ISalesOrderDetailsService salesOrderDetailsService;
	private ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService;
	private ISalesService salesService;
	
	public ISalesService getSalesService() {
		return salesService;
	}

	@Autowired        
	@Qualifier(value="salesServiceImpl")
	public void setSalesService(ISalesService salesService) {
		this.salesService = salesService;
	}
	
	public ISalesOrderDetailsQtyUpdateLogService getSalesOrderDetailsQtyUpdateLogService() {
		return salesOrderDetailsQtyUpdateLogService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsQtyUpdateLogServiceImpl")
	public void setSalesOrderDetailsQtyUpdateLogService(ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService) {
		this.salesOrderDetailsQtyUpdateLogService = salesOrderDetailsQtyUpdateLogService;
	}
	
	public ISalesOrderDetailsService getSalesOrderDetailsService() {
		return salesOrderDetailsService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsServiceImpl")
	public void setSalesOrderDetailsService(ISalesOrderDetailsService salesOrderDetailsService) {
		this.salesOrderDetailsService = salesOrderDetailsService;
	}
	
	public IItemService getItemService() {
		return itemService;
	}

	@Autowired        
	@Qualifier(value="itemServiceImpl")
	public void setItemService(IItemService itemService) {
		this.itemService = itemService;
	}
	
	@Override
	public ISalesDetailsDao getRepository() {
		return getSalesDetailsDao();
	}

	public ISalesDetailsDao getSalesDetailsDao() {
		return salesDetailsDao;
	}

	@Autowired
	@Qualifier(value = "salesDetailsDaoImpl")
	public void setSalesDetailsDao(ISalesDetailsDao salesDetailsDao) {
		this.salesDetailsDao = salesDetailsDao;
	}
	
	@Override
	public ResultCodeConstant validateData(SalesDetails salesDetails) {
		if(salesDetails == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		
		// validate name
		if(salesDetails.getItemName() == null || salesDetails.getItemName().isEmpty()) {
			return ResultCodeConstant.SALES_DETAILS_ITEM_NAME_NOT_FOUND;
		}
		
		if(salesDetails.getQuantity() <= 0) {
			return ResultCodeConstant.SALES_DETAILS_ITEM_QTY_NOT_FOUND;
		}
		
		if(salesDetails.getSalesId() <= 0) {
			return ResultCodeConstant.SALES_DETAILS_SALES_ID_NOT_FOUND;
		}
		
		return ResultCodeConstant.SUCCESS;
	}

	@Override
	public List<SalesDetails> getSalesDetailsBySalesId(long salesId) {
		return getSalesDetailsDao().getSalesDetailsBySalesId(salesId);
	}

	@Override
	public int deleteSalesDetailsBySalesId(long salesId) {
		return getSalesDetailsDao().deleteSalesDetailsBySalesId(salesId);
	}


	@Override
	public List<SalesDetails> getBySalesId(long salesId){
		Map<String, Object> params = new HashMap<>();
		params.put("salesId", salesId);
		params.put("deleted", false);
		return getRepository().getByQueryData(params, Collections.emptyMap());
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesDetails>> delete(RequestData<SalesDetails,Long> requestData) {
		SalesDetails salesDetails  = requestData.getData();
		ResultCodeConstant resultCodeConstant = validateData(salesDetails);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			IResponseData<SalesDetails> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), resultCodeConstant);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		Long salesId = salesDetails.getSalesId();
		int count = this.delete(salesDetails);
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		if(isCachingEnabled()) {
			removeCacheEntry(requestData.getData().getId());
		}
		
		List<SalesDetails> salesDetailsList = salesDetailsDao.getSalesDetailsBySalesId(salesId);
		
		IResponseData<SalesDetails> responseData = new ResponseData<>(requestData.getData(), salesDetailsList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public Integer delete(SalesDetails salesDetails) {
		int count = salesDetailsDao.delete(salesDetails);
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		// revert all qty details 
		revertRemainingQtyDetailsBySalesId(salesDetails);
		
		return count;
	}
	
	private boolean revertRemainingQtyDetailsBySalesId(SalesDetails salesDetails) {
		// revert stock details based on previous sales details data
		List<SalesOrderDetailsQtyUpdateLog> currentQtyUpdateLogsList = salesOrderDetailsQtyUpdateLogService.getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(salesDetails.getId());
		if(currentQtyUpdateLogsList != null) {
			for(SalesOrderDetailsQtyUpdateLog qtyUpdateLog : currentQtyUpdateLogsList) {
				salesOrderDetailsService.updateRemainingQty(qtyUpdateLog.getSalesOrderDetailsId(), qtyUpdateLog.getQuantity());
			}
		}
		salesOrderDetailsQtyUpdateLogService.deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(salesDetails.getId());
		return true;
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesDetails>> save(RequestData<SalesDetails, Long> requestData) {
		// save sales master
		SalesDetails salesDetails = requestData.getData();
        
        ResultCodeConstant resultCodeConstant = this.saveAndUpdateStock(salesDetails, false, true, true);
        
        List<SalesDetails> salesDetailsList = salesDetailsDao.getSalesDetailsBySalesId(salesDetails.getSalesId());
		
		IResponseData<SalesDetails> responseData = new ResponseData<>(salesDetails, salesDetailsList, resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant saveAndUpdateStock(SalesDetails salesDetails, boolean saveSalesDetails, boolean saveContact, boolean updateStock) {
		if(saveSalesDetails) {
			this.save(salesDetails);	
		}
		
		Sales sales = null;
		if(saveContact) {
			sales = salesService.setContactId(salesDetails.getSalesId(), salesDetails.getContactId(), salesDetails.getDesktopSalesOrderId());
			if(sales == null) {
				return ResultCodeConstant.SALES_DETAILS_UPDATE_CONTACT_ID_ERROR;
			}
			// save sales details
			if(updateStock) {
				updateRemainingQtyStockDetails(salesDetails, sales);	
			}
		}
		
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResultCodeConstant save(SalesDetails salesDetails) {
		ResultCodeConstant resultCodeConstant = validateData(salesDetails);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
							
		long agentId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(agentId == 0) {
			throw new AuthenticationFailedException("Login user not found");
		}
		// save sales details
		salesDetails = getRepository().save(salesDetails);
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public ResultCodeConstant update(SalesDetails salesDetails) {
		// as need to revert the stock and update, 
		// which is currently handled by delete and create new
		// so update operation is not allowed
		return ResultCodeConstant.INTERNAL_ERROR;
	}
	
	private boolean updateRemainingQtyStockDetails(SalesDetails salesDetails, Sales sales) {
		List<SalesDetails> details = new ArrayList<>();
		details.add(salesDetails);
		sales.setSalesDetailsList(details);
		
		return salesService.updateRemainingQtyStockDetails(sales);
	}
	
}
package com.aone.module.salesDetails.service;

import java.util.List;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.salesDetails.model.SalesDetails;

public interface ISalesDetailsService  extends IBaseMasterService<SalesDetails , Long>{

	public List<SalesDetails> getSalesDetailsBySalesId(long salesId);
	
	public int deleteSalesDetailsBySalesId(long salesId);
	
	public List<SalesDetails> getBySalesId(long salesId);

	public ResultCodeConstant saveAndUpdateStock(SalesDetails salesDetails, boolean saveSalesDetails, boolean saveContact, boolean updateStock);
}

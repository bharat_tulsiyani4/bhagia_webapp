package com.aone.module.salesDetails.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.salesDetails.model.SalesDetails;

public interface ISalesDetailsDao extends IBaseMasterRepository<SalesDetails , Long>{

	public List<SalesDetails> getSalesDetailsBySalesId(long salesId);

	public int deleteSalesDetailsBySalesId(long salesId);

}

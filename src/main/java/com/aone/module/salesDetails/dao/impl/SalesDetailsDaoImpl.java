package com.aone.module.salesDetails.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.module.salesDetails.dao.ISalesDetailsDao;
import com.aone.module.salesDetails.model.SalesDetails;

@Lazy
@Repository("salesDetailsDaoImpl")
public class SalesDetailsDaoImpl extends AbstractBaseMasterRepository<SalesDetails, Long> implements ISalesDetailsDao {

	@Override
	public Class<SalesDetails> getClazz() {
		return SalesDetails.class;
	}
	
	@Override
	public List<SalesDetails> getSalesDetailsBySalesId(long salesId){
		Map<String, Object> params = new HashMap<>();
		params.put("deleted", false);
		params.put("salesId", salesId);
		return super.getByQueryData(params, Collections.emptyMap());
	}

	@Override
	public int deleteSalesDetailsBySalesId(long salesId) {
		String whereClause = " WHERE salesId = :salesId";
		Map<String, Object> params = new HashMap<>();
		params.put("salesId", salesId);
		return super.deleteByQuery(whereClause, params);
	}

	@Override
	public SalesDetails getNewData() {
		SalesDetails salesDetails = new SalesDetails();
		salesDetails.setDefaultValueForObject();
		return salesDetails;
	}

}

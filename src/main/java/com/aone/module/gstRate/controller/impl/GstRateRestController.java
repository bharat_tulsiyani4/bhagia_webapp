package com.aone.module.gstRate.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.gstRate.controller.IGstRateController;
import com.aone.module.gstRate.model.GstRate;
import com.aone.module.gstRate.service.IGstRateService;

@RestController
@RequestMapping("/rest/gstRate")
//@SecurityClassRole(baseRoleName = SecurityRoleModule.GST_RATE)
public class GstRateRestController extends AbstractBaseMasterRestController<GstRate , Long, IGstRateService > implements IGstRateController {
	
	private IGstRateService gstRateService;
	
	@Override
	public IGstRateService getService() {
		return getGstRateService();
	}

	public IGstRateService getGstRateService() {
		return gstRateService;
	}

	@Autowired        
	@Qualifier(value="gstRateServiceImpl")
	public void setGstRateService(IGstRateService gstRateService) {
		this.gstRateService = gstRateService;
	}
	
	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<IResponseData<GstRate>> getActiveData(@RequestBody RequestData<SearchData<GstRate>, Long> requestData) {
		return super.getActiveData(requestData);
	}
	
}

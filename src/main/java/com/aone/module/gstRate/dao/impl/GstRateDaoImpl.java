package com.aone.module.gstRate.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.gstRate.dao.IGstRateDao;
import com.aone.module.gstRate.model.GstRate;

@Lazy
@Repository("gstRateDaoImpl")
public class GstRateDaoImpl extends AbstractBaseMasterRepository<GstRate , Long> implements IGstRateDao {

	@Override
	public Class<GstRate> getClazz() {
		return GstRate.class;
	}
	

	@Override
	@Transactional(readOnly = true)
	public List<GstRate> getActiveData(SearchData<GstRate> searchData) {
		TypedQuery<GstRate> query = getQueryObject(searchData, false, false, getClazz());
		query.setParameter("deleted", false);
		if (searchData != null) {
			GstRate gstRate = searchData.getData();
			if (gstRate != null) {
				if (gstRate.getName() != null && !gstRate.getName().isEmpty()) {
					query.setParameter("name", "%" + gstRate.getName() + "%");
				}
			}

		}
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Long getActiveDataCount(SearchData<GstRate> searchData) {

		if (!isPublicModule()) {
		
		}
		TypedQuery<Long> query = getQueryObject(searchData, false, true, Long.class);
		query.setParameter("deleted", false);
		if (searchData != null) {
			GstRate gstRate = searchData.getData();
			if (gstRate != null) {
				if (gstRate.getName() != null && !gstRate.getName().isEmpty()) {
					query.setParameter("name", "%" + gstRate.getName() + "%");
				}
			}

		}
		return query.getSingleResult();
	}

	@Override
	protected <R> TypedQuery<R> getQueryObject(SearchData<GstRate> searchData, boolean isGetAll, boolean isCountQuery,
			Class<R> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);

		if (isCountQuery) {
			queryBuilder.append(SELECT_COUNT_PREFIX);
		} else {
			queryBuilder.append(SELECT_PREFIX);
		}
		queryBuilder.append(getClazz().getName() + " e Where 1=1  ");
		if (!isGetAll) {
			if (isPublicModule()) {
				queryBuilder.append("  AND e.deleted = :deleted ");
			} else {
				/* queryBuilder.append(" e.createdBy = :createdBy AND "); */
				if (searchData != null) {
					GstRate gstRate = searchData.getData();
					if (gstRate != null) {
						if (gstRate.getName() != null && !gstRate.getName().isEmpty()) {
							queryBuilder.append(" AND e.name like :name ");
						}					}
					queryBuilder.append(" AND e.deleted = :deleted ");
				}
			}
		}
		if (searchData != null) {
			if (searchData.getOrderingData() != null && !isCountQuery) {
				queryBuilder.append(" order by e." + searchData.getOrderingData().getColumn());
				if (searchData.getOrderingData().isDescending()) {
					queryBuilder.append(" desc ");
				}
			}
		}
		TypedQuery<R> query = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if (searchData != null) {
			if (searchData.getPaginationData() != null && !isCountQuery) {
				query.setFirstResult(searchData.getPaginationData().getRowsPerPage()
						* (searchData.getPaginationData().getPageNumber() - 1));
				query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
			}
		}
		return query;
	}


	@Override
	public GstRate getNewData() {
		return new GstRate();
	}	

}

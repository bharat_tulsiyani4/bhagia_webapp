package com.aone.module.salesOrderDetailsQtyUpdateLog.controller;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;

public interface ISalesOrderDetailsQtyUpdateLogController extends IBaseMasterController<SalesOrderDetailsQtyUpdateLog , Long>{

}

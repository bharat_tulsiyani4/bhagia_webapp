package com.aone.module.salesOrderDetailsQtyUpdateLog.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.module.salesOrderDetailsQtyUpdateLog.controller.ISalesOrderDetailsQtyUpdateLogController;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;
import com.aone.module.salesOrderDetailsQtyUpdateLog.service.ISalesOrderDetailsQtyUpdateLogService;

@RestController
@RequestMapping("/rest/salesOrderDetailsQtyUpdateLog")
public class SalesOrderDetailsQtyUpdateLogRestController extends AbstractBaseMasterRestController<SalesOrderDetailsQtyUpdateLog , Long, ISalesOrderDetailsQtyUpdateLogService> implements ISalesOrderDetailsQtyUpdateLogController {
	
	private ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService;
	
	@Override
	public ISalesOrderDetailsQtyUpdateLogService getService() {
		return getSalesOrderDetailsQtyUpdateLogService();
	}

	public ISalesOrderDetailsQtyUpdateLogService getSalesOrderDetailsQtyUpdateLogService() {
		return salesOrderDetailsQtyUpdateLogService;
	}

	@Autowired        
	@Qualifier(value="salesOrderDetailsQtyUpdateLogServiceImpl")
	public void setSalesOrderDetailsQtyUpdateLogService(ISalesOrderDetailsQtyUpdateLogService salesOrderDetailsQtyUpdateLogService) {
		this.salesOrderDetailsQtyUpdateLogService = salesOrderDetailsQtyUpdateLogService;
	}

}

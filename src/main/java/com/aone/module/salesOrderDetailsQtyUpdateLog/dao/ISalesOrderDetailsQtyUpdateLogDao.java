package com.aone.module.salesOrderDetailsQtyUpdateLog.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;

public interface ISalesOrderDetailsQtyUpdateLogDao extends IBaseMasterRepository<SalesOrderDetailsQtyUpdateLog , Long>{

	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesId(long salesId);

	public int deleteSalesOrderDetailsQtyUpdateLogBySalesId(long salesId);

	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId);

	public int deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId);
}

package com.aone.module.salesOrderDetailsQtyUpdateLog.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.module.salesOrderDetailsQtyUpdateLog.dao.ISalesOrderDetailsQtyUpdateLogDao;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;

@Lazy
@Repository("salesOrderDetailsQtyUpdateLogDaoImpl")
public class SalesOrderDetailsQtyUpdateLogDaoImpl extends AbstractBaseMasterRepository<SalesOrderDetailsQtyUpdateLog , Long> implements ISalesOrderDetailsQtyUpdateLogDao {

	
	@Override
	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesId(long salesId){
		Map<String, Object> params = new HashMap<>();
		params.put("deleted", false);
		params.put("salesId", salesId);
		return super.getByQueryData(params, Collections.emptyMap());
	}

	@Override
	public int deleteSalesOrderDetailsQtyUpdateLogBySalesId(long salesId) {
		String whereClause = " WHERE salesId = :salesId";
		Map<String, Object> params = new HashMap<>();
		params.put("salesId", salesId);
		return super.deleteByQuery(whereClause, params);
	}
	
	@Override
	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId){
		Map<String, Object> params = new HashMap<>();
		params.put("deleted", false);
		params.put("salesDetailsId", salesDetailsId);
		return super.getByQueryData(params, Collections.emptyMap());
	}

	@Override
	public int deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId) {
		String whereClause = " WHERE salesDetailsId = :salesDetailsId";
		Map<String, Object> params = new HashMap<>();
		params.put("salesDetailsId", salesDetailsId);
		return super.deleteByQuery(whereClause, params);
	}
	
	@Override
	public Class<SalesOrderDetailsQtyUpdateLog> getClazz() {
		return SalesOrderDetailsQtyUpdateLog.class;
	}

	@Override
	public SalesOrderDetailsQtyUpdateLog getNewData() {
		return new SalesOrderDetailsQtyUpdateLog();
	}

}

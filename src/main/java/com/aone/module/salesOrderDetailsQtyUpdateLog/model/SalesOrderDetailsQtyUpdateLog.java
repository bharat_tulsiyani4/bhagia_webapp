package com.aone.module.salesOrderDetailsQtyUpdateLog.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.aone.config.hibernateConvertors.CustomDoubleConvertor;
import com.aone.corelibrary.model.impl.AbstractBaseMasterEntity;
import com.aone.module.salesOrderDetailsQtyUpdateLog.constant.SalesOrderDetailsQtyUpdateLogDbColPojoConstant;

@Entity
@Table(name = "sales_order_details_qty_update_log")
public class SalesOrderDetailsQtyUpdateLog extends AbstractBaseMasterEntity<Long> {

	private static final long serialVersionUID = 2720214281416012405L;

	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.SALES_ID)
	private long salesId;
	
	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.SALES_DETAILS_ID)
	private long salesDetailsId;
	
	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.SALES_ORDER_DETAILS_ID)
	private long salesOrderDetailsId;
	
	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.DATE)
	private LocalDateTime date;

	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.ITEM_NAME)
	private String itemName;

	@Column(name = SalesOrderDetailsQtyUpdateLogDbColPojoConstant.QUANTITY)
	@Convert(converter=CustomDoubleConvertor.class)
	private double quantity;

	public long getSalesId() {
		return salesId;
	}

	public void setSalesId(long salesId) {
		this.salesId = salesId;
	}

	public long getSalesDetailsId() {
		return salesDetailsId;
	}

	public void setSalesDetailsId(long salesDetailsId) {
		this.salesDetailsId = salesDetailsId;
	}
	
	public long getSalesOrderDetailsId() {
		return salesOrderDetailsId;
	}

	public void setSalesOrderDetailsId(long salesOrderDetailsId) {
		this.salesOrderDetailsId = salesOrderDetailsId;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "SalesOrderDetailsQtyUpdateLog [salesId=" + salesId + ", salesDetailsId=" + salesDetailsId
				+ ", salesOrderDetailsId=" + salesOrderDetailsId + ", date=" + date + ", itemName=" + itemName
				+ ", quantity=" + quantity + "]";
	}

}

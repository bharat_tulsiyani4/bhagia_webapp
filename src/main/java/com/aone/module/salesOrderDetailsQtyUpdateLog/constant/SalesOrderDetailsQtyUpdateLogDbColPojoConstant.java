package com.aone.module.salesOrderDetailsQtyUpdateLog.constant;

public class SalesOrderDetailsQtyUpdateLogDbColPojoConstant {
	
	public static final String DATE="date";
	public static final String SALES_ID="sales_id";
	public static final String SALES_DETAILS_ID="sales_details_id";
	public static final String SALES_ORDER_DETAILS_ID="sales_order_details_id";
	public static final String ITEM_NAME="item_name";
	public static final String QUANTITY="quantity";
	
	private SalesOrderDetailsQtyUpdateLogDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

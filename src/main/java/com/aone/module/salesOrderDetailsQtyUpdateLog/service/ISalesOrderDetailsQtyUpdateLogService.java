package com.aone.module.salesOrderDetailsQtyUpdateLog.service;

import java.util.List;

import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;

public interface ISalesOrderDetailsQtyUpdateLogService  extends IBaseMasterService<SalesOrderDetailsQtyUpdateLog , Long>{

	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesId(long salesId);

	public int deleteSalesOrderDetailsQtyUpdateLogBySalesId(long salesId);

	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId);

	public int deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId);
}

package com.aone.module.salesOrderDetailsQtyUpdateLog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.salesOrderDetailsQtyUpdateLog.dao.ISalesOrderDetailsQtyUpdateLogDao;
import com.aone.module.salesOrderDetailsQtyUpdateLog.model.SalesOrderDetailsQtyUpdateLog;
import com.aone.module.salesOrderDetailsQtyUpdateLog.service.ISalesOrderDetailsQtyUpdateLogService;

@Lazy
@Service("salesOrderDetailsQtyUpdateLogServiceImpl")
public class SalesOrderDetailsQtyUpdateLogServiceImpl extends AbstractBaseMasterService<SalesOrderDetailsQtyUpdateLog , Long, ISalesOrderDetailsQtyUpdateLogDao> implements
		ISalesOrderDetailsQtyUpdateLogService {
	
	private ISalesOrderDetailsQtyUpdateLogDao salesOrderDetailsQtyUpdateLogDao;

	@Override
	public ISalesOrderDetailsQtyUpdateLogDao getRepository() {
		return getSalesOrderDetailsQtyUpdateLogDao();
	}

	public ISalesOrderDetailsQtyUpdateLogDao getSalesOrderDetailsQtyUpdateLogDao() {
		return salesOrderDetailsQtyUpdateLogDao;
	}

	@Autowired
	@Qualifier(value = "salesOrderDetailsQtyUpdateLogDaoImpl")
	public void setSalesOrderDetailsQtyUpdateLogDao(ISalesOrderDetailsQtyUpdateLogDao salesOrderDetailsQtyUpdateLogDao) {
		this.salesOrderDetailsQtyUpdateLogDao = salesOrderDetailsQtyUpdateLogDao;
	}

	@Override
	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesId(long salesId) {
		return salesOrderDetailsQtyUpdateLogDao.getSalesOrderDetailsQtyUpdateLogBySalesId(salesId);
	}

	@Override
	public int deleteSalesOrderDetailsQtyUpdateLogBySalesId(long salesId) {
		return salesOrderDetailsQtyUpdateLogDao.deleteSalesOrderDetailsQtyUpdateLogBySalesId(salesId);
	}
	
	@Override
	public List<SalesOrderDetailsQtyUpdateLog> getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId) {
		return salesOrderDetailsQtyUpdateLogDao.getSalesOrderDetailsQtyUpdateLogBySalesDetailsId(salesDetailsId);
	}

	@Override
	public int deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(long salesDetailsId) {
		return salesOrderDetailsQtyUpdateLogDao.deleteSalesOrderDetailsQtyUpdateLogBySalesDetailsId(salesDetailsId);
	}
}
package com.aone.module.location.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.module.location.model.Location;

public interface ILocationController extends IBaseMasterController<Location , Long>{

	public @ResponseBody ResponseEntity<IResponseData<Boolean>> checkLocationName(@RequestBody RequestData<Location,Long> requestData);
}

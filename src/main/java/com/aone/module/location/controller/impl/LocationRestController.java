package com.aone.module.location.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.location.controller.ILocationController;
import com.aone.module.location.model.Location;
import com.aone.module.location.service.ILocationService;

@RestController
@RequestMapping("/rest/location")
//@SecurityClassRole(baseRoleName = SecurityRoleModule.LOCATION)
public class LocationRestController extends AbstractBaseMasterRestController<Location , Long, ILocationService> implements ILocationController {
	
	private ILocationService locationService;
	
	@Override
	public ILocationService getService() {
		return getLocationService();
	}

	public ILocationService getLocationService() {
		return locationService;
	}

	@Autowired        
	@Qualifier(value="locationServiceImpl")
	public void setLocationService(ILocationService locationService) {
		this.locationService = locationService;
	}
	
	@Override
	@RequestMapping(value="/checkLocationName", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "SAVE", "UPDATE" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<Boolean>> checkLocationName(@RequestBody RequestData<Location,Long> requestData) {
		return getService().checkLocationName(requestData);
	}

	@Override
	@RequestMapping(value="/getActiveData", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<IResponseData<Location>> getActiveData(@RequestBody RequestData<SearchData<Location>, Long> requestData) {
		return super.getActiveData(requestData);
	}
	
}

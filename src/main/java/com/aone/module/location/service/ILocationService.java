package com.aone.module.location.service;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.location.model.Location;

public interface ILocationService  extends IBaseMasterService<Location , Long>{

	public ResponseEntity<IResponseData<Boolean>> checkLocationName(RequestData<Location, Long> requestData);
}

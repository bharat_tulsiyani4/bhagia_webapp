package com.aone.module.location.service.impl;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.location.dao.ILocationDao;
import com.aone.module.location.model.Location;
import com.aone.module.location.service.ILocationService;

@Lazy
@Service("locationServiceImpl")
public class LocationServiceImpl extends AbstractBaseMasterService<Location, Long, ILocationDao>
		implements ILocationService {
	
	private ILocationDao locationDao;

	@Override
	public ILocationDao getRepository() {
		return getLocationDao();
	}

	public ILocationDao getLocationDao() {
		return locationDao;
	}

	@Autowired
	@Qualifier(value = "locationDaoImpl")
	public void setLocationDao(ILocationDao locationDao) {
		this.locationDao = locationDao;
	}
	
	@Override
	public ResponseEntity<IResponseData<Boolean>> checkLocationName(RequestData<Location, Long> requestData) {
		IResponseData<Boolean> responseData = new ResponseData<>(!getRepository().checkUniqueDataByField("name", requestData.getData().getName(), "id", requestData.getData().getId()).isEmpty(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant validateData(Location location) {
		if(location == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		
		// validate name
		/*if(location.getName() == null || location.getName().isEmpty()) {
			return ResultCodeConstant.LOCATION_NAME_NOT_FOUND;
		}
		// check if category name already exist
		if(!getRepository().checkUniqueDataByField("name", location.getName(), "id", location.getId()).isEmpty()) {
			return ResultCodeConstant.LOCATION_ALREADY_EXIST;
		}*/
		return ResultCodeConstant.SUCCESS;
	}
	
}
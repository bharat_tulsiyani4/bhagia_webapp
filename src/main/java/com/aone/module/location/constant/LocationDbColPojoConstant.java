package com.aone.module.location.constant;

public class LocationDbColPojoConstant {

	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String SYSTEM_INSERTED = "system_inserted";

	private LocationDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

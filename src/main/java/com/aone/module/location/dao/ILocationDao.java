package com.aone.module.location.dao;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.module.location.model.Location;

public interface ILocationDao extends IBaseMasterRepository<Location , Long>{

}

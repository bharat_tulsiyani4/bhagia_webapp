package com.aone.module.location.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.aone.corelibrary.model.impl.AbstractBaseMasterEntity;
import com.aone.module.location.constant.LocationDbColPojoConstant;

@Entity
@Table(name = "location")
public class Location extends AbstractBaseMasterEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = LocationDbColPojoConstant.NAME)
	private String name;
	
	@Column(name = LocationDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Column(name = LocationDbColPojoConstant.SYSTEM_INSERTED)
	private String systemInserted;

	public String getSystemInserted() {
		return systemInserted;
	}

	public void setSystemInserted(String systemInserted) {
		this.systemInserted = systemInserted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Location [name=" + name + ", description=" + description
				+ ", systemInserted=" + systemInserted + "]";
	}

}

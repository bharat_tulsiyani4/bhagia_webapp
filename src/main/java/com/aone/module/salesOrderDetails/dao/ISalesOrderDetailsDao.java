package com.aone.module.salesOrderDetails.dao;

import java.util.List;

import com.aone.corelibrary.dao.IBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

public interface ISalesOrderDetailsDao extends IBaseMasterRepository<SalesOrderDetails , Long>{

	public List<SalesOrderDetails> getSalesOrderDetailsBySalesOrderId(long salesOrderId);

	public int deleteSalesOrderDetailsBySalesOrderId(long salesOrderId);

	public List<SalesOrderDetails> getRemainingQtySalesOrderDetails(SearchData<SalesOrderDetails> searchData);

	public int updateRemainingQty(long salesOrderDetailsId, double qtyToUpdate);

}

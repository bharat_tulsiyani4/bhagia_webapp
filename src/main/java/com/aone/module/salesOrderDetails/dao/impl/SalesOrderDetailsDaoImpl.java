package com.aone.module.salesOrderDetails.dao.impl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.salesOrder.model.SalesOrder;
import com.aone.module.salesOrderDetails.dao.ISalesOrderDetailsDao;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

@Lazy
@Repository("salesOrderDetailsDaoImpl")
public class SalesOrderDetailsDaoImpl extends AbstractBaseMasterRepository<SalesOrderDetails, Long> implements ISalesOrderDetailsDao {

	@Override
	public Class<SalesOrderDetails> getClazz() {
		return SalesOrderDetails.class;
	}
	
	@Override
	public List<SalesOrderDetails> getSalesOrderDetailsBySalesOrderId(long salesOrderId){
		Map<String, Object> params = new HashMap<>();
		params.put("deleted", false);
		params.put("salesOrderId", salesOrderId);
		return super.getByQueryData(params, Collections.emptyMap());
	}

	@Override
	public int deleteSalesOrderDetailsBySalesOrderId(long salesOrderId) {
		String whereClause = " WHERE salesOrderId = :salesOrderId";
		Map<String, Object> params = new HashMap<>();
		params.put("salesOrderId", salesOrderId);
		return super.deleteByQuery(whereClause, params);
	}

	@Override
	public SalesOrderDetails getNewData() {
		SalesOrderDetails SalesOrderDetails = new SalesOrderDetails();
		SalesOrderDetails.setDefaultValueForObject();
		return SalesOrderDetails;
	}

	@Override
	public List<SalesOrderDetails> getRemainingQtySalesOrderDetails(SearchData<SalesOrderDetails> searchData) {
		StringBuilder queryBuilder = new StringBuilder(50);
		
		queryBuilder.append("select e from ");
		queryBuilder.append(getClazz().getName() + " e ");
		queryBuilder.append(" INNER JOIN ").append(SalesOrder.class.getName()).append(" so ON so.id = e.salesOrderId ");
		queryBuilder.append(" where e.deleted = :deleted ");
		queryBuilder.append(" AND e.remainingQty > 0 ");
		
		Map<String, Object> params = new HashMap<>();
		params.put("deleted", false);
		SalesOrderDetails salesOrderDetails = searchData.getData();
		if(salesOrderDetails !=null ) {
			if(salesOrderDetails.getContactId() > 0 ) {
				queryBuilder.append(" AND so.contactId =:contactId ");
				params.put("contactId", salesOrderDetails.getContactId());
			}
			if(salesOrderDetails.getDesktopSalesOrderId() > 0 ) {
				queryBuilder.append(" AND so.desktopSalesOrderId =:desktopSalesOrderId ");
				params.put("desktopSalesOrderId", salesOrderDetails.getDesktopSalesOrderId());
			}
		}
		
		TypedQuery<SalesOrderDetails> query = getEntityManager().createQuery(queryBuilder.toString(), getClazz());
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}
	
	@Override
	@Transactional(readOnly = false)
	public int updateRemainingQty(long salesOrderDetailsId, double qtyToUpdate) {
		StringBuilder queryBuilder = new StringBuilder(70);
		queryBuilder.append("UPDATE ");
		queryBuilder.append(getClazz().getName());
		queryBuilder.append(" SET remainingQty = remainingQty + :quantity, ");
		queryBuilder.append(" updatedOn = :updatedOn");
		queryBuilder.append(" where id = :id");
		Query query = getEntityManager().createQuery(queryBuilder.toString());
		query.setParameter("quantity", qtyToUpdate);
		query.setParameter("updatedOn", LocalDateTime.now());
		query.setParameter("id", salesOrderDetailsId);
		return query.executeUpdate();
	}

}

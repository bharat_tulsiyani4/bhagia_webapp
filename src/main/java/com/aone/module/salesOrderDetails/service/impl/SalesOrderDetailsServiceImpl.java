package com.aone.module.salesOrderDetails.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.item.service.IItemService;
import com.aone.module.salesOrderDetails.dao.ISalesOrderDetailsDao;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;
import com.aone.module.salesOrderDetails.service.ISalesOrderDetailsService;

@Lazy
@Service("salesOrderDetailsServiceImpl")
public class SalesOrderDetailsServiceImpl extends AbstractBaseMasterService<SalesOrderDetails , Long, ISalesOrderDetailsDao> implements
		ISalesOrderDetailsService {
	
	private ISalesOrderDetailsDao salesOrderDetailsDao;
	private IItemService itemService;
	
	public IItemService getItemService() {
		return itemService;
	}

	@Autowired        
	@Qualifier(value="itemServiceImpl")
	public void setItemService(IItemService itemService) {
		this.itemService = itemService;
	}
	
	@Override
	public ISalesOrderDetailsDao getRepository() {
		return getSalesOrderDetailsDao();
	}

	public ISalesOrderDetailsDao getSalesOrderDetailsDao() {
		return salesOrderDetailsDao;
	}

	@Autowired
	@Qualifier(value = "salesOrderDetailsDaoImpl")
	public void setSalesOrderDetailsDao(ISalesOrderDetailsDao salesOrderDetailsDao) {
		this.salesOrderDetailsDao = salesOrderDetailsDao;
	}

	@Override
	public List<SalesOrderDetails> getSalesOrderDetailsBySalesOrderId(long salesOrderId) {
		return getSalesOrderDetailsDao().getSalesOrderDetailsBySalesOrderId(salesOrderId);
	}

	@Override
	public int deleteSalesOrderDetailsBySalesOrderId(long salesOrderId) {
		return getSalesOrderDetailsDao().deleteSalesOrderDetailsBySalesOrderId(salesOrderId);
	}


	@Override
	public List<SalesOrderDetails> getBySalesOrderId(long salesOrderId){
		Map<String, Object> params = new HashMap<>();
		params.put("salesOrderId", salesOrderId);
		params.put("deleted", false);
		return getRepository().getByQueryData(params, Collections.emptyMap());
	}
	
	@Override
	public ResponseEntity<IResponseData<SalesOrderDetails>> getRemainingQtySalesOrderDetails(RequestData<SearchData<SalesOrderDetails>, Long> requestData) {
		List<SalesOrderDetails> contactList = getRemainingQtySalesOrderDetails(requestData.getData());
		IResponseData<SalesOrderDetails> responseData = new ResponseData<>(getRepository().getNewData(), contactList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public List<SalesOrderDetails> getRemainingQtySalesOrderDetails(SearchData<SalesOrderDetails> searchData) {
		SalesOrderDetails orderDetails = searchData.getData();
		if(orderDetails.getContactId() <= 0) {
			return Collections.emptyList();
		}
		List<SalesOrderDetails> salesOrderDetailsList = salesOrderDetailsDao.getRemainingQtySalesOrderDetails(searchData);
		return salesOrderDetailsList;
	}

	@Override
	public int updateRemainingQty(long salesOrderDetailsId, double qtyToUpdate) {
		return salesOrderDetailsDao.updateRemainingQty(salesOrderDetailsId, qtyToUpdate);
	}
	
}
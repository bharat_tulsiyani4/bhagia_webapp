package com.aone.module.salesOrderDetails.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

public interface ISalesOrderDetailsService  extends IBaseMasterService<SalesOrderDetails, Long>{

	public List<SalesOrderDetails> getSalesOrderDetailsBySalesOrderId(long salesOrderId);
	
	public int deleteSalesOrderDetailsBySalesOrderId(long salesOrderId);
	
	public List<SalesOrderDetails> getBySalesOrderId(long salesOrderId);

	public ResponseEntity<IResponseData<SalesOrderDetails>> getRemainingQtySalesOrderDetails(
			RequestData<SearchData<SalesOrderDetails>, Long> requestData);

	public List<SalesOrderDetails> getRemainingQtySalesOrderDetails(SearchData<SalesOrderDetails> searchData);
	
	public int updateRemainingQty(long salesOrderDetailsId, double qtyToUpdate);
}

package com.aone.module.salesOrderDetails.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aone.commons.utils.CacheRequestUtils;
import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.config.security.annotation.SecurityMethodRole;
import com.aone.config.security.constant.RoleMatcher;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.salesOrderDetails.controller.ISalesOrderDetailsController;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;
import com.aone.module.salesOrderDetails.service.ISalesOrderDetailsService;

@RestController
@RequestMapping(value = { "/rest/salesOrderDetails", "/web/rest/salesOrderDetails"})
@SecurityClassRole(baseRoleName = SecurityRoleModule.SALES_ORDER)
public class SalesOrderDetailsRestController extends AbstractBaseMasterRestController<SalesOrderDetails , Long, ISalesOrderDetailsService> implements ISalesOrderDetailsController {
	
	private ISalesOrderDetailsService salesOrderDetailsService;
	
	@Override
	public ISalesOrderDetailsService getService() {
		return getSalesOrderDetailsService();
	}

	public ISalesOrderDetailsService getSalesOrderDetailsService() {
		return salesOrderDetailsService;
	}
	@Autowired        
	@Qualifier(value="salesOrderDetailsServiceImpl")
	public void setSalesDetailsService(ISalesOrderDetailsService salesOrderDetailsService) {
		this.salesOrderDetailsService = salesOrderDetailsService;
	}
	
	@Override
	@RequestMapping(value="/getRemainingQtySalesOrderDetails", method=RequestMethod.POST, consumes={MediaType.APPLICATION_JSON_UTF8_VALUE})
	@SecurityMethodRole(roleList = { "VIEW" }, roleMatcher = RoleMatcher.ANY)
	public @ResponseBody ResponseEntity<IResponseData<SalesOrderDetails>> getRemainingQtySalesOrderDetails(@RequestBody RequestData<SearchData<SalesOrderDetails>, Long> requestData) {
		CacheRequestUtils.chacheRequestData(Thread.currentThread().getName(), requestData);
		return getService().getRemainingQtySalesOrderDetails(requestData);
	}
	
}

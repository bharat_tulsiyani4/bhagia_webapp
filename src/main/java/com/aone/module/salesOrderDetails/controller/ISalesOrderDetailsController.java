package com.aone.module.salesOrderDetails.controller;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.salesOrderDetails.model.SalesOrderDetails;

public interface ISalesOrderDetailsController extends IBaseMasterController<SalesOrderDetails , Long>{

	public ResponseEntity<IResponseData<SalesOrderDetails>> getRemainingQtySalesOrderDetails(
			RequestData<SearchData<SalesOrderDetails>, Long> requestData);

}

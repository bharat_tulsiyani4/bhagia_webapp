package com.aone.module.salesOrderDetails.constant;

public class SalesOrderDetailsDbColPojoConstant {

	public static final String SALES_ORDER_ID="sales_order_id";
	public static final String DESKTOP_SALES_ORDER_ID = "desktop_sales_order_id";
	public static final String TRANSACTION_DATE="transaction_date";
	public static final String ITEM_NAME="item_name";
	public static final String ORDER_QTY="order_qty";
	public static final String REMAINING_QTY="remaining_qty";
	public static final String DESCRIPTION ="description";
	
	private SalesOrderDetailsDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

package com.aone.module.salesOrderDetails.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.salesOrderDetails.constant.SalesOrderDetailsDbColPojoConstant;

@Entity
@Table(name = "sales_order_details")
public class SalesOrderDetails extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = 7895104325035517069L;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.SALES_ORDER_ID)
	private long salesOrderId;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.DESKTOP_SALES_ORDER_ID)
	private long desktopSalesOrderId;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.TRANSACTION_DATE)
	private LocalDateTime transactionDate;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.ITEM_NAME)
	private String itemName;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.ORDER_QTY)
	private double orderQty;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.REMAINING_QTY)
	private double remainingQty;
	
	@Column(name = SalesOrderDetailsDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private long contactId;

	public long getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(long salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(double orderQty) {
		this.orderQty = orderQty;
	}

	public double getRemainingQty() {
		return remainingQty;
	}

	public void setRemainingQty(double remainingQty) {
		this.remainingQty = remainingQty;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	
	public long getDesktopSalesOrderId() {
		return desktopSalesOrderId;
	}

	public void setDesktopSalesOrderId(long desktopSalesOrderId) {
		this.desktopSalesOrderId = desktopSalesOrderId;
	}

	@Override
	public String toString() {
		return "SalesOrderDetails [salesOrderId=" + salesOrderId + ", transactionDate=" + transactionDate
				+ ", itemName=" + itemName + ", orderQty=" + orderQty + ", remainingQty=" + remainingQty
				+ ", description=" + description + ", contactId=" + contactId + ", desktopSalesOrderId="
				+ desktopSalesOrderId + "]";
	}

}

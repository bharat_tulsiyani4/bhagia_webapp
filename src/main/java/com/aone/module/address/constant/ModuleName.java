package com.aone.module.address.constant;

public enum ModuleName {
	
	CONTACT_SHOP_ADDRESS("CONTACT_SHOP_ADDRESS"),
	;
	
	private String value;

	private ModuleName(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}

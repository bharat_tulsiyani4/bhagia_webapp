package com.aone.module.address.service;

import java.util.List;

import com.aone.corelibrary.service.IBaseMasterService;
import com.aone.module.address.model.Address;

public interface IAddressService  extends IBaseMasterService<Address  , Long>{

	public List<Address> getAddressByContact(Address address);
}

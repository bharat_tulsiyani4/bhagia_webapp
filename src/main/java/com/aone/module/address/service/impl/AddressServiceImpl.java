package com.aone.module.address.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.address.dao.IAddressDao;
import com.aone.module.address.model.Address;
import com.aone.module.address.service.IAddressService;
import com.aone.module.city.model.City;
import com.aone.module.city.service.ICityService;
import com.aone.module.country.model.Country;
import com.aone.module.country.service.ICountryService;
import com.aone.module.state.model.State;
import com.aone.module.state.service.IStateService;

@Lazy
@Service("addressServiceImpl")
public class AddressServiceImpl extends AbstractBaseMasterService<Address, Long, IAddressDao>
		implements IAddressService {

	private IAddressDao  addressDao;
	private IStateService stateService;
	private ICityService cityService;
	private ICountryService countryService;

	@Override
	public IAddressDao getRepository() {
		return getAddressDao();
	}
	
	public ICountryService getCountryService() {
		return countryService;
	}

	@Autowired
	@Qualifier(value="countryServiceImpl")
	public void setCountryService(ICountryService countryService) {
		this.countryService = countryService;
	}
	
	public ICityService getCityService() {
		return cityService;
	}

	@Autowired        
	@Qualifier(value="cityServiceImpl")
	public void setCityService(ICityService cityService) {
		this.cityService = cityService;
	}
	
	public IStateService getStateService() {
		return stateService;
	}

	@Autowired        
	@Qualifier(value="stateServiceImpl")
	public void setStateService(IStateService stateService) {
		this.stateService = stateService;
	}

	public IAddressDao getAddressDao() {
		return addressDao;
	}

	@Autowired
	@Qualifier(value = "addressDaoImpl")
	public void setAddressDao(IAddressDao addressDao) {
		this.addressDao = addressDao;
	}
	
	@Override
	public List<Address> getAddressByContact(Address address){
		List<Address> addresseList = getRepository().getAddressByContact(address);
		State state = new State();
		City city = new City();
		Country country = new Country();
		for(Address add : addresseList) {
			// set city data
			city.setId(add.getCityId());
			add.setCity(cityService.getById(city));

			// set state data
			state.setId(add.getStateId());
			add.setState(stateService.getById(state));
			
			// set country data
			country.setId(add.getCountryId());
			add.setCountry(countryService.getById(country));
		}
		return addresseList;
	}

	@Override
	public ResultCodeConstant validateData(Address address) {
		if (address == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}

		if (address.getCountryId() < 0) {
			return ResultCodeConstant.ADDRESS_COUNTRY_CODE_NOT_FOUND;
		}
		if (address.getStateId() < 0) {
			return ResultCodeConstant.ADDRESS_STATE_CODE_NOT_FOUND;
		}
		if (address.getCityId() < 0) {
			return ResultCodeConstant.ADDRESS_CITY_CODE_NOT_FOUND;
		}
		if (address.getForeinKey() <= 0) {
			return ResultCodeConstant.ADDRESS_FOREIGN_KEY_NOT_FOUND;
		}
		if (address.getModuleName() == null || address.getModuleName().isEmpty()) {
			return ResultCodeConstant.ADDRESS_MOUDLE_NAME_NOT_FOUND;
		}
		return ResultCodeConstant.SUCCESS;
	}

}

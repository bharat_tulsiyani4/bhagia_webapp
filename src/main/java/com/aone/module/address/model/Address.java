package com.aone.module.address.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.address.constant.AddressDbColPojoConstant;
import com.aone.module.city.model.City;
import com.aone.module.country.model.Country;
import com.aone.module.state.model.State;

@Entity
@Table(name = "address")
public class Address extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = AddressDbColPojoConstant.IMAGE_PATH)
	private String street;

	@Column(name = AddressDbColPojoConstant.CITY_ID)
	private long cityId;

	@Column(name = AddressDbColPojoConstant.STATE_ID)
	private long stateId;

	@Column(name = AddressDbColPojoConstant.COUNTRY_ID)
	private long countryId;

	@Column(name = AddressDbColPojoConstant.ZIPCODE)
	private long zipCode;

	@Column(name = AddressDbColPojoConstant.PHONE)
	private String phone;

	@Column(name = AddressDbColPojoConstant.FAX)
	private String fax;

	@Column(name = AddressDbColPojoConstant.FOREIGN_KEY)
	private long foreinKey;

	@Column(name = AddressDbColPojoConstant.MODULE_NAME)
	private String moduleName;
	
	@Transient
	private City city;
	
	@Transient
	private State state;
	
	@Transient
	private Country country;
	
	@Override
	public boolean setDefaultValueForObject() {
		if(this.city == null)
			this.city = new City();
		if(this.state == null)
			this.state = new State();
		if(this.country == null)
			this.country = new Country();
		return true;
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getZipCode() {
		return zipCode;
	}

	public void setZipCode(long zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public long getForeinKey() {
		return foreinKey;
	}

	public void setForeinKey(long foreinKey) {
		this.foreinKey = foreinKey;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", cityId=" + cityId + ", stateId=" + stateId + ", countryId=" + countryId
				+ ", zipCode=" + zipCode + ", phone=" + phone + ", fax=" + fax + ", foreinKey=" + foreinKey
				+ ", moduleName=" + moduleName + ", city=" + city + ", state=" + state + ", country=" + country + "]";
	}

}

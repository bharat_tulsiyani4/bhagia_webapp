package com.aone.module.login.model;

import java.util.List;

import com.aone.module.userRole.model.UserRole;

public class UserInfo {
	
	private String userName;
	private List<UserRole> userRoleList;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<UserRole> getUserRoleList() {
		return userRoleList;
	}
	public void setUserRoleList(List<UserRole> userRoleList) {
		this.userRoleList = userRoleList;
	}
	@Override
	public String toString() {
		return "UserInfo [userName=" + userName + ", userRoleList=" + userRoleList + "]";
	}

}

package com.aone.module.login.service;

import org.springframework.http.ResponseEntity;

import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.module.login.model.UserInfo;
import com.aone.module.user.model.User;

public interface ILoginService {

	public UserInfo getCurrentLoginUserDetails();
	
	public ResponseEntity<IResponseData<UserInfo>> getCurrentLoginUserDetails(RequestData<UserInfo, Long> requestData);

	public boolean checkIfHasPermission(String moduleName, String permisson);
	
	public ResponseEntity<IResponseData<User>> userLogin(RequestData<User, Long> requestData);

	public ResponseEntity<IResponseData<User>> userLogout(RequestData<User, Long> requestData);
	
}

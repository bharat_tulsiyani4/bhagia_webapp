package com.aone.module.login.service.impl;

import java.util.Collections;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.aone.commons.CommonConstant;
import com.aone.commons.utils.CacheRequestUtils;
import com.aone.commons.utils.PropertyUtil;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.login.model.UserInfo;
import com.aone.module.login.service.ILoginService;
import com.aone.module.user.model.User;
import com.aone.module.user.service.IUserService;
import com.aone.module.userRole.model.UserRole;
import com.aone.module.userRole.service.IUserRoleService;

@Lazy
@Service("loginServiceImpl")
public class LoginServiceImpl implements ILoginService {
	
	private IUserRoleService userRoleService;
	private static IUserService userService;
	
	public IUserService getUserService() {
		return userService;
	}

	@Autowired        
	@Qualifier(value="userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IUserRoleService getUserRoleService() {
		return userRoleService;
	}

	@Autowired
	@Qualifier(value="userRoleServiceImpl")
	public void setUserRoleService(IUserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

	@Override
	public UserInfo getCurrentLoginUserDetails() {
		
		String userName = getCurrentLoginUserName();
		
		UserInfo userInfo = new UserInfo();
		
		User user = userService.getByUserName(userName);
		userInfo.setUserName(user.getName());
		
		if(userName.equals("sa") || userName.equals("admin")) {
			SearchData<UserRole> searchData = new SearchData<>();
			userInfo.setUserRoleList(userRoleService.getActiveData(searchData));
			return userInfo;
		}
		
		if(userName.startsWith(CommonConstant.USER)) {
			userInfo.setUserRoleList(userRoleService.getByContactInfo(user.getId(),CommonConstant.USER));			
			return userInfo;
		}
		
		//fetch and set user Role
		return null;
	}
	
	@Override
	public ResponseEntity<IResponseData<UserInfo>> getCurrentLoginUserDetails(RequestData<UserInfo, Long> requestData) {
		UserInfo userInfo = getCurrentLoginUserDetails();
		IResponseData<UserInfo> responseData = new ResponseData<>(userInfo, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public boolean checkIfHasPermission(String moduleName, String permisson) {
		UserInfo userInfo = getCurrentLoginUserDetails();
		String modulePermission = moduleName + permisson;
		for (UserRole userRole : userInfo.getUserRoleList()) {
			if (userRole.getModuleName().equals(modulePermission)) {
				return true;
			}
		}
		return false;
	}

	public static String getCurrentLoginUserName() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName = null;
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		//DEV-- start
		if(!PropertyUtil.checkProdEnv() && (userName == null || userName.isEmpty() || userName.equals("anonymousUser"))) {
			userName = "admin";
		}
		//DEV-- end
		
		return userName;
	}

	public static boolean isUserLogin() {
		String userName = getCurrentLoginUserName();
		if(userName.contains(CommonConstant.USER)) {
			return true;
		}
		return false;
	}
	
	public static boolean isAdminLogin() {
		String userName = getCurrentLoginUserName();
		if(userName.contains(CommonConstant.ADMIN)) {
			return true;
		}
		return false;
	}
	
	public static boolean isSuperAdminLogin() {
		String userName = getCurrentLoginUserName();
		if(userName.contains(CommonConstant.SUPER_ADMIN)) {
			return true;
		}
		return false;
	}
	
	public static long getCurrentLoginUserId() {
		long userId = NumberUtils.toLong(CacheRequestUtils.getRequestData(Thread.currentThread().getName()).getAuthToken());
		if(userId > 0) {
			return userId;
		}
		String userName = getCurrentLoginUserName();
		User user = userService.getByUserName(userName);
		if(user != null) {
			return user.getId();
		}
		return -1;
	}
	
	@Override
	public ResponseEntity<IResponseData<User>> userLogin(RequestData<User, Long> requestData) {
		return userService.processLogin(requestData);
	}
	
	@Override
	public ResponseEntity<IResponseData<User>> userLogout(RequestData<User, Long> requestData) {
		return userService.processLogout(requestData); 
	}
	
}

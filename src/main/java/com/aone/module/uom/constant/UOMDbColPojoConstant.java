package com.aone.module.uom.constant;

public class UOMDbColPojoConstant {

	public static final String NAME="name";
	public static final String DESCRIPTION ="description";
	public static final String SYSTEM_INSERTED ="system_inserted";
	
	private UOMDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

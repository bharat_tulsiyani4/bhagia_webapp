package com.aone.module.uom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.aone.corelibrary.model.impl.AbstractBaseMasterEntity;
import com.aone.module.uom.constant.UOMDbColPojoConstant;

@Entity
@Table(name = "uom_master")
public class UOM extends AbstractBaseMasterEntity<Long> {

	private static final long serialVersionUID = -3283165044313532989L;

	@Column(name = UOMDbColPojoConstant.NAME)
	private String name;

	@Column(name = UOMDbColPojoConstant.DESCRIPTION)
	private String description;

	@Column(name = UOMDbColPojoConstant.SYSTEM_INSERTED)
	private String systemInserted;
	
	public String getSystemInserted() {
		return systemInserted;
	}
	
	public void setSystemInserted(String systemInserted) {
		this.systemInserted = systemInserted;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "UOM [name=" + name + ", description=" + description
				+ ", systemInserted=" + systemInserted + "]";
	}

}

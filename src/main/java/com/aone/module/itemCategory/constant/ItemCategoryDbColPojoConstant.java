package com.aone.module.itemCategory.constant;

public class ItemCategoryDbColPojoConstant {

	public static final  String NAME="name";
	public static final String DESCRIPTION ="description";
	public static final String SYSTEM_INSERTED ="system_inserted";
	
	private ItemCategoryDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

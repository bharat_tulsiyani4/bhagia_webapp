package com.aone.module.itemCategory.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.itemCategory.dao.IItemCategoryDao;
import com.aone.module.itemCategory.model.ItemCategory;

@Lazy
@Repository("itemCategoryDaoImpl")
public class ItemCategoryDaoImpl extends AbstractBaseMasterRepository<ItemCategory, Long> implements IItemCategoryDao {

	@Override
	public Class<ItemCategory> getClazz() {
		return ItemCategory.class;
	}
	

	@Override
	@Transactional(readOnly = true)
	public List<ItemCategory> getActiveData(SearchData<ItemCategory> searchData) {
		TypedQuery<ItemCategory> query = getQueryObject(searchData, false, false, getClazz());
		query.setParameter("deleted", false);
		if (searchData != null) {
			ItemCategory itemCategory = searchData.getData();
			if (itemCategory != null) {
				if (itemCategory.getName() != null && !itemCategory.getName().isEmpty()) {
					query.setParameter("name", "%" + itemCategory.getName() + "%");
				}
			}
		}
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Long getActiveDataCount(SearchData<ItemCategory> searchData) {
		if (!isPublicModule()) {
		
		}
		TypedQuery<Long> query = getQueryObject(searchData, false, true, Long.class);
		query.setParameter("deleted", false);
		if (searchData != null) {
			ItemCategory itemCategory = searchData.getData();
			if (itemCategory != null) {
				if (itemCategory.getName() != null && !itemCategory.getName().isEmpty()) {
					query.setParameter("name", "%" + itemCategory.getName() + "%");
				}
			}

		}
		return query.getSingleResult();
	}

	@Override
	protected <R> TypedQuery<R> getQueryObject(SearchData<ItemCategory> searchData, boolean isGetAll, boolean isCountQuery,
			Class<R> clazz) {
		StringBuilder queryBuilder = new StringBuilder(50);

		if (isCountQuery) {
			queryBuilder.append(SELECT_COUNT_PREFIX);
		} else {
			queryBuilder.append(SELECT_PREFIX);
		}
		queryBuilder.append(getClazz().getName() + " e Where 1=1  ");
		if (!isGetAll) {
			if (isPublicModule()) {
				queryBuilder.append("  AND e.deleted = :deleted ");
			} else {
				/* queryBuilder.append(" e.createdBy = :createdBy AND "); */
				if (searchData != null) {
					ItemCategory itemCategory = searchData.getData();
					if (itemCategory != null) {
						if (itemCategory.getName() != null && !itemCategory.getName().isEmpty()) {
							queryBuilder.append(" AND e.name like :name ");
						}					}
					queryBuilder.append(" AND e.deleted = :deleted ");
				}
			}
		}
		if (searchData != null) {
			if (searchData.getOrderingData() != null && !isCountQuery) {
				queryBuilder.append(" order by e." + searchData.getOrderingData().getColumn());
				if (searchData.getOrderingData().isDescending()) {
					queryBuilder.append(" desc ");
				}
			}
		}
		TypedQuery<R> query = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if (searchData != null) {
			if (searchData.getPaginationData() != null && !isCountQuery) {
				query.setFirstResult(searchData.getPaginationData().getRowsPerPage()
						* (searchData.getPaginationData().getPageNumber() - 1));
				query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
			}
		}
		return query;
	}


	@Override
	public ItemCategory getNewData() {
		return new ItemCategory(); 
	}
}

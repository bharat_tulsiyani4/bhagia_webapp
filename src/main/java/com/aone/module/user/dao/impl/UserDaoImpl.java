package com.aone.module.user.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.boot.spi.AdditionalJaxbMappingProducer;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aone.commons.exceptions.AuthenticationFailedException;
import com.aone.commons.utils.CacheRequestUtils;
import com.aone.corelibrary.dao.impl.AbstractBaseMasterRepository;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.module.login.service.impl.LoginServiceImpl;
import com.aone.module.user.dao.IUserDao;
import com.aone.module.user.model.User;

@Lazy
@Repository("userDaoImpl")
public class UserDaoImpl extends AbstractBaseMasterRepository<User, Long> implements IUserDao {

	@Override
	public Class<User> getClazz() {
		return User.class;
	}
	
	@Override
	public boolean isPublicModule() {
		return true;
	}

	@Override
	public User getNewData() {
		User user = new User();
		return user;
	}
	
	@Override
	protected <R> TypedQuery<R> getQueryObject(SearchData<User> searchData, boolean isGetAll, boolean isCountQuery, Class<R> clazz){
		StringBuilder queryBuilder = new StringBuilder(50);
		
		if(isCountQuery) {
			queryBuilder.append(SELECT_COUNT_PREFIX);
		} else {
			queryBuilder.append(SELECT_PREFIX);
		}
		queryBuilder.append(getClazz().getName() + " e ");
		if(!isGetAll) {
			queryBuilder.append(" where e.deleted = :deleted ");

			if(LoginServiceImpl.isAdminLogin()) {
				queryBuilder.append(" AND e.id > -2 ");
			} else if(LoginServiceImpl.isUserLogin()) {
				queryBuilder.append(" AND e.id > 0 ");
			}
			
			if(!isPublicModule()) {
				queryBuilder.append(" AND e.createdBy = :createdBy ");
			}
		}
		if(searchData != null && searchData.getOrderingData() != null && !isCountQuery) {
			queryBuilder.append(" order by e."+searchData.getOrderingData().getColumn());
			if(searchData.getOrderingData().isDescending()) {
				queryBuilder.append(" desc ");
			}
		}
		TypedQuery<R> query = getEntityManager().createQuery(queryBuilder.toString(), clazz);
		if(searchData != null && searchData.getPaginationData() != null && !isCountQuery) {
			query.setFirstResult(searchData.getPaginationData().getRowsPerPage() * (searchData.getPaginationData().getPageNumber() - 1));
			query.setMaxResults(searchData.getPaginationData().getRowsPerPage());
		}
		return query;
	}

}

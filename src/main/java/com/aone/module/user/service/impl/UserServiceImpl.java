package com.aone.module.user.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.commons.CommonConstant;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.impl.AbstractBaseMasterService;
import com.aone.module.files.constant.FilesDataModuleName;
import com.aone.module.files.constant.FilesDataPathConstant;
import com.aone.module.files.model.FilesData;
import com.aone.module.user.dao.IUserDao;
import com.aone.module.user.model.User;
import com.aone.module.user.model.UserInit;
import com.aone.module.user.service.IUserService;
import com.aone.module.user.util.UserUtil;
import com.aone.module.userRoleGroup.model.UserRoleGroup;
import com.aone.module.userRoleGroup.service.IUserRoleGroupService;
import com.aone.module.userRoleGroupMapping.model.UserRoleGroupMapping;
import com.aone.module.userRoleGroupMapping.service.IUserRoleGroupMappingService;

@Lazy
@Service("userServiceImpl")
public class UserServiceImpl extends AbstractBaseMasterService<User, Long, IUserDao> implements IUserService {

	private IUserDao userDao;
	private IUserRoleGroupMappingService userRoleGroupMappingService;
	private IUserRoleGroupService userRoleGroupService;

	public IUserRoleGroupService getUserRoleService() {
		return userRoleGroupService;
	}

	@Autowired
	@Qualifier(value="userRoleGroupServiceImpl")
	public void setUserRoleService(IUserRoleGroupService userRoleGroupService) {
		this.userRoleGroupService = userRoleGroupService;
	}
	
	@Autowired        
	@Qualifier(value="userRoleGroupMappingServiceImpl")
	public void setUserRoleGroupMappingService(IUserRoleGroupMappingService userRoleGroupMappingService) {
		this.userRoleGroupMappingService = userRoleGroupMappingService;
	}	
	
	public IUserRoleGroupMappingService getUserRoleGroupMappingService() {
		return userRoleGroupMappingService;
	}

	@Override
	public IUserDao getRepository() {
		return getUserDao();
	}
	
	@Override
	public boolean isSetDefaultRequired() {
		return true;
	}

	public IUserDao getUserDao() {
		return userDao;
	}

	@Autowired        
	@Qualifier(value="userDaoImpl")
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public ResponseEntity<IResponseData<Boolean>> checkUserName(RequestData<User, Long> requestData) {
		IResponseData<Boolean> responseData = new ResponseData<>(!getRepository().checkUniqueDataByField("name", requestData.getData().getName(), "id", requestData.getData().getId()).isEmpty(), Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<User>> save(RequestData<User, Long> requestData) {
		User user = requestData.getData();
		ResultCodeConstant resultCodeConstant = this.save(user);
		IResponseData<User> responseData = new ResponseData<>(user, getRepository().getNewDataList(), resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	@Override
	public ResultCodeConstant save(User user) {
		ResultCodeConstant resultCodeConstant = validateData(user);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		
		// save sales master
		user = getRepository().save(user);
		
		// save agent bit plan details list
		List<UserRoleGroupMapping> userRoleGroupMappingsList = user.getUserRoleGroupMappingList();
		if(userRoleGroupMappingsList != null) {
			for(UserRoleGroupMapping userRoleGroupMapping : userRoleGroupMappingsList) {
				userRoleGroupMapping.setContactId(user.getId());
				userRoleGroupMapping.setModuleName(CommonConstant.USER);
			}
			userRoleGroupMappingService.saveOrUpdateAll(userRoleGroupMappingsList);
		}
		
		return ResultCodeConstant.SUCCESS;
	}
	@Override
	public ResponseEntity<IResponseData<User>> update(RequestData<User, Long> requestData) {
		User user = requestData.getData();
		ResultCodeConstant resultCodeConstant = this.update(user);
		
		IResponseData<User> responseData = new ResponseData<>(user, getRepository().getNewDataList(), resultCodeConstant);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant update(User user) {
		ResultCodeConstant resultCodeConstant = validateData(user);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		
		// save sales master
		user = getRepository().update(user);
		
		// delete pre details
		userRoleGroupMappingService.deleteByContactId(user.getId());
		
		// save agent bit plan details list
		List<UserRoleGroupMapping> userRoleGroupMappingsList = user.getUserRoleGroupMappingList();
		for(UserRoleGroupMapping userRoleGroupMapping : userRoleGroupMappingsList) {
			userRoleGroupMapping.setContactId(user.getId());
			userRoleGroupMapping.setModuleName(CommonConstant.USER);
		}
		userRoleGroupMappingService.saveOrUpdateAll(userRoleGroupMappingsList);
		
		return ResultCodeConstant.SUCCESS;
	}
	@Override
	public ResponseEntity<IResponseData<User>> getActiveData(RequestData<SearchData<User>, Long> requestData) {
		ResponseEntity<IResponseData<User>> response = super.getActiveData(requestData);
		return response;
	}
	
	
	@Override
	public ResultCodeConstant validateData(User user) {
		if(user == null) {
			return ResultCodeConstant.PARAMETER_NOT_FOUND;
		}
		
		// validate name
		if(user.getName() == null || user.getName().isEmpty()) {
			return ResultCodeConstant.USER_NAME_NOT_FOUND;
		}
		// check if User name already exist
		if(!getRepository().checkUniqueDataByField("name", user.getName(), "id", user.getId()).isEmpty()) {
			return ResultCodeConstant.USER_WITH_SAME_NAME_ALREADY_EXIST;
		}
		
		if(!getRepository().checkUniqueDataByField("userName", user.getUserName(), "id", user.getId()).isEmpty()) {
			return ResultCodeConstant.USER_NAME_ALREADY_EXIST;
		}
		
		if(null == user.getUserName() ||user.getUserName() == "") {
			return ResultCodeConstant.USER_USERNAME_REQURIED;
		}
		
		if(null == user.getPassword() ||user.getPassword() == "") {
			return ResultCodeConstant.USER_PASSWORD_REQURIED;
		}
		return ResultCodeConstant.SUCCESS;
	}


	@Override
	public ResponseEntity<IResponseData<User>> getById(RequestData<User, Long> requestData) {
		ResponseEntity<IResponseData<User>> responseEntity = super.getById(requestData); 
		
		IResponseData<User> responseData = responseEntity.getBody();
		
		User user = responseData.getData();
		
		// set init data
		UserInit userInit = new UserInit();
		
		SearchData<UserRoleGroup> userRoleSearchData = new SearchData<UserRoleGroup>();
		userInit.setUserRoleGroupList(userRoleGroupService.getActiveData(userRoleSearchData));
		user.setUserInit(userInit);
		
		user.setUserRoleGroupMappingList(userRoleGroupMappingService.getByContactId(user.getId()));
		return responseEntity;
	}
	
	@Override
	public User getByUserName(String userName){
		Map<String, Object> params = new HashMap<>();
		params.put("userName", userName);
		params.put("deleted", false);
		List<User> userList = getRepository().getByQueryData(params, Collections.emptyMap());
		if(userList == null || userList.isEmpty()) {
			return null;
		}
		return userList.get(0);
	}
	
	@Override
	public ResponseEntity<IResponseData<User>> processLogin(RequestData<User, Long> requestData){
		User user = requestData.getData();
		
		if(user.getUserName() == null  || user.getUserName().isEmpty()) {
			IResponseData<User> responseData = new ResponseData<>(user, Collections.emptyList(), ResultCodeConstant.USER_NAME_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}

		if(user.getPassword() == null  || user.getPassword().isEmpty()) {
			IResponseData<User> responseData = new ResponseData<>(user, Collections.emptyList(), ResultCodeConstant.USER_PASSWORD_REQURIED);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		/*if(user.getDeviceId() == null  || user.getDeviceId().isEmpty()) {
			IResponseData<User> responseData = new ResponseData<>(user, Collections.emptyList(), ResultCodeConstant.USER_DEVICE_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}*/
		
		Map<String, Object> params = new HashMap<>();
		params.put("userName", user.getUserName());
		params.put("password", user.getPassword());
		params.put("deleted", false);
		params.put("active", true);
		List<User> userList = getRepository().getByQueryData(params, Collections.emptyMap());
		
		if(userList == null || userList.isEmpty()) {
			IResponseData<User> responseData = new ResponseData<>(user, getRepository().getNewDataList(), ResultCodeConstant.NETWORK_AUTHENTICATION_REQUIRED);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		requestData.setAuthToken("-1");
		
		User userDB = userList.get(0);
		
		/*if(userDB.getDeviceId() == null || userDB.getDeviceId().isEmpty()) {
			userDB.setDeviceId(user.getDeviceId());
			getRepository().update(userDB);
		}*/
		
		/*if(!userDB.getDeviceId().equals(user.getDeviceId())) {
			IResponseData<User> responseData = new ResponseData<>(user, getRepository().getNewDataList(), ResultCodeConstant.DEVICE_ID_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}*/
		
		//userDB.setProfileImagePath(getProfileImagePath(userDB));
		
		/*AgentVisitLocation agentVisitLocation = new AgentVisitLocation();
		agentVisitLocation.setAgentId(agentMasterDB.getId());
		agentVisitLocation.setDate(LocalDateTime.now());
		agentVisitLocation.setStatus(StatusValues.LOGIN.name());
		agentVisitLocation.setLatitude(user.getLatitude());
		agentVisitLocation.setLongtitude(user.getLongtitude());
		agentVisitLocation.setLocation(user.getLocation());
		
		agentVisitLocationService.save(agentVisitLocation);*/
		
		userDB.setPassword("");
		
		IResponseData<User> responseData = new ResponseData<>(userDB, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<User>> processLogout(RequestData<User, Long> requestData) {
		
		User user = requestData.getData();
		User userDB = getRepository().getById(NumberUtils.toLong(requestData.getAuthToken()));
		
		if(userDB == null) {
			IResponseData<User> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.NETWORK_AUTHENTICATION_REQUIRED);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		/*AgentVisitLocation logoutData = new AgentVisitLocation();
		logoutData.setAgentId(userDB.getId());
		logoutData.setStartDate(getStartTodayDate());
		logoutData.setEndDate(getEndTodayDate());
		logoutData.setStatus(StatusValues.LOGOUT.name());
		List<AgentVisitLocation> agentVisitLocationList = agentVisitLocationService.getDataByStatus(logoutData);
		
		if(agentVisitLocationList == null || agentVisitLocationList.isEmpty()) {
			AgentVisitLocation agentVisitLocation = new AgentVisitLocation();
			agentVisitLocation.setAgentId(userDB.getId());
			agentVisitLocation.setDate(LocalDateTime.now());
			agentVisitLocation.setStatus(StatusValues.LOGOUT.name());
			agentVisitLocation.setLatitude(user.getLatitude());
			agentVisitLocation.setLongtitude(user.getLongtitude());
			agentVisitLocation.setLocation(user.getLocation());
			agentVisitLocation.setDailyAllowances(user.getDailyAllowances());
			agentVisitLocation.setTravelAllowances(user.getTravelAllowances());
			
			agentVisitLocationService.save(agentVisitLocation);
		} else {
			AgentVisitLocation agentVisitLocationToUpdate = agentVisitLocationList.get(0);
			agentVisitLocationToUpdate.setDate(LocalDateTime.now());
			agentVisitLocationToUpdate.setLatitude(user.getLatitude());
			agentVisitLocationToUpdate.setLongtitude(user.getLongtitude());
			agentVisitLocationToUpdate.setLocation(user.getLocation());
			agentVisitLocationToUpdate.setDailyAllowances(user.getDailyAllowances());
			agentVisitLocationToUpdate.setTravelAllowances(user.getTravelAllowances());
			
			agentVisitLocationService.update(agentVisitLocationToUpdate);
		}*/
		
		IResponseData<User> responseData = new ResponseData<>(user, Collections.emptyList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

}
package com.aone.module.user.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aone.corelibrary.model.impl.AbstractBaseMasterLongKeyEntity;
import com.aone.module.user.constant.UserDbColPojoConstant;
import com.aone.module.userRoleGroupMapping.model.UserRoleGroupMapping;

@Entity
@Table(name = "user")
public class User extends AbstractBaseMasterLongKeyEntity<Long> {

	private static final long serialVersionUID = 2720214281416012405L;

	@Column(name = UserDbColPojoConstant.NAME)
	private String name;

	@Column(name = UserDbColPojoConstant.USER_NAME)
	private String userName;
	
	@Column(name = UserDbColPojoConstant.PASSWORD)
	private String password;
	
	@Column(name = UserDbColPojoConstant.DESCRIPTION)
	private String description;
	
	@Transient
	private List<UserRoleGroupMapping> userRoleGroupMappingList;
	
	@Transient
	private UserInit userInit;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<UserRoleGroupMapping> getUserRoleGroupMappingList() {
		return userRoleGroupMappingList;
	}

	public void setUserRoleGroupMappingList(List<UserRoleGroupMapping> userRoleGroupMappingList) {
		this.userRoleGroupMappingList = userRoleGroupMappingList;
	}

	public UserInit getUserInit() {
		return userInit;
	}

	public void setUserInit(UserInit userInit) {
		this.userInit = userInit;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", userName=" + userName + ", password=" + password + ", description="
				+ description + ", userRoleGroupMappingList=" + userRoleGroupMappingList + ", userInit=" + userInit
				+ "]";
	}
	
}

package com.aone.module.user.constant;

public class UserDbColPojoConstant {
	public static final String NAME = "name";
	public static final String USER_NAME = "user_name";
	public static final String PASSWORD = "password";
	public static final String DESCRIPTION = "description";

	private UserDbColPojoConstant() {
		throw new IllegalStateException("Constant class");
	}
}

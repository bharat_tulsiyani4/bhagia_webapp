package com.aone.module.user.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aone.config.security.annotation.SecurityClassRole;
import com.aone.corelibrary.constant.SecurityRoleModule;
import com.aone.corelibrary.controller.impl.AbstractBaseMasterRestController;
import com.aone.module.user.controller.IUserController;
import com.aone.module.user.model.User;
import com.aone.module.user.service.IUserService;

@RestController
@RequestMapping(value = {"/rest/user", "/web/rest/user"})
@SecurityClassRole(baseRoleName = SecurityRoleModule.USER)
public class UserRestController extends AbstractBaseMasterRestController<User, Long, IUserService> implements IUserController {
	
	private IUserService userService;
	
	@Override
	public IUserService getService() {
		return getUserService();
	}

	public IUserService getUserService() {
		return userService;
	}

	@Autowired        
	@Qualifier(value="userServiceImpl")
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
}

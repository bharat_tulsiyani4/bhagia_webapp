package com.aone.module.user.controller;

import com.aone.corelibrary.controller.IBaseMasterController;
import com.aone.module.user.model.User;

public interface IUserController extends IBaseMasterController<User, Long> {

}

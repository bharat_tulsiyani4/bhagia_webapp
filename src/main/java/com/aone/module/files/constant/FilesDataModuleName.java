package com.aone.module.files.constant;

public enum FilesDataModuleName {
	
	CONTACT("CONTACT"),
	OFFER("OFFER"),
	;
	
	private String value;

	private FilesDataModuleName(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}

package com.aone.module.files.constant;

/**
* @author Bharat on Jun 24, 2017
*/
public enum FilesDataPathConstant {

	CONTACT_IMAGES("/resources/images/uploaded/contact-images"),
	OFFER_IMAGES("/resources/images/uploaded/offer-images"),
	;
	
	private String path;

	/**
	 * @param path
	 */
	private FilesDataPathConstant(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
	
}




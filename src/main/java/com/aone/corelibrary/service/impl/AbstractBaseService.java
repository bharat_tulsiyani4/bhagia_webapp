package com.aone.corelibrary.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aone.commons.exceptions.OperationFailedException;
import com.aone.corelibrary.constant.ResultCodeConstant;
import com.aone.corelibrary.dao.IBaseRepository;
import com.aone.corelibrary.model.IBaseEntity;
import com.aone.corelibrary.model.IResponseData;
import com.aone.corelibrary.model.impl.RequestData;
import com.aone.corelibrary.model.impl.ResponseData;
import com.aone.corelibrary.model.impl.SearchData;
import com.aone.corelibrary.service.IBaseService;

@Lazy
@Service
public abstract class AbstractBaseService<T extends IBaseEntity<K>, K extends Serializable, E extends IBaseRepository<T, K>>
		implements IBaseService<T, K> {

	private Map<K, T> chacheData = new HashMap<>();

	@Override
	public boolean addCacheEntry(K id, T entity) {
		if(!entity.isDeleted()) {
			chacheData.put(id, entity);
		}
		return true;
	}

	@Override
	public boolean removeCacheEntry(K id) {
		chacheData.remove(id);
		return true;
	}
	
	@Override
	public boolean removeAllCacheEntry() {
		chacheData = new HashMap<>();
		return true;
	}
	
	@Override
	public T getCacheEntry(K id) {
		return chacheData.get(id);
	}

	@Override
	public Collection<T> getAllCacheEntry() {
		return chacheData.values();
	}

	@Override
	public ResponseEntity<IResponseData<T>> init(RequestData<T,K> requestData) {
		IResponseData<T> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<T>> save(RequestData<T,K> requestData) {
		ResultCodeConstant resultCodeConstant = validateData(requestData.getData());
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			IResponseData<T> responseData = new ResponseData<>( requestData.getData(), getRepository().getNewDataList(), resultCodeConstant);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		T data = getRepository().save(requestData.getData());
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		IResponseData<T> responseData = new ResponseData<>(data, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<T>> update(RequestData<T,K> requestData) {
		ResultCodeConstant resultCodeConstant = validateData(requestData.getData());
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			IResponseData<T> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), resultCodeConstant);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		T data = getRepository().update(requestData.getData());
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		IResponseData<T> responseData = new ResponseData<>(data, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<T>> saveOrUpdate(RequestData<T,K> requestData) {
		ResultCodeConstant resultCodeConstant = validateData(requestData.getData());
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			IResponseData<T> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), resultCodeConstant);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		T data = getRepository().saveOrUpdate(requestData.getData());
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		IResponseData<T> responseData = new ResponseData<>(data, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<T>> saveOrUpdateAll(RequestData<T,K> requestData) {
		// check if list is empty then return the result code with empty list message
		List<T> dataList = requestData.getDataList();
		if(dataList == null  || dataList.isEmpty()) {
			IResponseData<T> responseData = new ResponseData<>(getRepository().getNewData(),requestData.getDataList(), ResultCodeConstant.PARAMETER_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		
		// validate all the data 
		for(T data : dataList) {
			ResultCodeConstant resultCodeConstant = validateData(data);
			if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
				IResponseData<T> responseData = new ResponseData<>(getRepository().getNewData(), requestData.getDataList(), resultCodeConstant);
				responseData.setRequestToken(requestData.getRequestToken());
				return new ResponseEntity<>(responseData, HttpStatus.OK);
			}
		}
		dataList = getRepository().saveOrUpdateAll(requestData.getDataList().iterator());
		if(isCachingEnabled()) {
			for(T data : dataList) {
				addCacheEntry(data.getId(), data);
			}
		}
		
		// save the data list
		IResponseData<T> responseData = new ResponseData<>(getRepository().getNewData(), dataList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<IResponseData<T>> delete(RequestData<T,K> requestData) {
		int count = getRepository().delete(requestData.getData());
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		if(isCachingEnabled()) {
			removeCacheEntry(requestData.getData().getId());
		}
		IResponseData<T> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<T>> getById(RequestData<T,K> requestData) {
		if(requestData.getId() == null) {
			IResponseData<T> responseData = new ResponseData<>(requestData.getData(), getRepository().getNewDataList(), ResultCodeConstant.PARAMETER_NOT_FOUND);
			responseData.setRequestToken(requestData.getRequestToken());
			return new ResponseEntity<>(responseData, HttpStatus.OK);
		}
		T data =  null;
		if(isCachingEnabled()) {
			data = getCacheEntry(requestData.getData().getId());
			data.setDefaultValueForObject();
		}
		if(data == null) {
			data = getRepository().getById(requestData.getId());
			data.setDefaultValueForObject();
			if(isCachingEnabled()) {
				addCacheEntry(data.getId(), data);
			}
		}
		IResponseData<T> responseData = new ResponseData<>(data, getRepository().getNewDataList(), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public T getById(K id) {
		if(id == null) {
			return null;
		}
		T data =  null;
		if(isCachingEnabled()) {
			data = getCacheEntry(id);
			data.setDefaultValueForObject();
		}
		if(data == null) {
			data = getRepository().getById(id);
			if(data != null) {
				data.setDefaultValueForObject();
			}
			if(isCachingEnabled()) {
				addCacheEntry(data.getId(), data);
			}
		}
		return data;
	}

	@Override
	public ResponseEntity<IResponseData<T>> getActiveData(RequestData<SearchData<T>,K> requestData) {
		List<T> dataList = Collections.emptyList();
		if(isCachingEnabled()) {
			dataList = new ArrayList<>(getAllCacheEntry());
		}
		
		if(dataList == null || dataList.isEmpty()) {
			dataList = getRepository().getActiveData(requestData.getData());
			if(isSetDefaultRequired()) {
				for(T data : dataList) {
					data.setDefaultValueForObject();
					if(isCachingEnabled()) {
						addCacheEntry(data.getId(), data);
					}
				}
			}
		}
		IResponseData<T> responseData = new ResponseData<>(getRepository().getNewData(), dataList, ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		responseData.setTotalRecords(getRepository().getActiveDataCount(requestData.getData()));
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<IResponseData<T>> getAll(RequestData<SearchData<T>,K> requestData) {
		IResponseData<T> responseData = new ResponseData<>(getRepository().getNewData(), getRepository().getAll(requestData.getData()), ResultCodeConstant.SUCCESS);
		responseData.setRequestToken(requestData.getRequestToken());
		return new ResponseEntity<>(responseData, HttpStatus.OK);
	}
	
	@Override
	public ResultCodeConstant save(T data) {
		ResultCodeConstant resultCodeConstant = validateData(data);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		data = getRepository().save(data);
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		return ResultCodeConstant.SUCCESS;
	}

	@Override
	public ResultCodeConstant update(T data) {
		ResultCodeConstant resultCodeConstant = validateData(data);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		data = getRepository().update(data);
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		return ResultCodeConstant.SUCCESS;
	}

	@Override
	public ResultCodeConstant saveOrUpdate(T data) {
		ResultCodeConstant resultCodeConstant = validateData(data);
		if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
			return resultCodeConstant;
		}
		data = getRepository().saveOrUpdate(data);
		if(isCachingEnabled()) {
			addCacheEntry(data.getId(), data);
		}
		return ResultCodeConstant.SUCCESS;
	}

	@Override
	public List<T> saveOrUpdateAll(List<T> dataList) {
		// check if list is empty then return the result code with empty list message
		if(dataList == null  || dataList.isEmpty()) {
			return Collections.emptyList();
		}
		
		// validate all the data 
		for(T data : dataList) {
			ResultCodeConstant resultCodeConstant = validateData(data);
			if(ResultCodeConstant.SUCCESS != resultCodeConstant) {
				return Collections.emptyList();
			}
		}
		
		// save the data list
		dataList = getRepository().saveOrUpdateAll(dataList.iterator());
		if(isCachingEnabled()) {
			if(dataList != null) {
				for(T data :  dataList) {
					data.setDefaultValueForObject();
					if(isCachingEnabled()) {
						addCacheEntry(data.getId(), data);
					}
				}
			}
		}
				
		return dataList;
	}

	@Override
	public Integer delete(T data) {
		int count = getRepository().delete(data);
		if(count <= 0 ) {
			throw new OperationFailedException("Error occured while deleting the data");
		}
		if(isCachingEnabled()) {
			removeCacheEntry(data.getId());
		}
		return count;
	}

	@Override
	public T getById(T data) {
		if(data.getId() == null) {
			return null;
		}
		T dataTemp = null;
		if(isCachingEnabled()) {
			dataTemp = getCacheEntry(data.getId());
			if(isSetDefaultRequired()) {
				dataTemp.setDefaultValueForObject();
			}
		}
		if(dataTemp == null) {
			dataTemp = getRepository().getById(data.getId());
			if(dataTemp != null) {
				if(isSetDefaultRequired()) {
					dataTemp.setDefaultValueForObject();
				}
				if(isCachingEnabled()) {
					addCacheEntry(dataTemp.getId(), dataTemp);
				}
			}
		}
		return dataTemp;
	}

	@Override
	public List<T> getActiveData(SearchData<T> searchData) {
		List<T> dataList = Collections.emptyList();
		if(isCachingEnabled()) {
			dataList = new ArrayList<>(getAllCacheEntry());
		}
		
		if(dataList == null || dataList.isEmpty()) {
			dataList = getRepository().getActiveData(searchData);
			if(isSetDefaultRequired()) {
				for(T data : dataList) {
					data.setDefaultValueForObject();
					if(isCachingEnabled()) {
						addCacheEntry(data.getId(), data);
					}
				}
			}
		}
		return dataList;
	}

	@Override
	public List<T> getAll(SearchData<T> searchData) {
		return getRepository().getAll(searchData);
	}
	
	
	@Override
	public ResultCodeConstant validateData(T entity) {
		return ResultCodeConstant.SUCCESS;
	}
	
	@Override
	public boolean isSetDefaultRequired() {
		return false;
	}
	
	@Override
	public boolean isCachingEnabled() {
		return false;
	}

	public abstract E getRepository();
}

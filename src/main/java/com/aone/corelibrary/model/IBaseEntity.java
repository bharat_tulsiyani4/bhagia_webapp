package com.aone.corelibrary.model;

import java.io.Serializable;

public interface IBaseEntity<K extends Serializable> {

	public K getId();

	public void setId(K id);
	
	public boolean isDeleted();

	public void setDeleted(boolean deleted);
	
	public boolean setDefaultValueForObject();
	
}

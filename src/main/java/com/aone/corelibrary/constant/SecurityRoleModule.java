package com.aone.corelibrary.constant;

public class SecurityRoleModule {

	public static final String BASE_PREFIX = "";
	public static final String BASE_SUFFIX = "_";
	
	public static final String USER = BASE_PREFIX + "USER" + BASE_SUFFIX;
	public static final String USER_ROLE_GROUP = BASE_PREFIX + "USER_ROLE_GROUP" + BASE_SUFFIX;
	public static final String COUNTRY = BASE_PREFIX + "COUNTRY" + BASE_SUFFIX;
	public static final String STATE = BASE_PREFIX + "STATE" + BASE_SUFFIX;
	public static final String CITY = BASE_PREFIX + "CITY" + BASE_SUFFIX;
	public static final String CONFIGURATION_MENU = BASE_PREFIX + "CONFIGURATION_MENU" + BASE_SUFFIX;
	public static final String CONTACT = BASE_PREFIX + "CONTACT" + BASE_SUFFIX;
	public static final String SALES = BASE_PREFIX + "SALES" + BASE_SUFFIX;
	public static final String SALES_ORDER = BASE_PREFIX + "SALES_ORDER" + BASE_SUFFIX;
}

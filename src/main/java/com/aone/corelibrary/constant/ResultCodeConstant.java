package com.aone.corelibrary.constant;

import java.util.HashMap;
import java.util.Map;

public enum ResultCodeConstant {
	
	/*************************************************************
	 * SUCCESS CODES- starts from 2000 to 2999                   *
	 *************************************************************/
	SUCCESS(2200, "Success"),
	
	
	/*************************************************************
	 * REDIRECT OR OTHER SERVER CODES - starts from 3000 to 3999 *
	 * ***********************************************************/
	REDIRECT(3302, "Internal server error"),
	
	
	/*************************************************************
	 * VALIDATION OR PROCESSING CODES- starts from 4000 to 4999  *
	 * ***********************************************************/
	UNAUTHORIZED(401, "Unauthorized"),
	FORBIDDEN(403, "Forbidden"),
	
	PARAMETER_NOT_FOUND(4404, "Parameter value not found"),
	RESULT_NOT_FOUND(4405, "Result value not found"),
	
	// State related result codes - > 05
	COUNTRY_NAME_NOT_FOUND(4404051, "Country name not found"),
	COUNTRY_NAME_ALREADY_EXIST(4404052, "Country name already exist"),
		
	// State related result codes - > 06
	STATE_NAME_NOT_FOUND(4404061, "State name not found"),
	STATE_NAME_ALREADY_EXIST(4404062, "State name already exist"),
	STATE_COUNTRY_DETAILS_MISSING(4404063, "Country details required"),
	STATE_CODE_GT_ZERO(4404064, "State code is required"),
	STATE_CODE_ALREADY_EXIST(4404065, "State code already exist"),
	
	// State related result codes - > 07
	CITY_NAME_NOT_FOUND(4404071, "City name not found"),
	CITY_NAME_ALREADY_EXIST(4404072, "City name already exist"),
	CITY_STATE_REQUIRED(4404073, "State details required"),
	
	// State related result codes - > 08
	ADDRESS_COUNTRY_CODE_NOT_FOUND(4404081, "Address country not found"),
	ADDRESS_STATE_CODE_NOT_FOUND(4404082, "Address  state not found"),
	ADDRESS_CITY_CODE_NOT_FOUND(4404083, "Address  city not found"),
	ADDRESS_FOREIGN_KEY_NOT_FOUND(4404084, "Address mapping not found"),
	ADDRESS_MOUDLE_NAME_NOT_FOUND(4404085, "Address module name not found"),
	
	// CONTACT_OTHER_DETAILS result codes- > 10
	CONTACT_OTHER_DETAILS_CONTACT_GT_ZERO(4404101, "Contact details are required"),

	// CONTACT_OTHER_DETAILS result codes- > 11
	CONTACT_BANK_DETAILS_CONTACT_GT_ZERO(4404111, "Contact bank details are required"),
	
	// FILES_DATA result codes- > 12
	FILES_DATA_FOREIGN_KEY_NOT_FOUND(4404081, "Files data mapping not found"),
	FILES_DATA_MOUDLE_NAME_NOT_FOUND(4404081, "Files Data module name not found"),
	
	// Super Stockiest related result codes - > 17
	USER_NAME_NOT_FOUND(4404171, "User name not found"),
	USER_WITH_SAME_NAME_ALREADY_EXIST(4404175, "User with the same name already exist"),
	USER_NAME_ALREADY_EXIST(4404172, "User name already exist"),
	USER_USERNAME_REQURIED(4404173, "Username is required"),
	USER_PASSWORD_REQURIED(4404174, "Password is required"),
	
	// Purchase result costant -> 20
	USER_ROLE_NAME_NOT_FOUND(4404201, "User role name not found"),
	USER_ROLE_NAME_ALREADY_EXIST(4404202, "User role name already exist"),
	
	// Contact result costant -> 21
	CONTACT_FULL_NAME_NOT_FOUND(4404211, "Contact full name not found"),
	CONTACT_FULL_NAME_ALREADY_EXIST(4404214, "Contact full name already exist"),
	CONTACT_MOBILE1_NOT_FOUND(4404212, "Contact mobile1 not found"),
	CONTACT_MOBILE1_ALREADY_EXIST(4404213, "Contact mobile1 already exist"),
	
	// Contact result costant -> 22
	SALES_DETAILS_SALES_ID_NOT_FOUND(4404221, "Sales Id not found for sales details"),
	SALES_DETAILS_ITEM_NAME_NOT_FOUND(4404222, "Item name not found for sales details"),
	SALES_DETAILS_ITEM_QTY_NOT_FOUND(4404223, "Item quantity not found for sales details"),
	SALES_DETAILS_UPDATE_CONTACT_ID_ERROR(4404223, "error occurred while updating contact id in sales bill"),
	
	/*************************************************************
	 * INTERNAL SERVER CODES - starts from 5000 to 5999  *
	 * ***********************************************************/
	INTERNAL_ERROR(5500, "Internal server error"),
	NETWORK_AUTHENTICATION_REQUIRED(5511, "Network Authentication Required"),
	DEVICE_ID_NOT_FOUND(5512, "Login from another device, Please ask admin to configure your device details");
	;
	
	private Integer resultCode;
	private String message;
	
	private static Map<Integer, ResultCodeConstant> codeWiseResultCodeMap  = new HashMap<>();
	
	static {
		ResultCodeConstant codeConstant = null;
		for(ResultCodeConstant resultCodeConstant : values()) {
			codeConstant = codeWiseResultCodeMap.get(resultCodeConstant.getResultCode());
			if(codeConstant != null) {
				System.out.println("Apllication can not be started as resulcodes are duplicate");
			}
		}
	}
	
	private ResultCodeConstant(Integer resultCode, String message) {
		this.resultCode = resultCode;
		this.message = message;
	}

	public Integer getResultCode() {
		return resultCode;
	}

	public String getMessage() {
		return message;
	}

	public static ResultCodeConstant getResultCodeConstant(Integer resultCode) {
		return codeWiseResultCodeMap.get(resultCode);
	}
	
}

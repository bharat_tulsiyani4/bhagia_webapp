package com.aone.commons;

import java.time.format.DateTimeFormatter;

public class ConfigConstant {
	
	/**
	 * DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	 */
	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"); 

	private ConfigConstant() {
		super();
	}
	
}

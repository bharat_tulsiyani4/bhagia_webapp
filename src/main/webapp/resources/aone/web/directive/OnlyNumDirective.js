/**
 * http://usejsdoc.org/
 */
getModule().directive('onlyNum', function() {
            return function(scope, element, attrs) {
                element.bind("keypress", function(event) {
//                    var charCode = (typeof event.which === "number") ? event.which : event.keyCode,
//                            chr = String.fromCharCode(charCode); //convert it to a character
//                    if (isNaN(parseInt(chr, 10)))
//                        event.preventDefault();
                    var code = event.which || event.keyCode;
                    if (!(event.shiftKey == false &&
                            (
                                    code == 46 ||
                                    code == 8 ||
                                    code == 37 ||
                                    code == 39 ||
                                    code == 190 ||
                                    code == 110 ||
                                    (code >= 48 && code <= 57)
                                    )
                            )
                            )
                    {
                        event.preventDefault();
                    }
                });
            };
        });
getModule().directive('aoneTextValidation', function($http) {
	return {
		require : 'ngModel',
		restrict : 'AE',
		scope : {
			aoneObject: '=',
			aoneIsValidFlag : '=',
			aoneValidationMessageObj : '=',
			aoneEqualTo : '=',
			aoneEqualText : '@',
			aoneRequired : '@',
			aoneIsEmail : '@',
			aoneMinLength : '@',
			aoneMaxLength : '@',
			aoneRestrictedChars : '@',
			aoneDisAllowSpace : '@',
			aoneCheckUnique :'@',
			aoneCheckUniqueFlag : '=',
			aoneCheckUniqueUrl : '@'
		},
		link : function(scope, elm, attrs, ctrl) {
			
			if (scope.aoneDisAllowSpace != null && scope.aoneDisAllowSpace != undefined && (scope.aoneDisAllowSpace === true || scope.aoneDisAllowSpace === 'true')) {
				elm.bind("keypress", function(event) {
					if(event.which === 32){
						return false;
					}
				});
			}
			
			// below method checks all validation parameters
			scope.aoneObject = scope.aoneObject || {};
			scope.aoneObject.validate = function(viewValue) {
				
				// check if validation is enabled or disabled
				if (scope.aoneIsValidFlag == null && scope.aoneIsValidFlag == undefined) {
					return;
				}

				// check if equal to
				if (scope.aoneEqualTo != null && scope.aoneEqualTo != undefined) {

					if (viewValue !== scope.aoneEqualTo) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						if (scope.aoneEqualText) {
							scope.aoneValidationMessageObj = "Not equal" ;
						} else {
							scope.aoneValidationMessageObj = "Not equal to " + scope.aoneEqualText;
						}
						return;
					}
				}

				// check if string is blank
				if (scope.aoneRequired != null && scope.aoneRequired != undefined && (scope.aoneRequired === true || scope.aoneRequired === 'true')) {
					var isBlank = viewValue === ''
					if (isBlank) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Required";
						return;
					}
				}

				var length;
				// check min length validation
				if (scope.aoneMinLength != null && scope.aoneMinLength != undefined) {
					length = viewValue.length;
					if (length < scope.aoneMinLength) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Minimum length allowed is " + scope.aoneMinLength;
						return;
					}
				}

				// check max length validation
				if (scope.aoneMaxLength != null && scope.aoneMaxLength != undefined) {
					length = viewValue.length;
					if (length > scope.aoneMaxLength) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Maximum length allowed is " + scope.aoneMaxLength;
						return;
					}
				}

				// check if valida email id
				if (scope.aoneIsEmail != null && scope.aoneIsEmail != undefined && ( scope.aoneIsEmail === true || scope.aoneIsEmail === 'true')) {
					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if (!regex.test(viewValue)) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Invalid email address";
						return;
					}
				}

				if (scope.aoneRestrictedChars != null && scope.aoneRestrictedChars != undefined) {
					if (scope.aoneRestrictedChars === '-aone-') {
						scope.aoneRestrictedChars = '%$&^@!*';
					}
					var regex = new RegExp("[" + scope.aoneRestrictedChars + "]");
					if (regex.test(viewValue)) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Invalid characters, " + scope.aoneRestrictedChars + " are not allowed";
						return;
					}
				}
				
				/*if (scope.aoneDisAllowSpace != null && scope.aoneDisAllowSpace != undefined && (scope.aoneDisAllowSpace === true || scope.aoneDisAllowSpace === 'true')) {
					var regex = /\s/g;
					if (regex.test(viewValue)) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Spaces are not allowed";
						return;
					}
				}*/

				// check for unique name
				if (scope.aoneCheckUnique != null && scope.aoneCheckUnique != undefined && (scope.aoneCheckUnique == true || scope.aoneCheckUnique == "true")) {
					console.log('inside');
					
					scope.aoneCheckUniqueFlag = true;
					
					var reqData = {};
					reqData.data = { code : viewValue };
					//reqData.data = scope.aoneCheckUniqueObj();
					$http({
						method :'POST',
						url : scope.aoneCheckUniqueUrl,
						data : reqData
					}).then(
						function(response, status, headers, config) {
							scope.aoneCheckUniqueFlag = false;
							
							if (response.data.data == true || response.data.data == 'true') {
								scope.aoneIsValidFlag = false;
								ctrl.$setValidity('invalidData', false);
								scope.aoneValidationMessageObj = "not available";								
								return;
							} else {
								scope.aoneIsValidFlag = true;
								ctrl.$setValidity('invalidData', true);
								scope.aoneValidationMessageObj = "";
								return;
							}
						},
						function(response) {
							//alert("O no, you a problem man!!!");
							scope.aoneCheckUniqueFlag = false;
							scope.aoneIsValidFlag = false;
							ctrl.$setValidity('invalidData', false);
							scope.aoneValidationMessageObj = "Show notifications";
							return;
						}
					);
					// as function will make the valid flag to true once the response is received from server 
					return;
				}


				//if all validations are satisfied
				scope.aoneIsValidFlag = true;
				ctrl.$setValidity('invalidData', true);
				scope.aoneValidationMessageObj = "";
			};
		}
	}
});
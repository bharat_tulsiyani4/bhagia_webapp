getModule().directive('aoneFloatNumberValidation', function() {
	return {
		require : 'ngModel',
		restrict : 'A',
		scope : {
			aoneObject : '=',
			aoneIsValidFlag : '=',
			aoneValidationMessageObj : '=',
			aoneRequired : '@',
			aoneMinValue : '@',
			aoneMaxValue : '@'
		},
		link : function(scope, elm, attrs, ctrl) {
			// prevent key not valid for float number
			elm.bind("keypress", function(event) {
				if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
					event.preventDefault();
				}
			});

			elm.bind('blur', function() {
				var index = $(this).val().indexOf(".");
				if (index > -1 && checkDigit($(this).val().charAt(index + 1)))
					$(this).val(parseFloat($(this).val()).toFixed(2));
				ctrl.$setViewValue($(this).val());
			});

			// below method checks all validation parameters
			scope.aoneObject = scope.aoneObject || {};
			scope.aoneObject.validate = function(viewValue) {

				// check if validation is enabled or disabled
				if (scope.aoneIsValidFlag == null && scope.aoneIsValidFlag == undefined) {
					return;
				}

				// check if string is blank
				if (scope.aoneRequired) {
					var isBlank = viewValue === ''
					if (isBlank) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Required";
						return;
					}
				}

				// check if only digits are entered
				var isDigit = checkDigit(viewValue);
				if (!isDigit) {
					scope.aoneIsValidFlag = false;
					ctrl.$setValidity('invalidData', false);
					scope.aoneValidationMessageObj = "Only digits are allowed";
					return;
				}

				var value;
				// check min value validation
				if (scope.aoneMinValue != null && scope.aoneMinValue != undefined) {
					value = parseFloat(viewValue);
					if (value < scope.aoneMinValue) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Minimum value is " + scope.aoneMinValue;
						return;
					}
				}

				// check max value validation
				if (scope.aoneMaxValue != null && scope.aoneMaxValue != undefined) {
					value = parseFloat(viewValue);
					if (value > scope.aoneMaxValue) {
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Maximum value is " + scope.aoneMaxValue;
						return;
					}
				}

				//if all validations are satisfied
				scope.aoneIsValidFlag = true;
				ctrl.$setValidity('invalidData', true);
				scope.aoneValidationMessageObj = "";
			};

			function checkDigit(input) {
				var regex = /\d/;
				return regex.test(input);
			}
		}
	}
});
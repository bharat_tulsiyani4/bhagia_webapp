getModule().directive('aoneNumberValidation', function() {
	return {
		require : 'ngModel',
		restrict : 'A',
		scope : {
			aoneObject : '=',
			aoneIsValidFlag : '=',
			aoneValidationMessageObj : '=',
			aoneRequired : '@',
			aoneMinValue : '@',
			aoneMaxValue : '@'
		},
		link : function(scope, elm, attrs, ctrl) {
			// prevent key not valid for integer number
			elm.bind("keypress", function(event) {
				var code = event.which || event.keyCode;
                if (!(event.shiftKey == false &&
                        (
                               // code == 46 ||
                                code == 8 ||
                                code == 37 ||
                                code == 39 ||
                                code == 190 ||
                                code == 110 ||
                                (code >= 48 && code <= 57)
                                )
                        )
                        )
                {
                    event.preventDefault();
                }

			});

			// below method checks all validation parameters
			scope.aoneObject = scope.aoneObject || {};
			scope.aoneObject.validate = function(viewValue) {
				
				// check if validation is enabled or disabled
				if(scope.aoneIsValidFlag == null && scope.aoneIsValidFlag == undefined){
					return;
				}
				
				// check if string is blank
				if(scope.aoneRequired){
					var isBlank = viewValue === ''
					if(isBlank){
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Required";
						return;
					}
				}
				
				// check if only digits are entered
				var isDigit = checkDigit(viewValue);
				if(!isDigit){
					scope.aoneIsValidFlag = false;
					ctrl.$setValidity('invalidData', false);
					scope.aoneValidationMessageObj = "Only digits are allowed";
					return;
				}

				var value;
				// check min value validation
				if(scope.aoneMinValue != null && scope.aoneMinValue != undefined){
					value = parseInt(viewValue);
					if(value < scope.aoneMinValue){
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Minimum value is " + scope.aoneMinValue;						
						return;
					}
				}
				
				// check max value validation
				if(scope.aoneMaxValue != null && scope.aoneMaxValue != undefined){
					value = parseInt(viewValue);
					if(value > scope.aoneMaxValue){
						scope.aoneIsValidFlag = false;
						ctrl.$setValidity('invalidData', false);
						scope.aoneValidationMessageObj = "Maximum value is " + scope.aoneMaxValue;						
						return;
					}
				}
				
				//if all validations are satisfied
				scope.aoneIsValidFlag = true;
				ctrl.$setValidity('invalidData', true);
				scope.aoneValidationMessageObj = "";
			};
			
			function checkDigit(input) {
					var regex = /\d/;
					return regex.test(input);
			}
		}
	}
});
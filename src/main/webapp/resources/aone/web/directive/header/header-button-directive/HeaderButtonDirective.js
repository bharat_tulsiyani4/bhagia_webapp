
getModule()
		.directive(
				'divHeaderButtons',
				function($timeout) {
					return {
						restrict : 'AE',
						templateUrl : getContextPath() + '/directive/header/header-button-directive/headerButton-tpl.html',
						scope : {
							//button related options
							onDeleteAll : '&',
							isShowDeleteAllBtnLoading : '&',
							onAdd : '&',
							isShowAddBtnLoading : '&',
							onRefresh : '&',
							isShowRefreshBtnLoading : '&',
							// action button methods
							isEnableActionButton : '&',
							markActive : '&',
							markInactive : '&',
							checkAddButtonPermission : '&',
							checkDeleteButtonPermission : '&',
							getActiveButtonLabel: '&',
							getInActiveButtonLabel: '&'
						},
						link : function(scope, element, attrs) {
							scope.hello = function (){
								Logger.logDebug('header buttons directive loaded');
							}
						}
					};
				});


getModule()
		.directive(
				'divHeaderTitle',
				function($timeout) {
					return {
						restrict : 'AE',
						templateUrl : getContextPath() + '/directive/header/header-title-directive/headerTitle-tpl.html',
						scope : {
							// header section
							pageTitle : '='
						},
						link : function(scope, element, attrs) {
							
						}
					};
				});

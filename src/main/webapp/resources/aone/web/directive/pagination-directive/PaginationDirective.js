/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
	getModule().directive('divPaginationControl', function($timeout) {
            return {
                restrict: 'AE',
                templateUrl: getContextPath()+'/directive/pagination-directive/pagination-tpl.html',
                scope: {
//                    maxSize: '=',
                    onPageChange: '&',
                    totalItems: '=',
                    currentPage: '=',
                    itemsPerPage: '=',
                    boundaryLinks: '@',
                    previousText: '@',
                    nextText: '@',
                    firstText: '@',
                    lastText: '@',
                    directionLinks: '@',
                    totalCountVisible: '@',
                    totalChargesVisible: '@',
                    totalCharges: '@'
                },
                link: function(scope, element, attrs) {
                    //set Default Values to attributes
                    scope.firstBtnText = scope.firstText == undefined ? "First" : scope.firstText;
                    scope.lastBtnText = scope.lastText == undefined ? "Last" : scope.lastText;
                    scope.prevBtnText = scope.previousText == undefined ? "Prev" : scope.previousText;
                    scope.nextBtnText = scope.nextText == undefined ? "Next" : scope.nextText;
                    scope.boundaryLinksFlag = scope.boundaryLinks == undefined ? false : scope.boundaryLinks;
                    scope.directionLinksFlag = scope.directionLinks == undefined ? true : scope.directionLinks;
                    scope.totalCounts = scope.totalItems;
                    scope.totalCharges = isNaN(scope.totalCharges) ? 0 : parseFloat(scope.totalCharges);
                    scope.currentPage = scope.currentPage == undefined ? 1 : scope.currentPage;
                    scope.lastNavPage = Math.ceil(scope.totalCounts / scope.itemsPerPage);
                    scope.lastNavPage = scope.lastNavPage == 0 ? 1 : scope.lastNavPage;
                    scope.totalCountVisibleFlag = angular.isUndefined(scope.totalCountVisible)? true : (scope.totalCountVisible == "false")?false:true;
                    scope.totalChargesVisibleFlag = angular.isUndefined(scope.totalChargesVisible)? false : (scope.totalChargesVisible == "false")?false:true;
//                    scope.ngModel = scope.cureentNavPage;
                    scope.$watch('itemsPerPage', function(newValue, oldValue) {
                        if (newValue != oldValue) {
                            scope.lastNavPage = Math.ceil(scope.totalCounts / newValue);
                            scope.lastNavPage = scope.lastNavPage == 0 ? 1 : scope.lastNavPage;
                        }
                    });
                    scope.$watch('totalItems', function(newValue, oldValue) {
                        scope.totalCounts = newValue;
                        scope.lastNavPage = Math.ceil(scope.totalCounts / scope.itemsPerPage);
                        scope.lastNavPage = scope.lastNavPage == 0 ? 1 && (scope.currentPage = 1) : scope.lastNavPage;
                    });
                    scope.oldPageNumber = 1;
                    scope.$watch('currentPage', function(newValue, oldValue) {
                        if (newValue >= 1 && newValue <= scope.lastNavPage && oldValue >= 1 && oldValue <= scope.lastNavPage) {
                            scope.oldPageNumber = oldValue;
//                            scope.currentPage = scope.oldPageNumber;
                        }
                    });
                    scope.goToPage = function(event, currentPage) {
                        var code = event.keyCode || event.which;
                        if (parseInt(code) === 13) { //Enter keycode
                            //Do something
                            cureentPage = parseInt(currentPage);
                            if (currentPage <= 0 && currentPage > scope.lastNavPage) {
                                scope.currentPage = scope.oldPageNumber;
                            }
                            else
                                scope.setCurrentPage(currentPage, null);
                        }
                    };
                    scope.setCurrentPage = function(pageNum, action) {
                        if (pageNum >= 1 && pageNum <= scope.lastNavPage) {
                            pageNum = parseInt(pageNum);
                            if (action === "next")
                            {
                                if (pageNum >= scope.lastNavPage)
                                    $("#nextBtn").prop("disabled", true);
                                else
                                    $("#nextBtn").prop("disabled", false);
                                pageNum = pageNum + 1;
                            }
                            else if (action === "prev")
                            {
                                if (pageNum <= 1)
                                    $("#prevBtn").prop("disabled", true);
                                else
                                    $("#prevBtn").prop("disabled", false);
                                pageNum = pageNum - 1;
                            }
                            scope.currentPage = pageNum;
                            $timeout(function() {
                                scope.onPageChange();
                            }, 100);
                        } else {
                            scope.currentPage = scope.oldPageNumber;
                        }
                    };
                }
            };
        });
        
 
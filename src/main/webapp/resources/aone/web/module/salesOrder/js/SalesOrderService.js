(function() {
	'use strict';
	
	getModule().service('SalesOrderService', getService);
	
	function getService($http) {
		var module = "salesOrder";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		service.getActiveDataWithTotal = getActiveDataWithTotal;
		
		function getActiveDataWithTotal(entityObj) {
			Logger.logDebug(module, "AngularJs - Service > save...");
			var reqData = {};
			reqData.data = entityObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getActiveDataWithTotal');
		}
		
		return service;
	}
})();
(function() {
	'use strict';

	getModule().controller('SalesOrderController',SalesOrderController);
	
	SalesOrderController.$inject = ['$scope', '$window', '$location', '$uibModal', '$confirm', 'SalesOrderService', 'notify'];

	function SalesOrderController($scope,$window,$location, $uibModal, $confirm, SalesOrderService, notify) {
		Logger.logDebug(MODULES.SALES_ORDER, "AngularJs - Controller Loaded...")

		var vm = this;
		vm.CURRENT_MODULE = MODULES.SALES_ORDER;
		vm.TITLE = "Order";
		vm.salesOrders = [];
		vm.salesOrderObj = {			
			parentId : 0,
			name : "",
			isActive : true
		};
		
		
		vm.totalAmount = 0;
		vm.baseTotalAmount = 0;
		vm.addUpdateUrl = '/salesOrder/addUpdate';
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getActiveDataWithTotal = getActiveDataWithTotal;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.getPermissionModule = getPermissionModule;
		
		
		function getScope(){
			return $scope;
		}
				
		function getPermissionModule(){
			return "ORDER";
		}
		
		// start date 
		vm.startDate = new Date();

		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		vm.getActiveButtonLabel = getActiveButtonLabel;
		vm.getInActiveButtonLabel = getInActiveButtonLabel;
		vm.refreshData = refreshData;
		
		// call initialization functions
		vm.init = init;
		vm.getSearchObj = getSearchObj;
		vm.clearSearch = clearSearch;
		vm.onGenerateTaxInvoice=onGenerateTaxInvoice;
		vm.init();
		
		function getActiveButtonLabel(){
			return "Mark Delivered";
		}
		
		function getInActiveButtonLabel(){
			return "Mark Not Delivered";
		}
		
		
		function onGenerateTaxInvoice(salesOrderId) {
			console.log(salesOrderId);
			
			var url="web/rest/salesOrder/generateDistributorTaxInvoice?salesOrderId="+salesOrderId;
			$window.open(url, '_blank');
//		      const URL = environment.SERVER_URL + this.salesOrderService.getModuleName() + '/generateTaxInvoice?salesOrderId=' + salesOrderId;
//		      window.open(
//		        URL,
//		        '_blank' // <- This is what makes it open in a new window.
//		      );
		}
		 
		function getSearchObj() {
			var paginationData = {
				"pageNumber" : vm.selectedPage,
				"rowsPerPage" : vm.recordsPerPage
			}
			var data = {
				agentMaster:{
					id : 0
				},
				superStockiestId: 0,
				distributorId: 0,
				startDate : getFormattedStartDate(vm.startDate, true),
				endDate : getFormattedEndDate(vm.endDate, true)
			};
			var reqData = {
				orderingData : vm.sort,
				paginationData : paginationData,
				data : data,
			};
			return reqData; 
		}
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.salesOrders = dataList;
		}
		
		function getList(){
			if(vm.salesOrders){
				angular.forEach(vm.salesOrders, function(salesOrderObj){
					if(salesOrderObj.orderDate){
						if(salesOrderObj.orderDate.indexOf('T') > 0) {
							salesOrderObj.orderDateView = moment(salesOrderObj.orderDate).format('DD-MM-YYYY');
						}	
					}
				});
			}
			return vm.salesOrders;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return SalesOrderService;
		}
		
		function getObject() {
			return vm.salesOrderObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
		}
			
		vm.getExcelExportList = function (){
			vm.showTableLoading = true;
			
			Logger.logDebug(vm.getModule(), "inside getExcelExportList");
			
			var searchObj = vm.getSearchObj();
			
			searchObj.paginationData = null;
			vm.getService().getUnDeletedData(searchObj)
				.success(successExportGetData)
				.error(errorExportFunction);
			
			function successExportGetData(data, status, headers, config) {
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				var index;
				var exportList = [];
				var exportObj;
				var dataList = data.dataList;
				for(index = 0; index < dataList.length; index++){
					exportObj = dataList[index];
					exportObj.agentName = exportObj.agentMaster.name;
					exportObj.customerName = exportObj.contact.companyName; 
					if(exportObj.contact.shopAddress){
						exportObj.shopLocation = exportObj.contact.shopAddress.street;
					}
					exportObj.agentLocation = exportObj.location; 
					exportObj.orderDate = exportObj.orderDate;
					exportObj.subTotal = exportObj.subTotal;
					exportList.push(exportObj);
				}
				
				vm.exportList = exportList;
				document.getElementById('btnExport').click();
				vm.disableCurrentLoadingButton();
				vm.showTableLoading = false;
			}
			
			function errorExportFunction(data, status, headers, config){
				vm.dangerNotify();
				vm.showTableLoading = false;
				
				vm.disableCurrentLoadingButton();
			}
		}
		
		vm.getExportList = function(){
			return vm.exportList;
		}
		
		function refreshData(){
			vm.enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.REFRESH);
			vm.getActiveDataWithTotal();
		}
		function clearSearch(){
			vm.selectedPage = 1;
			vm.recordsPerPage = 10;
			vm.startDate = new Date();
			vm.endDate = new Date();
			vm.sort={column: "id", descending: false};
			vm.agentMasterObj = {};
			vm.distributorObj = {};
			vm.superStockiestObj={};
			vm.init();
		}
		
		function getActiveDataWithTotal(){
			vm.showTableLoading = true;
			
			$("tr[data-expanded='true']").trigger('click');
			console.log(vm.sort);
			
			vm.selectedObject = {};
			vm.selectedIndex = -1;
			Logger.logDebug(vm.getModule(), "inside get All");
			vm.getService().getActiveDataWithTotal(vm.getSearchObj())
			.success(successGetActiveDataWithTotal)
			.error(errorGetActiveDataWithTotal);
			
			function successGetActiveDataWithTotal(data, status, headers, config) {
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.setList(data.data.salesOrderList);
				vm.totalRecords = data.totalRecords;
				vm.totalAmount = data.data.totalAmount;
				vm.totalBaseAmount = data.data.totalBaseAmount;
				Logger.logDebug(vm.getModule(), vm.getList());
				
				vm.disableCurrentLoadingButton();
				vm.showTableLoading = false;
				vm.isAllSelected = false;
			}
			
			function errorGetActiveDataWithTotal(data, status, headers, config){
				vm.dangerNotify();
				vm.showTableLoading = false;
				
				vm.disableCurrentLoadingButton();
			}
		}
		
	}
	
})();
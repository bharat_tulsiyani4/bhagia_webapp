(function() {
	'use strict';

	getModule().controller('AddSalesOrderController', addAgentMasterController);

	addAgentMasterController.$inject = [ '$location', 'SalesOrderService', 'notify', 'ContactService', '$confirm'];

	function addAgentMasterController($location, SalesOrderService, notify, ContactService, $confirm ) {
		Logger.logDebug(MODULES.ITEM_SCHEME, "AngularJs - Add Order Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Order";
			SalesOrderService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New Order";
			vm.salesOrderObj = {
				contactObj:{},
				salesOrderDetailsList : []
			};
		}

		vm.validationOptions = {};

		vm.CURRENT_MODULE = "Order Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init = init;
		// contact methods
		vm.setContactData = setContactData;
		vm.loadContactData = loadContactData;
		
		vm.salesOrderDetailsObj = {};
		vm.clearAddSalesOrderDetails = clearAddSalesOrderDetails;
		vm.addSalesOrderDetails = addSalesOrderDetails;
		vm.deleteSalesOrderDetailsMapping = deleteSalesOrderDetailsMapping;
		
		vm.orderDate = new Date();
		
		vm.contacts = [];
		vm.contactObj = {};
		
		vm.transactionTypeList = ['In-Ward', 'Out-Ward'];
		
		
		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);

		// call init function to load all the data
		vm.init();
		
		
		function init(){
			if (vm.screen === 'add') {
				vm.loadContactData();
			}
		}
		
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			vm.salesOrderObj.orderDate = getFormattedStartDate(vm.orderDate, false);
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.salesOrderObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return SalesOrderService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/salesOrder");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.salesOrderObj = data.data;
			
			if(vm.screen == 'edit'){
				
				vm.orderDate = vm.salesOrderObj.orderDate; 
				
				//setDefaultObject
				var index;
				vm.contacts = vm.salesOrderObj.salesOrderInitData.contactList;
				if(vm.salesOrderObj.contactId > 0){
					var contact = null;
					for(index = 0 ; index < vm.contacts.length ; index++){
						contact = vm.contacts[index];
						if(contact.id === vm.salesOrderObj.contactId){
							vm.salesOrderObj.contactObj = contact;
							break;
						}
					}
				}
			}
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}
		
		/**
		 * 
		 * CONTACTS DATA METHODS
		 * 
		 */
		function loadContactData(){
			var sort = {
					column : 'companyName',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			ContactService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.contacts = data.dataList;
				
				// set details for update 
				if(vm.screen == 'edit'){
					if(vm.salesOrderObj == null || vm.salesOrderObj == undefined){
						return;
					}
					if(vm.salesOrderObj.contactId > 0){
						var index = 0;
						var contact = null;
						for(index = 0 ; index < vm.contacts.length ; index++){
							contact = vm.contacts[index];
							if(contact.id === vm.salesOrderObj.contactId){
								vm.salesOrderObj.contactObj = contact;
							}
						}
					}
				}
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setContactData(contactObj){
			vm.contactObj = contactObj;
			vm.salesOrderObj.contactId = contactObj.id;
		}
		
		function clearAddSalesOrderDetails(){
			vm.salesOrderDetailsObj = {};
		}
		
		function addSalesOrderDetails() {
			var salesOrderDetailsObj = angular.copy(vm.salesOrderDetailsObj);
			// adding object to purchase item list
			
			vm.salesOrderObj.salesOrderDetailsList.push(salesOrderDetailsObj);
			vm.successNotify("Item details added successfiteully");
			vm.clearAddSalesOrderDetails();
		}
		
		function deleteSalesOrderDetailsMapping(index) {
			$confirm({
				text : 'Are you sure you want to delete?',
				ok : 'Yes',
				cancel : 'No'
			}, {
				size : 'sm',
				templateUrl : getContextPath() + '/directive/confirm-dialog/ConfirmDialog.html'
			}).then(function() {
				vm.salesOrderObj.salesOrderDetailsList.remove(index);
				vm.successNotify("Item details deleted");
			});
		}
		
	}
})();

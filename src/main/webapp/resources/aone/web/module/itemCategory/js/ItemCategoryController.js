(function() {
	'use strict';

	getModule().controller('ItemCategoryController',itemCategoryController);
	
	itemCategoryController.$inject = ['$scope', '$location', '$uibModal', '$confirm', 'ItemCategoryService', 'notify'];

	function itemCategoryController($scope, $location, $uibModal, $confirm, itemCategoryService, notify) {
		Logger.logDebug(MODULES.ITEM_CATEGORY, "AngularJs - Controller Loaded...")
		
		var vm = this;
		vm.CURRENT_MODULE = MODULES.ITEM_CATEGORY;
		vm.TITLE = "Item Category Master";
		vm.itemCategories = [];
		vm.itemCategoryObj = {			
			parentCategoryId : 0,
			name : "",
			isActive : true
		};
		vm.addUpdateUrl = '/itemCategory/addUpdate';
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.getScope = getScope;
		vm.getPermissionModule = getPermissionModule; 
		
		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		// call initialization functions
		vm.init = init;
		
		
		vm.init();
		
		function getScope(){
			return $scope;
		}
		
		function getPermissionModule(){
			return "ITEM_CATEGORY";
		}
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.itemCategories = dataList;
		}
		
		function getList(){
			return vm.itemCategories;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return itemCategoryService;
		}
		
		function getObject() {
			return vm.itemCategoryObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
		}
		
	}
	
})();
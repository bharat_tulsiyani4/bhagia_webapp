(function() {
	'use strict';

	getModule().controller('AddItemCategoryController', addItemCategoryController);

	addItemCategoryController.$inject = [ '$location', 'ItemCategoryService', 'notify' ];

	function addItemCategoryController($location, ItemCategoryService, notify) {
		Logger.logDebug(MODULES.ITEM_CATEGORY, "AngularJs - Add Item Category Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Item Category";
			ItemCategoryService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New Item Category";
			vm.itemCategoryObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				}
			}
		};

		vm.CURRENT_MODULE = "Item Category Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;

		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);

		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.itemCategoryObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return ItemCategoryService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/itemCategory");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.itemCategoryObj = data.data;
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

(function() {
	'use strict';
	
	getModule().service('ItemCategoryService', getService);
	
	function getService($http) {
		var module = "itemCategory";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
(function() {
	'use strict';

	getModule().controller('AddStateController', addStateController);

	addStateController.$inject = [ '$location', 'StateService', 'notify', 'CountryService'];

	function addStateController($location, StateService, notify, CountryService) {
		Logger.logDebug(MODULES.STATE, "AngularJs - Add State Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update State";
			StateService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New State";
			vm.stateObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				},
				code : {
					required : true,
					maxlength : 5,
					digits: true
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				},
				code : {
					required : MESSAGES.REQUIRED,
					maxlength : "Code should be maximum of 5 length...",
					digits: "Only digits are allowed in state code"
				}
			
			}
		};
		
		// country object
		vm.countries = [];
		vm.countryObj = {};

		vm.CURRENT_MODULE = "State Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init =  init;
		
		// country methods
		vm.setCountryData = setCountryData;
		vm.loadCountryData = loadCountryData;

		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);
		
		vm.init();
		
		function init(){
			vm.loadCountryData();
		}
		
		// load country data
		function loadCountryData(){
			var sort = {
					column : 'name',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			CountryService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.countries = data.dataList;
				if(vm.stateObj == null || vm.stateObj == undefined){
					return;
				}
				if(vm.stateObj.countryId > 0){
					var index = 0;
					var country = null;
					for(index = 0 ; index < vm.countries.length ; index++){
						country = vm.countries[index];
						if(country.id === vm.stateObj.countryId){
							vm.countryObj = country;
						}
					}
				}
				
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setCountryData(countryObj){
			vm.countryObjObj = countryObj;
			vm.stateObj.countryId = countryObj.id;
		}

		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.stateObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return StateService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/state");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.stateObj = data.data;
			
			if(vm.countries.length === 0){
				vm.loadCountryData();
			} else {
				if(vm.countryObj.countryId > 0){
					var index = 0;
					var country = null;
					for(index = 0 ; index < vm.countries.length ; index++){
						country = vm.countries[index];
						if(country.id === vm.stateObj.countryId){
							vm.countryObj = country;
						}
					}
				}
			}
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

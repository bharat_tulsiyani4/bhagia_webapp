(function() {
	'use strict';
	
	getModule().service('StateService', getService);
	
	function getService($http) {
		var module = "state";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
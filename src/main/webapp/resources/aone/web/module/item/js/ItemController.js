(function() {
	'use strict';

	getModule().controller('ItemController',itemController);
	
	itemController.$inject = ['$scope', '$location', '$uibModal', '$confirm', 'ItemService', 'notify'];

	function itemController($scope, $location, $uibModal, $confirm, itemService, notify) {
		Logger.logDebug(MODULES.ITEM, "AngularJs - Controller Loaded...")

		var vm = this;
		vm.CURRENT_MODULE = MODULES.ITEM;
		vm.TITLE = "Item Master";
		vm.items = [];
		vm.itemCbj = {			
			parentId : 0,
			name : "",
			isActive : true
		};
		vm.addUpdateUrl = '/item/addUpdate';
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.getScope = getScope;
		vm.getPermissionModule = getPermissionModule; 

		function getScope(){
			return $scope;
		}
				
		function getPermissionModule(){
			return "ITEM";
		}

		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		// call initialization functions
		vm.init = init;
		
		vm.init();
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.items = dataList;
		}
		
		function getList(){
			return vm.items;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return itemService;
		}
		
		function getObject() {
			return vm.itemObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
		}
		
	}
	
})();
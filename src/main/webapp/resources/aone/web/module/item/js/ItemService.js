(function() {
	'use strict';
	
	getModule().service('ItemService', getService);
	
	function getService($http) {
		var module = "item";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
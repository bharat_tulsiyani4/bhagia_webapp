(function() {
	'use strict';

	getModule().controller('AddItemController', addItemController);

	addItemController.$inject = [ '$location', 'ItemService', 'notify', 'ItemCategoryService', 'ItemSizeService', 'ItemSchemeService', '$confirm' ];

	function addItemController($location, ItemService, notify, ItemCategoryService, ItemSizeService, ItemSchemeService, $confirm) {
		Logger.logDebug(MODULES.ITEM, "AngularJs - Add Item Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Item";
			ItemService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New Item";
			vm.itemObj = {
				itemCategoryId : 0,
				itemSizesMappingList : [],
				itemSchemesMappingList : []
			};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				},
				salePrice : {
					 required: true,
					 number: true
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				},
				salePrice : {
					required : MESSAGES.REQUIRED,
					number: "Only numbers are allowed"
				}
			}
		};

		vm.CURRENT_MODULE = "Item Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init = init;
		vm.setItemCategoryData = setItemCategoryData;
		vm.loadItemCategoryData = loadItemCategoryData;
		
		// item size mapping methods
		vm.loadItemSizeData = loadItemSizeData;
		vm.addItemSizeMapping = addItemSizeMapping;
		vm.deleteItemSizeMapping = deleteItemSizeMapping;
		vm.setItemSizeDetails = setItemSizeDetails;
		vm.clearItemSizeMappingData = clearItemSizeMappingData;
		
		// item scheme mapping methods
		vm.loadItemSchemeData = loadItemSchemeData;
		vm.addItemSchemeMapping = addItemSchemeMapping;
		vm.deleteItemSchemeMapping = deleteItemSchemeMapping;
		vm.clearItemSchemeMappingData = clearItemSchemeMappingData;
		
		vm.itemCategories = [];
		vm.itemCategoryObj = {};
		
		vm.itemSizeList = [];
		vm.itemSizeObj = {};
		vm.itemSizeMappingObj = {};
		
		vm.itemSchemeList = [];
		vm.itemSchemeObj = {};
		
		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);

		// call init function to load all the data
		vm.init();
		
		function init(){
			vm.loadItemCategoryData();
			vm.loadItemSizeData();
			vm.loadItemSchemeData();
		}
		
		function loadItemCategoryData(){
			var sort = {
					column : 'name',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			ItemCategoryService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.itemCategories = data.dataList;
				if(vm.itemObj == null || vm.itemObj == undefined){
					return;
				}
				if(vm.itemObj.itemCategoryId > 0){
					var index = 0;
					var category = null;
					for(index = 0 ; index <= vm.itemCategories.length ; index++){
						category = vm.itemCategories[index];
						if(category.id === vm.itemObj.itemCategoryId){
							vm.itemCategoryObj = category;
						}
					}
				}
				
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setItemCategoryData(itemCategoryObj){
			vm.itemCategoryObj = itemCategoryObj;
			vm.itemObj.itemCategoryId = itemCategoryObj.id;
		}
		
		function setItemSizeObj(itemSizeObj){
			vm.itemSizeObj = itemSizeObj;
		}
		
		function loadItemSizeData(){
			var sort = {
					column : 'id',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			ItemSizeService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.itemSizeList = data.dataList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setItemSizeDetails() {
			Logger.logDebug(MODULES.ITEM, "Inside itemsize details function");
			// setting property to match the pojo fields
			vm.itemSizeMappingObj.sizeId = vm.itemSizeObj.id;
			vm.itemSizeMappingObj.size = vm.itemSizeObj.size;
		}
		
		function addItemSizeMapping() {
			var itemSizeMappingObj = angular.copy(vm.itemSizeMappingObj);
			// adding object to purchase item list
			
			vm.itemObj.itemSizesMappingList.push(itemSizeMappingObj);
			vm.successNotify("Item size mapping added successfiteully");
		}
		
		function clearItemSizeMappingData(){
			vm.itemSizeMappingObj = {};
			vm.itemSizeObj = {};
		}
		
		function deleteItemSizeMapping(index) {
			$confirm({
				text : 'Are you sure you want to delete?',
				ok : 'Yes',
				cancel : 'No'
			}, {
				size : 'sm',
				templateUrl : getContextPath() + '/directive/confirm-dialog/ConfirmDialog.html'
			}).then(function() {
				vm.itemObj.itemSizesMappingList.remove(index);
				vm.successNotify("Item size mapping deleted");
			});
		}
		
		function loadItemSchemeData(){
			var sort = {
					column : 'id',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			ItemSchemeService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.itemSchemeList = data.dataList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function addItemSchemeMapping() {
			var itemSchemeObj = angular.copy(vm.itemSchemeObj);
			itemSchemeObj.scehemId = itemSchemeObj.id; 
			
			var tempObj= {};
			tempObj.name = itemSchemeObj.name;
			tempObj.scehemId = itemSchemeObj.id;
			tempObj.discount = itemSchemeObj.discount;
			// adding object to purchase item list
			vm.itemObj.itemSchemesMappingList.push(tempObj);
			vm.successNotify("Item scheme mapping added successfully");
		}
		
		function clearItemSchemeMappingData(){
			vm.itemSchemeObj = {};
		}
		
		function deleteItemSchemeMapping(index) {
			$confirm({
				text : 'Are you sure you want to delete?',
				ok : 'Yes',
				cancel : 'No'
			}, {
				size : 'sm',
				templateUrl : getContextPath() + '/directive/confirm-dialog/ConfirmDialog.html'
			}).then(function() {
				vm.itemObj.itemSchemesMappingList.remove(index);
				vm.successNotify("Item scheme mapping deleted");
			});
		}
		
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.itemObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return ItemService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/item");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.itemObj = data.data;
			
			var index = 0;
			
			// set item size list
			vm.itemObj.itemSizesMappingList = [];
			for(index = 0; index < vm.itemObj.itemSizesList.length; index++){
				var itemSizeObj = vm.itemObj.itemSizesList[index];
				var tempObj= {};
				tempObj.size = itemSizeObj.size;
				tempObj.salePrice = itemSizeObj.salePrice;
				tempObj.basePrice = itemSizeObj.basePrice;
				tempObj.sizeId = itemSizeObj.id;
				tempObj.id = itemSizeObj.itemSizeMappingId;
				vm.itemObj.itemSizesMappingList.push(tempObj);
			}
			
			// set item scheme list
			vm.itemObj.itemSchemesMappingList = [];
			for(index = 0; index < vm.itemObj.itemSchemesList.length; index++){
				var itemSchemeObj = vm.itemObj.itemSchemesList[index];
				var tempObj= {};
				tempObj.name = itemSchemeObj.name;
				tempObj.scehemId = itemSchemeObj.id;
				tempObj.discount = itemSchemeObj.discount;
				vm.itemObj.itemSchemesMappingList.push(tempObj);
			}
			
			if(vm.itemCategories.length === 0){
				vm.loadItemCategoryData();
			} else {
				if(vm.itemObj.itemCategoryId > 0){
					var index = 0;
					var category = null;
					for(index = 0 ; index < vm.itemCategories.length ; index++){
						category = vm.itemCategories[index];
						if(category.id === vm.itemObj.itemCategoryId){
							vm.itemCategoryObj = category;
						}
					}
				}
			}
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

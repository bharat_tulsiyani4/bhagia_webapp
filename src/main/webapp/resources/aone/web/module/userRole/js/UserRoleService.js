(function() {
	'use strict';
	
	getModule().service('UserRoleService', getService);
	
	function getService($http) {
		var module = "userRole";
		var service = createServiceObject($http, module);
		
		service.getUnDeletedData = getSearchActiveData;
		service.getActiveData = getActiveData;
		
		function getSearchActiveData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getSearchActiveData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getSearchActiveData');
		}
		
		function getActiveData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getActiveData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getActiveData');
		}
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
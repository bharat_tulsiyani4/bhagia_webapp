(function() {
	'use strict';

	getModule().controller('CityController',CityController);
	
	CityController.$inject = ['$scope', '$location', '$uibModal', '$confirm', 'CityService', 'notify'];

	function CityController($scope, $location, $uibModal, $confirm, CityService, notify) {
		Logger.logDebug(MODULES.CITY, "AngularJs - Controller Loaded...")

		var vm = this;
		vm.CURRENT_MODULE = MODULES.CITY;
		vm.TITLE = "City Master";
		vm.countries = [];
		vm.cityObj = {			
			name : ""
		};
		vm.addUpdateUrl = '/city/addUpdate';
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.getScope = getScope;
		vm.getPermissionModule = getPermissionModule; 

		function getScope(){
			return $scope;
		}
				
		function getPermissionModule(){
			return "CITY";
		}

		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		// call initialization functions
		vm.init = init;
		
		vm.init();
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.countries = dataList;
		}
		
		function getList(){
			return vm.countries;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return CityService;
		}
		
		function getObject() {
			return vm.cityObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
		}
		
	}
	
})();
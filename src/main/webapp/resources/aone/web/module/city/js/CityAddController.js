(function() {
	'use strict';

	getModule().controller('AddCityController', addCityController);

	addCityController.$inject = [ '$location', 'CityService', 'notify', 'StateService'];

	function addCityController($location, CityService, notify, StateService) {
		Logger.logDebug(MODULES.CITY, "AngularJs - Add City Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update City";
			CityService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New City";
			vm.cityObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				}			
			}
		};
		
		// State object
		vm.states = [];
		vm.stateObj = {};

		vm.CURRENT_MODULE = "City Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init =  init;
		
		// State methods
		vm.setStateData = setStateData;
		vm.loadStateData = loadStateData;

		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);
		
		vm.init();
		
		function init(){
			vm.loadStateData();
		}
		
		// load State data
		function loadStateData(){
			var sort = {
					column : 'name',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			StateService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.states = data.dataList;
				if(vm.cityObj == null || vm.cityObj == undefined){
					return;
				}
				if(vm.cityObj.stateId > 0){
					var index = 0;
					var state = null;
					for(index = 0 ; index <= vm.states.length ; index++){
						state = vm.states[index];
						if(state.id === vm.cityObj.stateId){
							vm.stateObj = state;
						}
					}
				}
				
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setStateData(stateObj){
			vm.stateObj = stateObj;
			vm.cityObj.stateId = stateObj.id;
		}

		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.cityObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return CityService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/city");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.cityObj = data.data;
			
			if(vm.states.length === 0){
				vm.loadStateData();
			} else {
				if(vm.stateObj.stateId > 0){
					var index = 0;
					var state = null;
					for(index = 0 ; index < vm.states.length ; index++){
						state = vm.states[index];
						if(state.id === vm.cityObj.stateId){
							vm.stateObj = state;
						}
					}
				}
			}
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

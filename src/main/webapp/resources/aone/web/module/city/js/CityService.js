(function() {
	'use strict';
	
	getModule().service('CityService', getService);
	
	function getService($http) {
		var module = "city";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
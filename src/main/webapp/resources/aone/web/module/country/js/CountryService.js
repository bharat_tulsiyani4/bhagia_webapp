(function() {
	'use strict';
	
	getModule().service('CountryService', getService);
	
	function getService($http) {
		var module = "country";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
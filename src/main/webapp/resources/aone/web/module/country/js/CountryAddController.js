(function() {
	'use strict';

	getModule().controller('AddCountryController', addCountryController);

	addCountryController.$inject = [ '$location', 'CountryService', 'notify' ];

	function addCountryController($location, CountryService, notify) {
		Logger.logDebug(MODULES.COUNTRY, "AngularJs - Add COUNTRY Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Country";
			CountryService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New Country";
			vm.countryObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				}
			}
		};

		vm.CURRENT_MODULE = "Country Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;

		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);

		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.countryObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return CountryService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/country");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.countryObj = data.data;
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

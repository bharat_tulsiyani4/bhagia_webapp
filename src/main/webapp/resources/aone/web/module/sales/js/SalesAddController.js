(function() {
	'use strict';

	getModule().controller('AddSalesController', addAgentMasterController);

	addAgentMasterController.$inject = [ '$location', 'SalesService', 'notify', 'ContactService', 'SalesOrderDetailsService', 'SalesDetailsService', '$confirm'];

	function addAgentMasterController($location, SalesService, notify, ContactService, SalesOrderDetailsService, SalesDetailsService, $confirm ) {
		Logger.logDebug(MODULES.ITEM_SCHEME, "AngularJs - Add Order Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Sales Bill";
			SalesService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'edit';
			vm.isAddScreen = true;
			vm.title = "New Sales Bill";
			SalesService.getById(0).success(successGetById)
				.error(errorFunction);
		}

		vm.validationOptions = {};

		vm.CURRENT_MODULE = "Order Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init = init;
		// contact methods
		vm.setContactData = setContactData;
		vm.loadContactData = loadContactData;
		
		vm.salesDetailsObj = {};
		vm.clearAddSalesDetails = clearAddSalesDetails;
		vm.addSalesDetails = addSalesDetails;
		vm.deleteSalesDetailsMapping = deleteSalesDetailsMapping;
		
		vm.getRemainingQtySalesOrderDetails = getRemainingQtySalesOrderDetails;
		
		vm.orderDate = new Date();
		
		vm.contacts = [];
		vm.contactObj = {};
		
		vm.transactionTypeList = ['In-Ward', 'Out-Ward'];
		
		
		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);
		
		vm.enabledAddDeleteButton = enabledAddDeleteButton;
		vm.disableContactSelection = disableContactSelection;
		vm.loadRemainigQty = loadRemainigQty;

		// call init function to load all the data
		vm.init();
		
		
		function init(){
			vm.loadContactData();
		}
		
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			if(vm.salesObj.contactId <= 0 ){
				vm.dangerNotify("Please select customer");
				return;
			}
			vm.salesObj.orderDate = getFormattedStartDate(vm.orderDate, false);
			// all validations are satisfied
			vm.saveOrUpdate();
		}
		
		function enabledAddDeleteButton(){
			if(!vm.salesObj){
				return false;
			}
			if(!vm.salesObj.contactId){
				return false;
			}
			return vm.salesObj.contactId > 0;
		}

		function getObject() {
			return vm.salesObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return SalesService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/sales");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.salesObj = data.data;
			
			if(vm.screen == 'edit'){
				
				vm.orderDate = vm.salesObj.orderDate; 
				
				//setDefaultObject
				var index;
				vm.contacts = vm.salesObj.salesInitData.contactList;
				if(vm.salesObj.contactId > 0){
					var contact = null;
					for(index = 0 ; index < vm.contacts.length ; index++){
						contact = vm.contacts[index];
						if(contact.id === vm.salesObj.contactId){
							vm.salesObj.contactObj = contact;
							break;
						}
					}
				}
			}
			getRemainingQtySalesOrderDetails(vm.salesObj.contactId, vm.salesObj.desktopSalesOrderId);
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}
		
		/**
		 * 
		 * CONTACTS DATA METHODS
		 * 
		 */
		function loadContactData(){
			var sort = {
					column : 'companyName',
					descending : false
			}
			var reqObj = {
				orderingData : sort,
				paginationData : null,
				"searchValue" : null,
			};
			ContactService.getCurrentOrderExistData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.contacts = data.dataList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setContactData(contactObj){
			vm.contactObj = contactObj;
			vm.salesObj.contactId = contactObj.id;
			vm.loadRemainigQty();
		}
		
		function loadRemainigQty(){
			if(vm.salesObj.contactId <= 0 ){
				vm.dangerNotify("Please select customer");
				return;
			}
			/*if(vm.salesObj.desktopSalesOrderId <= 0 ){
				vm.dangerNotify("Please select order no");
				return;
			}*/
			vm.getRemainingQtySalesOrderDetails(vm.salesObj.contactId, vm.salesObj.desktopSalesOrderId);
		}
		
		function disableContactSelection(){
			if(salesContactDropDownElement){
				salesContactDropDownElement.trigger('chosen:updated');	
			}
			
			if(!vm.salesObj){
				return false;
			}
			if(!vm.salesObj.salesDetailsList){
				return false;
			}
			if(vm.salesObj.salesDetailsList.length > 0){
				return true;
			}
			return false;
		}
		
		function clearAddSalesDetails(){
			vm.salesDetailsObj = {};
		}
		
		function addSalesDetails() {
			//var salesDetailsObj = angular.copy(vm.salesDetailsObj);
			// adding object to purchase item list
			saveSalesOrderDetails();
			//vm.salesObj.salesDetailsList.push(salesDetailsObj);
			//vm.successNotify("Item details added successfiteully");
			//vm.clearAddSalesDetails();
		}
		
		function deleteSalesDetailsMapping(index) {
			$confirm({
				text : 'Are you sure you want to delete?',
				ok : 'Yes',
				cancel : 'No'
			}, {
				size : 'sm',
				templateUrl : getContextPath() + '/directive/confirm-dialog/ConfirmDialog.html'
			}).then(function() {
				deleteSalesOrderDetails(index);
				//vm.salesObj.salesDetailsList.remove(index);
				//vm.successNotify("Item details deleted");
				
			});
		}
		
		/**
		 * 
		 * SALE ORDER ITEMS DATA METHODS
		 * 
		 */
		function getRemainingQtySalesOrderDetails(contactId, desktopSalesOrderId){
			var salesOrderDetails = {
				contactId : contactId,
				desktopSalesOrderId : desktopSalesOrderId
			};
			var reqObj = {
				orderingData : null,
				paginationData : null,
				data : salesOrderDetails
			};
			SalesOrderDetailsService.getRemainingQtySalesOrderDetails(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.remainingSalesOrderDetails = data.dataList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function saveSalesOrderDetails(){
			var salesDetailsObj = angular.copy(vm.salesDetailsObj)
			var salesDetails = {
				salesId  : vm.salesObj.id,
				itemName : salesDetailsObj.itemName,
				quantity : salesDetailsObj.quantity,
				description : salesDetailsObj.description,
				contactId : vm.salesObj.contactId,
				desktopSalesOrderId : vm.salesObj.desktopSalesOrderId
			};
			SalesDetailsService.save(salesDetails)
				.success(function(data, status, headers, config){
					if(data.responseCode !== 2200){
						vm.dangerNotify(data.responseMessage);
						return;
					}
					vm.salesObj.salesDetailsList = data.dataList;
					vm.getRemainingQtySalesOrderDetails(vm.salesObj.contactId, vm.salesObj.desktopSalesOrderId);
					vm.successNotify("Item details added successfully");
					vm.clearAddSalesDetails();
			}).error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function deleteSalesOrderDetails(index){			
			var salesDetailsObj = angular.copy(vm.salesObj.salesDetailsList[index])
			var salesDetails = {
				id: salesDetailsObj.id,
				contactId : vm.salesObj.contactId,
				salesId  : vm.salesObj.id,
				itemName : salesDetailsObj.itemName,
				quantity : salesDetailsObj.quantity,
				description : salesDetailsObj.description,
				desktopSalesOrderId : vm.salesObj.desktopSalesOrderId
			};
			SalesDetailsService.deleteData(salesDetails)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.salesObj.salesDetailsList = data.dataList;
				vm.getRemainingQtySalesOrderDetails(vm.salesObj.contactId, vm.salesObj.desktopSalesOrderId);
				vm.successNotify("Item details deleted");
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
	}
})();

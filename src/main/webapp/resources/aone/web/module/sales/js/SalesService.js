(function() {
	'use strict';
	
	getModule().service('SalesService', getService);
	
	function getService($http) {
		var module = "sales";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		service.getActiveDataWithTotal = getActiveDataWithTotal;
		service.getOrderSaleData = getOrderSaleData;
		service.getOrderQtyData = getOrderQtyData;
		service.getOrderQtyByAgentData= getOrderQtyByAgentData;
		function getActiveDataWithTotal(entityObj) {
			Logger.logDebug(module, "AngularJs - Service > save...");
			var reqData = {};
			reqData.data = entityObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getActiveDataWithTotal');
		}
		
		function getOrderSaleData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getAllowancesData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getOrderSaleData');
		}
		
		function getOrderQtyData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getAllowancesData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getOrderQtyData');
		}
		function getOrderQtyByAgentData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getOrderQtyByAgentData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getOrderQtyByAgentData');
		}
		
		return service;
	}
})();
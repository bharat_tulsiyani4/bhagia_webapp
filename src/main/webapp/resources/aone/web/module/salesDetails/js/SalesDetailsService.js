(function() {
	'use strict';
	
	getModule().service('SalesDetailsService', getService);
	
	function getService($http) {
		var module = "salesDetails";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		service.deleteData = deleteData;
		
		function deleteData(deleteObj) {
			Logger.logDebug(module, "AngularJs - Service > delete...");
			var reqData = {};
			reqData.data = deleteObj;
			return httpPost($http, reqData, service.baseUrl + '/delete');
		}
		return service;
	}
})();
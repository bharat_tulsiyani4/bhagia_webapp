(function() {
	'use strict';

	getModule().controller('ContactController',ContactController);
	
	ContactController.$inject = ['$scope', '$location', '$uibModal', '$confirm', 'ContactService', 'UserService', 'notify'];

	function ContactController($scope, $location, $uibModal, $confirm, ContactService, UserService, notify) {
		Logger.logDebug(MODULES.CONTACT, "AngularJs - Controller Loaded...")

		var vm = this;
		vm.CURRENT_MODULE = MODULES.CONTACT;
		vm.TITLE = "Contact Master";
		vm.contacts = [];
		vm.contactObj = {			
			companyName : ""
		};
		vm.addUpdateUrl = '/contact/addUpdate';
		vm.exportList = [];
		
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.clearSearch = clearSearch;
		
		vm.getScope = getScope;
		vm.getPermissionModule = getPermissionModule; 
		
		vm.loadUserData = loadUserData;
		vm.userList = [];		
		vm.userObj = {};
		vm.setUserData = setUserData;
		
		vm.exportList = [];

		function getScope(){
			return $scope;
		}
				
		function getPermissionModule(){
			return "CONTACT";
		}
		
		vm.getExcelExportList = getExcelExportList;

		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		vm.getSearchObj = getSearchObj;
		vm.getActiveButtonLabel = getActiveButtonLabel;
		vm.getInActiveButtonLabel = getInActiveButtonLabel;
		
		vm.getExcelExportList = getExcelExportList;
		
		
		// call initialization functions
		vm.init = init;
		
		vm.init();
		
		function getSearchObj() {
			var paginationData = {
					"pageNumber" : vm.selectedPage,
					"rowsPerPage" : vm.recordsPerPage
			}
			
			var contactObj = angular.copy(vm.contactObj);
			contactObj.startDate = getFormattedStartDate(vm.contactObj.startDate, false);
			contactObj.endDate = getFormattedStartDate(vm.contactObj.endDate, false);
			if(vm.userObj){
				contactObj.createdBy = vm.userObj.id;
			}
			
			var reqData = {
					orderingData : vm.sort,
					paginationData : paginationData,
					data:contactObj
			};
			return reqData; 
		}
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.contacts = dataList;
		}
		
		function getList(){
			return vm.contacts;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return ContactService;
		}
		
		function getObject() {
			return vm.contactObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
			vm.loadUserData();
		}
		
		function getExcelExportList(){
			vm.showTableLoading = true;
			
			Logger.logDebug(vm.getModule(), "inside getExcelExportList");
			
			var searchObj = vm.getSearchObj();
			
			searchObj.paginationData = null;
			vm.getService().getUnDeletedData(searchObj)
				.success(successExportGetData)
				.error(errorExportFunction);
			
			function successExportGetData(data, status, headers, config) {
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				var index;
				var exportList = [];
				var exportObj;
				var dataList = data.dataList;
				for(index = 0; index < dataList.length; index++){
					exportObj = dataList[index];
					exportObj.agentName = exportObj.agentMaster.name;
					exportObj.addressStreet = exportObj.shopAddress.street 
					exportObj.addressCity = exportObj.shopAddress.city.name 
					exportObj.addressState = exportObj.shopAddress.state.name 
					exportObj.addressCountry = exportObj.shopAddress.country.name;
					exportList.push(exportObj);
				}
				
				vm.exportList = exportList;
				document.getElementById('btnExport').click();
				vm.disableCurrentLoadingButton();
				vm.showTableLoading = false;
			}
			
			function errorExportFunction(data, status, headers, config){
				vm.dangerNotify();
				vm.showTableLoading = false;
				
				vm.disableCurrentLoadingButton();
			}
		}
		
		vm.getExportList = function(){
			return vm.exportList;
		}
		
		function getActiveButtonLabel(){
			return "Mark Complete";
		}
		
		function getInActiveButtonLabel(){
			return "Mark Incomplete";
		}
		
		function loadUserData(){
			var sort = {
					column : 'id',
					descending : false
			}
			
			var reqObj = {
				orderingData : sort,
				paginationData : null
			};
			
			UserService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.userList = data.dataList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setUserData(userObj){
			vm.userObj = userObj;
			vm.selectedPage = 1;
			vm.refreshData();
		}
		
		function clearSearch(){
			vm.contactObj = {};
			vm.userObj = {};
			vm.selectedPage = 1;
			vm.refreshData();
		}
		
		function getExcelExportList(){
			vm.showTableLoading = true;
			
			Logger.logDebug(vm.getModule(), "inside getExcelExportList");
			
			var searchObj = vm.getSearchObj();
			
			searchObj.paginationData = null;
			vm.getService().getUnDeletedData(searchObj)
				.success(successExportGetData)
				.error(errorExportFunction);
			
			function successExportGetData(data, status, headers, config) {
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				var index;
				var exportList = [];
				var exportObj;
				var dataList = data.dataList;
				for(index = 0; index < dataList.length; index++){
					exportObj = dataList[index];
					exportObj.addressStreet = ""; 
					exportObj.addressCity = ""; 
					exportObj.addressState = ""; 
					exportObj.addressCountry = "";
					if(exportObj.shopAddress) {
						if(exportObj.shopAddress.street){
							exportObj.addressStreet = exportObj.shopAddress.street;
						}
						if(exportObj.shopAddress.city){
							exportObj.addressCity = exportObj.shopAddress.city.name; 
						}
						if(exportObj.shopAddress.state){
							exportObj.addressState = exportObj.shopAddress.state.name; 
						}
						if(exportObj.shopAddress.country){
							exportObj.addressCountry = exportObj.shopAddress.country.name;
						}
					}
					exportList.push(exportObj);
				}
				
				vm.exportList = exportList;
				document.getElementById('btnExport').click();
				vm.disableCurrentLoadingButton();
				vm.showTableLoading = false;
			}
			
			function errorExportFunction(data, status, headers, config){
				vm.dangerNotify();
				vm.showTableLoading = false;
				
				vm.disableCurrentLoadingButton();
			}
		}
		
		vm.getExportList = function(){
			return vm.exportList;
		}

	}
	
})();
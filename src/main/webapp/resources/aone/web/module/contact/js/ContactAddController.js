(function() {
	'use strict';

	getModule().controller('AddContactController', addContactController);

	addContactController.$inject = [ '$location', 'ContactService', 'notify', 'CityService', 'StateService', 'CountryService', '$confirm'];

	function addContactController($location, ContactService, notify, CityService, StateService, CountryService, $confirm) {
		Logger.logDebug(MODULES.CONTACT, "AngularJs - Add Contact Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update Contact";
			ContactService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New Contact";
			vm.contactObj = {
				shopAddress:{},
				contactNotesDetailsList : []	
			};
			vm.orderDate = new Date();
		}
		
		vm.validationOptions = {
			rules : {
				mobile1 : {
					required : true,
					maxlength : 10
				},
				mobile2 : {
					required : true,
					maxlength : 10
				},
				fullName : {
					required : true,
					maxlength : 100
				}
			},
			messages : {
				mobile1 : {
					required : MESSAGES.REQUIRED,
					maxlength : "Mobile 1 should be maximum of 10 length..."
				},
				mobile2 : {
					required : MESSAGES.REQUIRED,
					maxlength : "Mobile 1 should be maximum of 10 length..."
				},
				fullName : {
					required : MESSAGES.REQUIRED,
					maxlength : "Full Name should be maximum of 100 length..."
				}
			}
		};
		// city object
		vm.cities = [];
		vm.cityObj = {};
		
		// State object
		vm.states = [];
		vm.stateObj = {};
		
		// country object
		vm.countries = [];
		vm.countryObj = {};

		vm.CURRENT_MODULE = "Contact Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init =  init;
		
		vm.setCityData = setCityData;
		vm.setStateData = setStateData;
		vm.setCountryData = setCountryData;
		
		vm.loadByMobile1Number = loadByMobile1Number;
		vm.setAddressData = setAddressData;
		
		vm.loadInitdata = loadInitdata;
		vm.handleMobile1KeyPress = handleMobile1KeyPress;
		
		vm.clearAll = clearAll;
		
		//contact Notes Details
		vm.clearContactNotesDetailsData = clearContactNotesDetailsData;
		vm.addContactNotesDetailsData = addContactNotesDetailsData;
		vm.deleteContactNotesDetailsData = deleteContactNotesDetailsData;
		
		vm.getPermissionModule = getPermissionModule; 
		
		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);
		
		vm.init();
		
		function getPermissionModule(){
			return "CONTACT";
		}
		
		function init(){
			if(vm.screen === 'add'){
				vm.loadInitdata();
			}
			vm.clearContactNotesDetailsData();
		}
		
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.contactObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return ContactService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/contact");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.contactObj = data.data;
			vm.cities = vm.contactObj.contactInitData.cityList;
			vm.states = vm.contactObj.contactInitData.stateList;
			vm.countries = vm.contactObj.contactInitData.countryList;
			
			vm.setAddressData();
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}
		
		function loadByMobile1Number(){
			var data = {
				mobile1 : vm.contactObj.mobile1
			};
			var reqObj = {
				orderingData : null,
				paginationData : null,
				data : data
			};
			
			getService().getByMobile1Number(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode === 4405){
					vm.dangerNotify("No record found for given mobile number");
					return;
				}
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				
				vm.contactObj = data.data;
				vm.setAddressData();
				vm.screen = 'edit';
				vm.isAddScreen = false;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function loadInitdata(){
			var reqObj = {
				orderingData : null,
				paginationData : null,
				data : null
			};
			getService().initData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.cities = data.data.contactInitData.cityList;
				vm.states = data.data.contactInitData.stateList;
				vm.countries = data.data.contactInitData.countryList;
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
		}
		
		function setAddressData(){
			if(!vm.contactObj.shopAddress){
				return;
			}
			
			var index;
			// set city data
			if(vm.contactObj.shopAddress.cityId > 0){
				var city = null;
				for(index = 0 ; index < vm.cities.length ; index++){
					city = vm.cities[index];
					if(city.id === vm.contactObj.shopAddress.cityId){
						vm.cityObj = city;
					}
				}
			}
			
			// set state data
			if(vm.contactObj.shopAddress.stateId > 0){
				var state = null;
				for(index = 0 ; index < vm.states.length ; index++){
					state = vm.states[index];
					if(state.id === vm.contactObj.shopAddress.stateId){
						vm.stateObj = state;
					}
				}
			}

			// set country data
			if(vm.contactObj.shopAddress.countryId > 0){
				var country = null;
				for(index = 0 ; index < vm.countries.length ; index++){
					country = vm.countries[index];
					if(country.id === vm.contactObj.shopAddress.countryId){
						vm.countryObj = country;
					}
				}
			}
		}
		
		function handleMobile1KeyPress(keyEvent){
			 if (keyEvent.which === 13){
				 vm.loadByMobile1Number();
			 }			
		}
		
		function clearAll(){
			vm.contactObj = {};
			vm.cityObj = null;
			vm.stateObj = null;
			vm.countryObj = null;
		}
		
		
		// contact notes details list 
		function clearContactNotesDetailsData() {
			vm.contactNotesDetailsObj = {
					userName : loginUserInfo.userName,
					notesDate: new Date()
			};
		}
		
		function addContactNotesDetailsData() {
			if(vm.contactNotesDetailsObj && !isEmpty(vm.contactNotesDetailsObj)) {
				var contactNotesDetailsObj = angular.copy(vm.contactNotesDetailsObj); 
				contactNotesDetailsObj.notesDate = getFormattedStartDate(contactNotesDetailsObj.notesDate, false);
				contactNotesDetailsObj.notesDateFormatted = getFormattedDate(contactNotesDetailsObj.notesDate, false);
				vm.contactObj.contactNotesDetailsList.push(contactNotesDetailsObj);
				vm.successNotify("Notes added successfully.");
				vm.clearContactNotesDetailsData();
			} else {
				vm.successNotify("Please add notes details.");
			}		
		}
		
		function isEmpty(obj) {
		    for(var key in obj) {
		        if(obj.hasOwnProperty(key))
		            return false;
		    }
		    return true;
		}
		
		function deleteContactNotesDetailsData(index) {
			$confirm({
				text : 'Are you sure you want to delete?',
				ok : 'Yes',
				cancel : 'No'
			}, {
				size : 'sm',
				templateUrl : getContextPath() + '/directive/confirm-dialog/ConfirmDialog.html'
			}).then(function() {
				var obj = vm.contactObj.contactNotesDetailsList[index];
				vm.contactObj.contactNotesDetailsList.splice(index, 1);
				vm.successNotify("Notes deleted successfully.");
			});	
		}
		
		function setCityData(cityObj){
			vm.cityObj = cityObj;
			vm.contactObj.shopAddress.cityId = cityObj.id;
		}
		
		function setStateData(stateObj){
			vm.stateObj = stateObj;
			vm.contactObj.shopAddress.stateId = stateObj.id;
		}
		
		function setCountryData(countryObj){
			vm.countryObj = countryObj;
			vm.contactObj.shopAddress.countryId = countryObj.id;
		}
	
	}
})();

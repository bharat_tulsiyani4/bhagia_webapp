(function() {
	'use strict';
	
	getModule().service('ContactService', getService);
	
	function getService($http) {
		var module = "contact";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		service.getUnDeletedData = getSearchActiveData;
		service.getActiveData = getActiveData;
		service.getByMobile1Number = getByMobile1Number;
		service.initData = initData;
		service.getCurrentOrderExistData = getCurrentOrderExistData;
		
		function getSearchActiveData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getSearchActiveData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getSearchActiveData');
		}
		
		function getCurrentOrderExistData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getCurrentOrderExistData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getCurrentOrderExistData');
		}
		
		function getActiveData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getActiveData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getActiveData');
		}
		
		function getByMobile1Number(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getByMobile1Number...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getByMobile1Number');
		}
		
		function initData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > initData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/init');
		}
		
		return service;
	}
})();
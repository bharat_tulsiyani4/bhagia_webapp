(function() {
	'use strict';
	
	getModule().service('SalesOrderDetailsService', getService);
	
	function getService($http) {
		var module = "salesOrderDetails";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		service.getRemainingQtySalesOrderDetails = getRemainingQtySalesOrderDetails;
		
		function getRemainingQtySalesOrderDetails(entityObj) {
			Logger.logDebug(module, "AngularJs - Service > getRemainingQtySalesOrderDetails...");
			var reqData = {};
			reqData.data = entityObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getRemainingQtySalesOrderDetails');
		}
		
		return service;
	}
})();
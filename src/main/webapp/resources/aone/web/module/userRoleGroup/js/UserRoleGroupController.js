(function() {
	'use strict';

	getModule().controller('UserRoleGroupController',userRoleGroupController);
	
	userRoleGroupController.$inject = ['$scope', '$location', '$uibModal', '$confirm', 'UserRoleGroupService', 'notify'];

	function userRoleGroupController($scope, $location, $uibModal, $confirm, userRoleGroupService, notify) {
		Logger.logDebug(MODULES.USER_ROLE_GROUP, "AngularJs - Controller Loaded...")

		var vm = this;
		vm.CURRENT_MODULE = MODULES.USER_ROLE_GROUP;
		vm.TITLE = "User Role Group Master";
		vm.userRoleGroupList = [];
		vm.userRoleGroupObj = {			
			parentCategoryId : 0,
			name : "",
			isActive : true
		};
		vm.addUpdateUrl = '/userRoleGroup/addUpdate';
		vm.getAddUpdateUrl = getAddUpdateUrl; 
		vm.getTitle = getTitle;
		vm.setList = setList;
		vm.getList = getList;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getObject = getObject;
		vm.getLocationObj = getLocationObj;
		vm.getConfirm = getConfirm;
		vm.getNotify = getNotify; 
		
		vm.getScope = getScope;
		vm.getPermissionModule = getPermissionModule; 

		function getScope(){
			return $scope;
		}
				
		function getPermissionModule(){
			return "USER_ROLE_GROUP";
		}

		//sets all common method to perform CRUD operations 
		setNotificationData(vm);
		setControllerMethods(vm);
		
		// call initialization functions
		vm.init = init;
		
		vm.init();
		
		function getAddUpdateUrl(){
			return vm.addUpdateUrl;
		}
		
		function getTitle(){
			return vm.TITLE;
		}
		
		function setList(dataList){
			vm.userRoleGroupList = dataList;
		}
		
		function getList(){
			return vm.userRoleGroupList;
		}
		
		function getModule() {
			return vm.CURRENT_MODULE;
		}
		
		function getService(){
			return userRoleGroupService;
		}
		
		function getObject() {
			return vm.userRoleGroupObj;
		}
		
		function getLocationObj(){
			return $location;
		}
		
		function getConfirm(){
			return $confirm;
		}
		
		function getNotify(){
			return notify;
		}
		
		// set initialization functions
		function init() {
			vm.refreshData();
		}
		
	}
	
})();
(function() {
	'use strict';

	getModule().controller('AddUserRoleGroupController', addUserRoleGroupController);

	addUserRoleGroupController.$inject = [ '$location', 'UserRoleGroupService','UserRoleService', 'notify','$rootScope','$filter', '$parse'];

	function addUserRoleGroupController($location, userRoleGroupService,userRoleService, notify, $rootScope, $filter) {
		Logger.logDebug(MODULES.USER_ROLE_GROUP, "AngularJs - Add User Role Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update User Role Group";
			userRoleGroupService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New User Role Group";
			vm.userRoleGroupObj = {};
			vm.userRoleObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				}
			}
		};

		vm.CURRENT_MODULE = "User Role Group Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.loadUserRoleData = loadUserRoleData;
		vm.init = init;
		vm.setUserRoleData = setUserRoleData;
		vm.UserRoleGrouptMappingList = [];
		vm.UserRoleList = [];
		 vm.leftValue = [];
		 vm.leftValueAll = [];
         var leftcounter = 0;
         vm.rightValue = [];
         vm.rightValueAll = [];
         var rightcounter = 0;
         vm.addValue = [];
         vm.removeValue = [];
         var leftValue = angular.copy(vm.leftValue);

         var rightValue = angular.copy(vm.rightValue);
		//sets method to insert and update the object
		vm.init();
		
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);

		function init(){
			if(vm.screen === 'add'){
				vm.loadUserRoleData();
			}
			activate();
		}
		
		function loadUserRoleData(){
			
			
			var sort = {
					column : 'displayName',
					descending : false
			}
			var reqObj = {
					orderingData : sort,
					paginationData : null,
					"searchValue" : null,
			};
			userRoleService.getActiveData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.UserRoleList = data.dataList;
				
				// by default left value contains all the contact list 
				// which will be used in "add" new beat pla group
				var index = 0;
				for(index=0;index<vm.UserRoleList.length;index++){
					vm.UserRoleList[index].displayInListName = vm.UserRoleList[index].displayName;
				}
				
				vm.leftValue = vm.UserRoleList;
				vm.leftValueAll = angular.copy(vm.leftValue);
				vm.rightValueAll = angular.copy(vm.rightValue);		        
				
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
			
		}
		
		function setUserRoleData(contactObj){
			vm.userRoleObj = contactObj;
			vm.salesObj.contactId = contactObj.id;
		}
		
		 

	        function activate() {
	           
	            vm.options = {
	                leftContainerSearch: function (text) {
	                    console.log(text);
	                    vm.leftValue = $filter('filter')(vm.leftValueAll, {
	                        'displayInListName': text
	                    })

	                },
	                rightContainerSearch: function (text) {

	                    vm.rightValue = $filter('filter')(vm.rightValueAll, {
	                        'displayInListName': text
	                    })
	                },
	                leftContainerLabel: 'Available Lists',
	                rightContainerLabel: 'Selected Lists',
	                onMoveRight: function () {
	                    console.log('right');
	                    console.log(vm.addValue);

	                },
	                onMoveLeft: function () {
	                    console.log('left');
	                    console.log(vm.removeValue);
	                }

	            };
	            console.log(vm.options)
	           /* vm.leftValueAll = angular.copy(vm.leftValue)

	            vm.rightValueAll = angular.copy(vm.rightValue)*/

	        }
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			
			var userRoleGroupDetails = [];
			for (var currentcounter = 0; currentcounter < vm.rightValueAll.length; currentcounter++) {
				var temp = {
					userRoleId : vm.rightValueAll[currentcounter].id,
					userRoleGroupId : 0
				}
				userRoleGroupDetails.push(temp);
			}
			
			vm.userRoleGroupObj.userRoleGroupDetails = userRoleGroupDetails;
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.userRoleGroupObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return userRoleGroupService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/userRoleGroup");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.userRoleGroupObj = data.data;
			
			vm.UserRoleList = data.data.userRoleGroupInit.userRoleList;
			
			// by default left value contains all the contact list 
			// which will be used in "add" new beat pla group
			var index = 0;
			for(index=0;index<vm.UserRoleList.length;index++){
				vm.UserRoleList[index].displayInListName = vm.UserRoleList[index].displayName;
			}
			
			if(vm.screen == 'edit'){
				
				var selectedList = vm.userRoleGroupObj.userRoleGroupDetails;
				var contactIdWiseSelectedMap  = {};
				var tempObj = null;
				for (var index = 0; index < selectedList.length; index++) {
					tempObj = selectedList [index];
					contactIdWiseSelectedMap[tempObj.userRoleId] = 1;
				}
				var tempContactObj = null;
				vm.leftValue = [];
				vm.rightValue = [];
				vm.leftValueAll = [];
				vm.rightValueAll = [];
				for (var indexformap = 0; indexformap < vm.UserRoleList.length; indexformap++) {
					tempContactObj = vm.UserRoleList [indexformap];
					if(contactIdWiseSelectedMap[tempContactObj.id] === 1){
						vm.rightValue.push(tempContactObj);
					} else {
						vm.leftValue.push(tempContactObj);
					}
				}

				vm.leftValueAll = angular.copy(vm.leftValue);
		        vm.rightValueAll = angular.copy(vm.rightValue);
		        
			}
			
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

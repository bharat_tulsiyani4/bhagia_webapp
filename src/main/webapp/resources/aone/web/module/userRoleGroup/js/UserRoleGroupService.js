(function() {
	'use strict';
	
	getModule().service('UserRoleGroupService', getService);
	
	function getService($http) {
		var module = "userRoleGroup";
		var service = createServiceObject($http, module);
		/*service.getActiveData = getActiveData;
		
		function getActiveData(searchObj) {
			Logger.logDebug(module, "AngularJs - Service > getActiveData...");
			var reqData = {};
			reqData.data = searchObj;
			reqData.authToken = "-1";
			return httpPost($http, reqData, service.baseUrl + '/getActiveData');
		}*/
		return service;
	}
})();
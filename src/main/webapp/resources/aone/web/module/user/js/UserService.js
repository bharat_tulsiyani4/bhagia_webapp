(function() {
	'use strict';
	
	getModule().service('UserService', getService);
	
	function getService($http) {
		var module = "user";
		var service = createServiceObject($http, module);
		
		// add additional method to service which are specific to this class
		
		return service;
	}
})();
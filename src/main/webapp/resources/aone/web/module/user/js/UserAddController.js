(function() {
	'use strict';

	getModule().controller('AddUserController', addUserController);

	addUserController.$inject = [ '$location', 'UserService', 'notify', 'UserRoleGroupService', '$filter'];

	function addUserController($location, UserService, notify, userRoleGroupService, $filter) {
		Logger.logDebug(MODULES.USER, "AngularJs - Add User Controller Loaded...");

		var urlParams = $location.search();
		var vm = this;
		if (urlParams.screen && urlParams.screen == 'edit' && urlParams.id) {
			vm.screen = 'edit';
			vm.isAddScreen = false;
			vm.title = "Update User";
			UserService.getById(urlParams.id).success(successGetById)
					.error(errorFunction);
		} else {
			vm.screen = 'add';
			vm.isAddScreen = true;
			vm.title = "New User";
			vm.userObj = {};
		}

		vm.validationOptions = {
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
			messages : {
				name : {
					required : MESSAGES.REQUIRED,
					maxlength : "Name should be maximum of 50 length..."
				}
			
			}
		};
		
		vm.CURRENT_MODULE = "User Add/Update";
		vm.getObject = getObject;
		vm.getModule = getModule;
		vm.getService = getService;
		vm.getScreen = getScreen;
		vm.cancel = cancel;
		vm.getTitle = getTitle;
		vm.save = save;
		vm.getNotify = getNotify;
		vm.init =  init;
		
		vm.loadRoleGroups = loadRoleGroups;
		vm.initiateDualList = initiateDualList;
		vm.leftValue = [];
		vm.leftValueAll = [];
        vm.rightValue = [];
        vm.rightValueAll = [];
        vm.addValue = [];
        vm.removeValue = [];
        vm.UserRoleGroupList = [];

		//sets method to insert and update the object
		setNotificationData(vm);
		setAddUpdateControllerMethods(vm);
		
		vm.init();
		
		function init(){
			if(vm.screen === 'add'){
				vm.loadRoleGroups();
			}
			
			vm.initiateDualList();
		}
		
		//load Role Groups of Permissions
		
		function loadRoleGroups(){
			var sort = {
					column : 'id',
					descending : false
			}
			var reqObj = {
					orderingData : sort,
					paginationData : null,
					"searchValue" : null,
			};
			userRoleGroupService.getUnDeletedData(reqObj)
			.success(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
				vm.UserRoleGroupList = data.dataList;
				
				// by default left value contains all the contact list 
				// which will be used in "add" new beat pla group
				var index;
				for(index=0;index<vm.UserRoleGroupList.length;index++){
					vm.UserRoleGroupList[index].displayInListName = vm.UserRoleGroupList[index].name;
				}
				
					vm.leftValue = vm.UserRoleGroupList;
					vm.leftValueAll = angular.copy(vm.leftValue);
					vm.rightValueAll = angular.copy(vm.rightValue);		        
			})
			.error(function(data, status, headers, config){
				if(data.responseCode !== 2200){
					vm.dangerNotify(data.responseMessage);
					return;
				}
			});
			
		
		}
		function initiateDualList(){
            vm.options = {
                leftContainerSearch: function (text) {
                    console.log(text);
                    vm.leftValue = $filter('filter')(vm.leftValueAll, {
                        'displayInListName': text
                    })

                },
                rightContainerSearch: function (text) {

                    vm.rightValue = $filter('filter')(vm.rightValueAll, {
                        'displayInListName': text
                    })
                },
                leftContainerLabel: 'Available Lists',
                rightContainerLabel: 'Selected Lists',
                onMoveRight: function () {
                    console.log('right');
                    console.log(vm.addValue);

                },
                onMoveLeft: function () {
                    console.log('left');
                    console.log(vm.removeValue);
                }

            };
            console.log(vm.options)
		}
		
		//all function declarations	
		function save(form) {
			if (!form.validate()) {
				//TODO: show notification bar here
				return;
			}
			// all validations are satisfied
			//vm.userObj.userName = USER_PREFIX + vm.userObj.userName;
			var userRoleGroupMappingList = [];
			for (var currentcounter = 0; currentcounter < vm.rightValueAll.length; currentcounter++) {
				var temp = {
					userRoleGroupId : vm.rightValueAll[currentcounter].id,
				}
				userRoleGroupMappingList.push(temp);
			}
			
			vm.userObj.userRoleGroupMappingList = userRoleGroupMappingList;
			// all validations are satisfied
			vm.saveOrUpdate();
		}

		function getObject() {
			return vm.userObj;
		}

		function getModule() {
			return vm.CURRENT_MODULE;
		}

		function getService() {
			return UserService;
		}

		function getScreen() {
			return vm.screen;
		}

		function cancel() {
			$location.path("/user");
		}

		function getTitle() {
			return vm.title;
		}

		function getNotify() {
			return notify;
		}

		function successGetById(data, status, headers, config) {
			vm.userObj = data.data;
			/*if(vm.userObj.userName){
				vm.userObj.userName = vm.userObj.userName.replace('user_','');
			}*/
			vm.UserRoleGroupList = vm.userObj.userInit.userRoleGroupList;
			
			// by default left value contains all the contact list 
			// which will be used in "add" new beat pla group
			var index;
			for (index = 0; index < vm.UserRoleGroupList.length; index++) {
				vm.UserRoleGroupList[index].displayInListName = vm.UserRoleGroupList[index].name;
			}
			var selectedList = vm.userObj.userRoleGroupMappingList;
			var contactIdWiseSelectedMap  = {};
			var tempObj = null;
			for (index = 0; index < selectedList.length; index++) {
				tempObj = selectedList [index];
				contactIdWiseSelectedMap[tempObj.userRoleGroupId] = 1;
			}
			var tempContactObj = null;
			vm.leftValue = [];
			vm.rightValue = [];
			vm.leftValueAll = [];
			vm.rightValueAll = [];
			for (var indexformap = 0; indexformap < vm.UserRoleGroupList.length; indexformap++) {
				tempContactObj = vm.UserRoleGroupList [indexformap];
				if(contactIdWiseSelectedMap[tempContactObj.id] === 1){
					vm.rightValue.push(tempContactObj);
				} else {
					vm.leftValue.push(tempContactObj);
				}
			}
			vm.leftValueAll = angular.copy(vm.leftValue);
	        vm.rightValueAll = angular.copy(vm.rightValue);
		}

		function errorFunction(data, status, headers, config) {
			console.log("Error occurred");
		}

	}
})();

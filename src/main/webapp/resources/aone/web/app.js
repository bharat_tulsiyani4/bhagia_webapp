(function() {
	'use strict';
	//angular.module(APP_NAME, ['ngRoute','oc.lazyLoad', 'ui.bootstrap', 'routeResolverServices']).
	getModule().
	config(['$routeProvider', '$ocLazyLoadProvider', '$httpProvider', '$locationProvider', function($routeProvider, $ocLazyLoadProvider, $httpProvider, $locationProvider) {
		
		$httpProvider.interceptors.push('userAuthenticated');
		
		// TODO: make common function to load the JS files
		$ocLazyLoadProvider.config({
			  debug: true
		});
		
		$routeProvider
			.when('/country', {
				templateUrl: getContextModulePath()+'/country/html/ListCountry.html',
				controller : "CountryController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryController.js');
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/country/addUpdate', {
				templateUrl: getContextModulePath()+'/country/html/AddUpdateCountry.html',
				controller : "AddCountryController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/state', {
				templateUrl: getContextModulePath()+'/state/html/ListState.html',
				controller : "StateController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateController.js');
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/state/addUpdate', {
				templateUrl: getContextModulePath()+'/state/html/AddUpdateState.html',
				controller : "AddStateController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateService.js');
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/city', {
				templateUrl: getContextModulePath()+'/city/html/ListCity.html',
				controller : "CityController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/city/js/CityController.js');
	                	allDependencies.push(getContextModulePath()+ '/city/js/CityService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/city/addUpdate', {
				templateUrl: getContextModulePath()+'/city/html/AddUpdateCity.html',
				controller : "AddCityController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/city/js/CityAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/city/js/CityService.js');
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/contact', {
				templateUrl: getContextModulePath()+'/contact/html/ListContact.html',
				controller : "ContactController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactController.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                	allDependencies.push(getContextModulePath()+ '/user/js/UserService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/contact/addUpdate', {
				templateUrl: getContextModulePath()+'/contact/html/AddUpdateContact.html',
				controller : "AddContactController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                	allDependencies.push(getContextModulePath()+ '/city/js/CityService.js');
	                	allDependencies.push(getContextModulePath()+ '/state/js/StateService.js');
	                	allDependencies.push(getContextModulePath()+ '/country/js/CountryService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/user', {
				templateUrl: getContextModulePath()+'/user/html/ListUser.html',
				controller : "UserController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/user/js/UserController.js');
	                	allDependencies.push(getContextModulePath()+ '/user/js/UserService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/user/addUpdate', {
				templateUrl: getContextModulePath()+'/user/html/AddUpdateUser.html',
				controller : "AddUserController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/user/js/UserAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/user/js/UserService.js');
	                	allDependencies.push(getContextModulePath()+ '/userRoleGroup/js/UserRoleGroupService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/userRoleGroup', {
				templateUrl: getContextModulePath()+'/userRoleGroup/html/ListUserRoleGroup.html',
				controller : "UserRoleGroupController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/userRoleGroup/js/UserRoleGroupController.js');
	                	allDependencies.push(getContextModulePath()+ '/userRoleGroup/js/UserRoleGroupService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/userRoleGroup/addUpdate', {
				templateUrl: getContextModulePath()+'/userRoleGroup/html/AddUpdateUserRoleGroup.html',
				controller : "AddUserRoleGroupController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/userRole/js/UserRoleService.js');
	                	allDependencies.push(getContextModulePath()+ '/userRoleGroup/js/UserRoleGroupAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/userRoleGroup/js/UserRoleGroupService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/sales', {
				templateUrl: getContextModulePath()+'/sales/html/ListSales.html',
				controller : "SalesController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/sales/js/SalesController.js');
	                	allDependencies.push(getContextModulePath()+ '/sales/js/SalesService.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/sales/addUpdate', {
				templateUrl: getContextModulePath()+'/sales/html/AddUpdateSales.html',
				controller : "AddSalesController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/sales/js/SalesAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/sales/js/SalesService.js');
	                	allDependencies.push(getContextModulePath()+ '/salesOrderDetails/js/SalesOrderDetailsService.js');
	                	allDependencies.push(getContextModulePath()+ '/salesDetails/js/SalesDetailsService.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/salesOrder', {
				templateUrl: getContextModulePath()+'/salesOrder/html/ListSalesOrder.html',
				controller : "SalesOrderController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/salesOrder/js/SalesOrderController.js');
	                	allDependencies.push(getContextModulePath()+ '/salesOrder/js/SalesOrderService.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.when('/salesOrder/addUpdate', {
				templateUrl: getContextModulePath()+'/salesOrder/html/AddUpdateSalesOrder.html',
				controller : "AddSalesOrderController",
				controllerAs : "vm",
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                	var allDependencies = getCommonDependencies();	       	                                
	                	allDependencies.push(getContextModulePath()+ '/salesOrder/js/SalesOrderAddController.js');
	                	allDependencies.push(getContextModulePath()+ '/salesOrder/js/SalesOrderService.js');
	                	allDependencies.push(getContextModulePath()+ '/contact/js/ContactService.js');
	                    return $ocLazyLoad.load([{
	                    	name: APP_NAME,
	                        files: allDependencies
	                    }]);
	                }]
	            }
			})
			.otherwise({redirectTo:'/contact'});
	}]);
})();

var handlingRouteChangeError = false;

getModule().run(
		function($rootScope, $location) {
			
			/**
			 * Route cancellation: On routing error, go to the dashboard. Provide an
			 * exit clause if it tries to do it twice.
			 */
			$rootScope.$on('$routeChangeError',
					function(event, current, previous, rejection) {
						if (handlingRouteChangeError) {
							return;
						}
						handlingRouteChangeError = true;
						var destination = (current && (current.title
								|| current.name || current.loadedTemplateUrl))
								|| 'unknown target';
						var msg = 'Error routing to ' + destination + '. '
								+ (rejection.msg || '');

						/**
						 * Optionally log using a custom service or $log. (Don't
						 * forget to inject custom service)
						 */
						//logger.warning(msg, [ current ]);

						/**
						 * On routing error, go to another route/state.
						 */
						$location.path('/');

					});
		})

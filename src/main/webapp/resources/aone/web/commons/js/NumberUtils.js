var NumberUtils = {};

NumberUtils.a = [ '', 'ONE ', 'TWO ', 'THREE ', 'FOUR ', 'FIVE ', 'SIX ', 'SEVEN ', 'EIGHT ', 'NINE ', 'TEN ', 'ELEVEN ', 'TWELVE ', 'THIRTEEN ', 'FOURTEEN ', 'FIFTEEN ', 'SIXTEEN ', 'SEVENTEEN ', 'EIGHTEEN ', 'NINETEEN ' ];
NumberUtils.b = [ '', '', 'TWENTY', 'THIRTY', 'FORTY', 'FIFTY', 'SIXTY', 'SEVENTY', 'EIGHTY', 'NINETY' ];

NumberUtils.toWords = function(num) {
	if ((num = num.toString()).length > 9)
		return 'overflow';
	n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
	if (!n)
		return;
	var str = '';
	str += (n[1] != 0) ? (NumberUtils.a[Number(n[1])] || NumberUtils.b[n[1][0]] + ' ' + NumberUtils.a[n[1][1]]) + 'CRORE ' : '';
	str += (n[2] != 0) ? (NumberUtils.a[Number(n[2])] || NumberUtils.b[n[2][0]] + ' ' + NumberUtils.a[n[2][1]]) + 'LAKH ' : '';
	str += (n[3] != 0) ? (NumberUtils.a[Number(n[3])] || NumberUtils.b[n[3][0]] + ' ' + NumberUtils.a[n[3][1]]) + 'THOUSAND ' : '';
	str += (n[4] != 0) ? (NumberUtils.a[Number(n[4])] || NumberUtils.b[n[4][0]] + ' ' + NumberUtils.a[n[4][1]]) + 'HUNDRED ' : '';
	str += (n[5] != 0) ? ((str != '') ? 'AND ' : '') + (NumberUtils.a[Number(n[5])] || NumberUtils.b[n[5][0]] + ' ' + NumberUtils.a[n[5][1]]) + 'ONLY ' : '';
	return str;
}

NumberUtils.toWordsOfDecimal = function(n) {
	var nums = n.toString().split('.');
	var whole = NumberUtils.toWords(nums[0]);
	if (nums.length == 2) {
		var fraction = NumberUtils.toWords(nums[1]);
		return whole + 'AND ' + fraction;
	} else {
		return whole;
	}
}
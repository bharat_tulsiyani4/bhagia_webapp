var LENGTH_DATA = {};
LENGTH_DATA.PASSWORD = 8;
LENGTH_DATA.MIN_LENGTH_PERCENTAGE = 1;
LENGTH_DATA.MAX_LENGTH_PERCENTAGE = 3;

var MESSAGES = {};
MESSAGES.REQUIRED = 'Required';
MESSAGES.MIN = "Minimum value should be ";

function getCommonValidationRules() {

	var validationOptions = {
		rules : {
			name : {
				required : true,
			},
			email : {
				required : true,
				email : true
			},
			password : {
				required : true,
				minlength : LENGTH_DATA.PASSWORD
			},
			percentage : {
				required : true,
				min : 0,
				max : 100,
			}
		},
		messages : {
			name : {
				required : MESSAGES.REQUIRED
			},
			email : {
				required : MESSAGES.REQUIRED,
				email : "Email format should be name@domain.com"
			},
			password : {
				required : MESSAGES.REQUIRED,
				minlength : "Password minimum length of "
						+ LENGTH_DATA.PASSWORD + " characters"
			},
			percentage : {
				required : MESSAGES.REQUIRED,
				min : MESSAGES.MIN + 0,
				max : "Max 100 is aloud..",
			}
		}
	}
	return validationOptions;
}
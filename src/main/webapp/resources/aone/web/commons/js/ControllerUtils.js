/**
 * http://usejsdoc.org/
 */
function setControllerMethods(vm){
	// set variables
	vm.HOME = MODULES.HOME;
	vm.selectedObject = {};
	vm.selectedIndex = -1;
	vm.selectedPage = 1;
	vm.recordsPerPage = 10;
	
	vm.LOADING_BUTTONS_TEXT = {
		ADD : "add",
		REFRESH : "refresh",
		DELETE_SINGLE : "deleteSingle",
		DELETE_ALL: "deleteAll",
		EDIT: "edit",
		TABLE_LOADING : "tableLoading"
	}

	// set methods
	vm.getHome = getHome;
	vm.getAll = getAll;
	vm.getUnDeletedData = getUnDeletedData;
	vm.add = add;
	vm.edit = edit;
	vm.saveOrUpdateAll = saveOrUpdateAll;
	vm.deleteData = deleteData;
	vm.deleteAllSelected = deleteAllSelected;
	vm.searchData = searchData;
	vm.refreshData = refreshData;
	//table check box methods and options  
	vm.isAllSelected = false;
	vm.toggleAll = toggleAll;
	vm.optionToggled = optionToggled;
	//action button methods
	vm.isAnySelected = isAnySelected;
	vm.markActive = markActive;
	vm.markInactive = markInactive;
	vm.checkAddButtonPermission = checkAddButtonPermission;
	vm.checkDeleteButtonPermission = checkDeleteButtonPermission;
	vm.getActiveButtonLabel = getActiveButtonLabel;
	vm.getInActiveButtonLabel = getInActiveButtonLabel;
	
	
	// sorting data
	vm.sort = {
		column : 'id',
		descending : false
	};
	vm.changeSorting = changeSorting;
	vm.getSearchObj = getSearchObj;
	
	
	// loading variables
	vm.getRefreshBtnLoading = getRefreshBtnLoading;
	vm.showAddBtnLoading = false;
	vm.getShowAddBtnLoading = getShowAddBtnLoading;
	vm.currentDeleteId = 0;
	vm.showDeleteSingleBtnLoading = false;
	vm.getShowDeleteSingleBtnLoading = getShowDeleteSingleBtnLoading;
	vm.showDeleteAllBtnLoading = false;
	vm.getDeleteAllBtnLoading = getDeleteAllBtnLoading;
	vm.showEditBtnLoading = false;
	vm.getShowEditBtnLoading = getShowEditBtnLoading;
	vm.showTableLoading = false;
	vm.getShowTableLoading = getShowTableLoading;
	
	// to enable/disable Current Button Loading
	vm.currentLoadingButton = "";
	vm.enableCurrentLoadingButton = enableCurrentLoadingButton;
	vm.disableCurrentLoadingButton = disableCurrentLoadingButton;
	vm.resetAllLoadingButton = resetAllLoadingButton;
	vm.checkTableLoading = checkTableLoading;
	
	function searchData(){
		vm.selectedObject = {};
		vm.selectedIndex = -1;
		vm.selectedPage = 1;
		vm.getUnDeletedData();
	}
	
	function getHome(){
		console.log('inside home');
		return vm.HOME; 
	}
	
	function getAll() {
		vm.showTableLoading = true;
		Logger.logDebug(vm.getModule(), "inside get All");
		vm.selectedObject = {};
		vm.selectedIndex = -1;
		vm.getService().getAll(vm.getSearchObj())
			.success(successGetData)
			.error(errorFunction);
	};
	
	function getUnDeletedData() {
		vm.showTableLoading = true;
		
		$("tr[data-expanded='true']").trigger('click');
		console.log(vm.sort);
		
		vm.selectedObject = {};
		vm.selectedIndex = -1;
		Logger.logDebug(vm.getModule(), "inside get All");
		vm.getService().getUnDeletedData(vm.getSearchObj())
			.success(successGetData)
			.error(errorFunction);
	}
	
	function add() {
		vm.getLocationObj().path(vm.getAddUpdateUrl()).search({screen: 'add'});
		enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.ADD);
		Logger.logDebug(vm.getModule(), "inside add");
		var screen = "Add";
	};
	
	function edit(id) {
		vm.getLocationObj().path(vm.getAddUpdateUrl()).search({screen: 'edit', id: id});
		/*enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.EDIT);
		Logger.logDebug(vm.getModule(), "inside edit:"+object.id);
		var screen = "Edit";
		*/
	};
	
	function saveOrUpdateAll(){
		vm.showTableLoading = true;
	}
	
	function deleteData(id) {
		$confirm = vm.getConfirm();
		$confirm({text: 'Are you sure you want to delete?', ok: 'Yes', cancel: 'No'}, {size: 'sm', templateUrl : getContextPath()+'/directive/confirm-dialog/ConfirmDialog.html'})
        .then(function() {
        	enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.DELETE_SINGLE);
			vm.currentDeleteId = id;
			Logger.logDebug(vm.getModule(), "inside delete: "+id);
			vm.getService().deleteData(id)
				.success(operationSuccess)
				.error(errorFunction);
        });
	};
	
	function refreshData(){
		enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.REFRESH);
		vm.getUnDeletedData();
	}

	function toggleAll() {
		var toggleStatus = vm.isAllSelected;
		angular.forEach(vm.getList(), function(itm) {
			itm.selected = toggleStatus;
		});
	}

	function optionToggled() {
		vm.isAllSelected = vm.getList().every(function(itm) {
			return itm.selected;
		})
	}
	
	function isAnySelected(){
		var obj;
		for(var i = 0; i < vm.getList().length; i++){
			obj = vm.getList()[i];
			if(obj.selected){
				return true;
			}
		}
		return false;
	}
	
	// action button functions
	function markActive(){
		vm.showTableLoading = true;
		Logger.logDebug(vm.getModule(), "inside mark active");
		var updatedDataList = [];
		for(var i = 0; i < vm.getList().length; i++){
			obj = vm.getList()[i];
			if(obj.selected){
				obj.active = true;
				// TODO: change date format for cretedOn and updatedOn
				obj.createdOn = null;
				obj.updatedOn = null;
				updatedDataList.push(obj);
			}
		}
		// TODO: show toaster after operation completion
		vm.getService().saveOrUpdateAll(updatedDataList)
		.success(operationSuccess)
		.error(errorFunction);
	}
	
	function markInactive(){
		vm.showTableLoading = true;
		Logger.logDebug(vm.getModule(), "inside mark inactive");
		var updatedDataList = [];
		for(var i = 0; i < vm.getList().length; i++){
			obj = vm.getList()[i];
			if(obj.selected){
				obj.active = false;
				// TODO: change date format for cretedOn and updatedOn
				obj.createdOn = null;
				obj.updatedOn = null;
				updatedDataList.push(obj);
			}
		}
		// TODO: show toaster after operation completion
		vm.getService().saveOrUpdateAll(updatedDataList)
		.success(operationSuccess)
		.error(errorFunction);
	}
	
	function deleteAllSelected(){
		$confirm = vm.getConfirm();
		$confirm({text: 'Are you sure you want to delete?', ok: 'Yes', cancel: 'No'}, {size: 'sm', templateUrl : getContextPath()+'/directive/confirm-dialog/ConfirmDialog.html'})
        .then(function() {
        	vm.showTableLoading = true;
    		Logger.logDebug(vm.getModule(), "inside delete selected");
    		enableCurrentLoadingButton(vm.LOADING_BUTTONS_TEXT.DELETE_ALL);
    		var updatedDataList = [];
    		for(var i = 0; i < vm.getList().length; i++){
    			obj = vm.getList()[i];
    			if(obj.selected){
    				obj.deleted = true;
    				// TODO: change date format for createdOn and updatedOn
    				obj.createdOn = null;
    				obj.updatedOn = null;
    				updatedDataList.push(obj);
    			}
    		}
    		// TODO: show toaster after operation completion
    		vm.getService().saveOrUpdateAll(updatedDataList)
    		.success(operationSuccess)
    		.error(errorFunction);
        });
	}
	
	function changeSorting(column) {
        var sort = vm.sort;
        if (sort.column == column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        vm.refreshData();
    }
	
	//functions related to loading/processing GUI
	function enableCurrentLoadingButton(loadingButton){
		vm.resetAllLoadingButton();
		
		if(vm.LOADING_BUTTONS_TEXT.REFRESH === loadingButton){
			vm.showRefreshBtnLoading = true;
			vm.currentLoadingButton = vm.LOADING_BUTTONS_TEXT.REFRESH;
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.ADD === loadingButton){
			vm.showAddBtnLoading = true;
			vm.currentLoadingButton = vm.LOADING_BUTTONS_TEXT.ADD;
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.DELETE_SINGLE === loadingButton){
			vm.showDeleteSingleBtnLoading = true;
			vm.currentLoadingButton = vm.LOADING_BUTTONS_TEXT.DELETE_SINGLE;
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.DELETE_ALL === loadingButton){
			vm.showDeleteAllBtnLoading = true;
			vm.currentLoadingButton = vm.LOADING_BUTTONS_TEXT.DELETE_ALL;
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.EDIT === loadingButton){
			vm.showEditBtnLoading = true;
			vm.currentLoadingButton = vm.LOADING_BUTTONS_TEXT.EDIT;
			return;
		}
		
	}
	
	//functions related to loading/processing GUI
	function disableCurrentLoadingButton(){
		if(vm.LOADING_BUTTONS_TEXT.REFRESH === vm.currentLoadingButton){
			vm.showRefreshBtnLoading = false;
			vm.showTableLoading = false;
			vm.currentLoadingButton = "";
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.ADD === vm.currentLoadingButton){
			vm.showAddBtnLoading = false;
			vm.currentLoadingButton = "";
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.DELETE_SINGLE === vm.currentLoadingButton){
			vm.showDeleteSingleBtnLoading = false;
			vm.showTableLoading = false;
			vm.currentLoadingButton = "";
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.DELETE_ALL === vm.currentLoadingButton){
			vm.showDeleteAllBtnLoading = false;
			vm.showTableLoading = false;
			vm.currentLoadingButton = "";
			return;
		}
		
		if(vm.LOADING_BUTTONS_TEXT.EDIT === vm.currentLoadingButton){
			vm.showEditBtnLoading = false;
			vm.currentLoadingButton = "";
			return;
		}
		
	}
	
	function getRefreshBtnLoading(){
		return vm.showRefreshBtnLoading;
	}
	
	function getShowAddBtnLoading(){
		return vm.showAddBtnLoading;
	}
	
	function getShowDeleteSingleBtnLoading(id){
		return vm.showDeleteSingleBtnLoading && vm.currentDeleteId == id;
	}
	
	function getDeleteAllBtnLoading(){
		return vm.showDeleteAllBtnLoading;
	}
	
	function getShowEditBtnLoading(){
		return vm.showEditBtnLoading;
	}
	
	function getShowTableLoading(){
		return vm.showTableLoading;
	}
	
	function resetAllLoadingButton(){
		vm.showRefreshBtnLoading = false;
		vm.showAddBtnLoading = false;
		vm.showDeleteSingleBtnLoading = false;
		vm.showDeleteAllBtnLoading = false;
		vm.showEditBtnLoading = false;
		vm.showTableLoading = false;
	}
	
	function checkTableLoading(){
		if(getShowTableLoading()){
			return "sk-loading"; 
		}
		return "";
	}
	
	function getSearchObj() {
		var paginationData = {
			"pageNumber" : vm.selectedPage,
			"rowsPerPage" : vm.recordsPerPage
		}
		var reqData = {
			orderingData : vm.sort,
			paginationData : paginationData,
			"searchValue" : vm.lotNoSearch,
		};
		return reqData; 
	}

	// HTTP util functions
	function operationSuccess(data, status, headers, config){
		if(data.responseCode !== 2200){
			vm.dangerNotify(data.responseMessage);
			return;
		}
		vm.successNotify();
		vm.getUnDeletedData();
	}
	
	function successGetData(data, status, headers, config) {
		if(data.responseCode !== 2200){
			vm.dangerNotify(data.responseMessage);
			return;
		}
		vm.setList(data.dataList);
		vm.totalRecords = data.totalRecords;
		Logger.logDebug(vm.getModule(), vm.getList());
		disableCurrentLoadingButton();
		vm.showTableLoading = false;
		vm.isAllSelected = false;
	}
	
	function errorFunction(data, status, headers, config){
		vm.dangerNotify();
		disableCurrentLoadingButton();
	}
	
	function checkAddButtonPermission(){
		return vm.getScope().checkModuleButtonPermission(vm.getPermissionModule(), 'SAVE');
	}
	
	function checkDeleteButtonPermission(){
		return vm.getScope().checkModuleButtonPermission(vm.getPermissionModule(), 'DELETE');
	}
	
	function getActiveButtonLabel(){
		return "Mark Active";
	}
	
	function getInActiveButtonLabel(){
		return "Mark Inactive";
	}
}


// add controller methods 
function setAddUpdateControllerMethods(vm){
	vm.saveOrUpdate = saveOrUpdate;
	
	function saveOrUpdate() {
		Logger.logDebug(vm.getModule(), "inside saveOrUpdate:");

		// insert new record
		if (vm.getScreen() == "add") {
			Logger.logDebug(vm.getModule(), "inside add:");
			vm.getService().save(vm.getObject())
				.success(operationSuccess).error(errorFunction);
		}
		
		
		// update current record
		if (vm.getScreen() == "edit") {
			Logger.logDebug(vm.getModule(), "inside update:");
			
			//TODO: remove below code
			vm.getObject().createdOn = null;
			vm.getObject().updatedOn = null;
			
			vm.getService().update(vm.getObject())
				.success(operationSuccess).error(errorFunction);
		}

	};
	
	function operationSuccess(data, status, headers, config){
		if(data.responseCode !== 2200){
			vm.dangerNotify(data.responseMessage);
			return;
		}
		vm.successNotify();
		vm.cancel();
	}
	
	function errorFunction(data, status, headers, config){
		vm.dangerNotify();
		disableCurrentLoadingButton();
	}
}


function setNotificationData(vm){
	
	var notifyObj = vm.getNotify();
	
	notifyObj.config({duration: 2000, templateUrl: 'resources/aone/web/commons/html/notify.html'});
	
	vm.infoNotify = function(message){
		var notify = vm.getNotify();
		if(!message){
			message="Operation can be risky";
		}
		notify({ message: message, classes: 'alert-info'});
	}
	
	vm.successNotify = function(message){
		var notify = vm.getNotify();
		if(!message){
			message="Operation completed successfully";
		}
		notify({ message: message, classes: 'alert-success'});
	}
	
	vm.warningNotify = function(message){
		var notify = vm.getNotify();
		if(!message){
			message="Operation completed with warning";
		}
		notify({ message: message, classes: 'alert-warning'});
	}
	
	vm.dangerNotify = function(message){
		var notify = vm.getNotify();
		if(!message){
			message="Error occurred while performing the operation.";
		}
		notify({ message: message, classes: 'alert-danger'});
	}
	
}




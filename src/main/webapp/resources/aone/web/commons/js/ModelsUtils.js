/**
 * http://usejsdoc.org/
 */
function getLazyLoadModelObject(controller, templateURL, fileList){
	var modelObj = {
			templateUrl: templateURL,
			controller : controller,
			resolve: {
                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                    	name: APP_NAME,
                        files: fileList
                    }]);
                }]
            }
	}; 
	return modelObj;
}

function createLazyLoadModelObject(controller, templateURL, resolve){
	var modelObj = {
			templateUrl: templateURL,
			controller : controller,
			resolve: resolve
	}; 
	return modelObj;
}
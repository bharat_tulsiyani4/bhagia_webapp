/**
 * http://usejsdoc.org/
 */
var APP_NAME = "myApp";

var salesSearchConfiguration;
var loginUserInfo = {};
var USER_PREFIX = "user_";
//set angular 

angular.module(APP_NAME, [ 'ngRoute', 'oc.lazyLoad', 'ui.bootstrap', 'ui.footable', 'ngAnimate', 'angular-ladda', 'ngValidate', 'angular-confirm', 'cgNotify', 'localytics.directives', 'ngGrid', 'datePicker', 'ngJsonExportExcel','ng-duallist'])
.run(function($rootScope) {
	
	/**
	 * =========================================================================================================
	 * User login - starts
	 * =========================================================================================================
	 */
	// Get Angular's $http module.
	var initInjector = angular.injector(['ng']);
	var $http = initInjector.get('$http');

	var entityObj = {};
	var reqData = {};
	reqData.data = entityObj;
	reqData.authToken = "-1";

	$http({
		method :'POST',
		url : 'web/rest/login/getCurrentLoginUserDetails',
		data : reqData
	}).success(successLoginGetData)
	.error(errorLoginFunction);
	// Get user info.

	function successLoginGetData(data, status, headers, config) {
		if(data.responseCode !== 2200){
			alert('Error occured while retrieving user details');
			return;
		}
		
		loginUserInfo.moduleList = [];
		loginUserInfo.roleList = [];
		loginUserInfo.userName = data.data.userName;
		
		var index = 0;
		var userRole;
		var mapTemp = {};
		for(index = 0;  index < data.data.userRoleList.length; index++){
			userRole = data.data.userRoleList[index];
			if(!mapTemp.hasOwnProperty(userRole.moduleName)) {
				loginUserInfo.moduleList.push(userRole.moduleName);
				mapTemp[userRole.moduleName] = 1;
			}
			loginUserInfo.roleList.push(userRole.role);
		}
		
		 $rootScope.getLoginUserName = function() {
			return loginUserInfo.userName;
		 }
		
		 $rootScope.checkModulePermission = function(moduleName) {
				if (!loginUserInfo.moduleList) {
					return false;
				}
			//return loginUserInfo.moduleList.includes(moduleName);
			return true;
         }
		 
		 $rootScope.checkModuleButtonPermission = function(moduleName, operation) {
				if (!loginUserInfo.moduleList) {
					return false;
				}
			var roleName = moduleName + "_" + operation;
			//return loginUserInfo.roleList.includes(roleName);
			return true;
		 }
		 
		 $rootScope.isUserLogin = function() {
			if(loginUserInfo.userName.startsWith(USER_PREFIX)){
				return true;
			}
			return false;
		 }
		 
		getModule().constant('loginUserInfo', loginUserInfo);
		
	}

	function errorLoginFunction(data, status, headers, config){
		//vm.dangerNotify();
		//disableCurrentLoadingButton();
		alert('Error occured while retrieving user details');
	}

	function checkModulePermission(moduleName){
		console.log(moduleName)
		if(!loginUserInfo.moduleList){
			return false;
		}
		return loginUserInfo.moduleList.includes(moduleName);
	}
	/**
	 *#########################################################################################################
	 * User login - ends
	 * ########################################################################################################
	 */
});

	
	/*$http.post('/user-info').then(
		function(success) {

			// Start myAngularApp manually.
			angular.element(document).ready(function() {
				
				angular.bootstrap(document, [APP_NAME]);
			});
		});*/



function getModule() {
	return angular.module(APP_NAME);
}

function getContextPath() {
	return '/bhagiatextileApp/resources/aone/web';
}

function basePath(){
	return 'http://localhost:8080/bhagiatextileApp';
}

function getApplicationLoginURL(){
	return "http://localhost:8080/bhagiatextileApp/login";
}

function getContextModulePath() {
	return getContextPath() + "/module";
}

var salesContactDropDownElement = null;

function getCommonDependencies() {
	return [
			'resources/js/plugins/footable/footable.all.min.js',
			'resources/css/plugins/footable/footable.core.css',
            /*'resources/js/plugins/footable/angular-footable.js',*/
			'resources/js/plugins/ladda/spin.min.js',
			'resources/js/plugins/ladda/ladda.min.js',
			'resources/css/plugins/ladda/ladda-themeless.min.css',
			'resources/js/plugins/ladda/angular-ladda.min.js',
			'resources/css/plugins/iCheck/custom.css',
			'resources/js/plugins/iCheck/icheck.min.js',
			// custom directve
			getContextPath() + '/directive/pagination-directive/PaginationDirective.js',
			getContextPath() + '/directive/header/header-title-directive/HeaderTitleDirective.js',
			getContextPath() + '/directive/header/header-button-directive/HeaderButtonDirective.js',
			getContextPath() + '/directive/OnlyNumDirective.js',
			getContextPath() + '/directive/commons/AlphabateCapitalize.js',
			getContextPath() + '/directive/commons/NumberDirective.js',
			getContextPath() + '/directive/commons/FloatNumberDirective.js',
			getContextPath() + '/directive/commons/TextValidationDirective.js',
			getContextPath() + '/commons/js/NumberUtils.js',
			getContextPath() + '/commons/js/DateUtils.js',
			
			//loading button dependencies
			'resources/js/plugins/ladda/spin.min.js', 
			'resources/js/plugins/ladda/ladda.min.js', 
			'resources/css/plugins/ladda/ladda-themeless.min.css',
			'resources/js/plugins/ladda/angular-ladda.min.js',
			// touch spin
			'resources/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 
			'resources/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js',
			'resources/js/plugins/jasny/jasny-bootstrap.min.js', 
			'resources/css/plugins/jasny/jasny-bootstrap.min.css',
			];
}

/*getModule().directive('fooRepeatDone', function() {
	return function($scope, element) {
		if ($scope.$last) { // all are rendered
			$('.table').trigger('footable_redraw');
		}
	}
});
*/



/*getModule().directive('fooRepeatDone', function(){
	  return function(scope, element){
		  
		if(scope.$last && !$('.footable').hasClass('footable-loaded')) {
			$('.footable-row-detail').remove();  
	        $('.footable').footable();
		}

		var footableObject = $('.footable').data('footable');
		if (footableObject  !== undefined) {
		        footableObject.appendRow($(element));            
		        footableObject.resize();    
		}
	  };
	})*/

/*getModule().directive('fooRepeatDone', function($interpolate) {
    return function($scope, element) {
    	console.log('inside fooRepeatDone');
        if ($scope.$last) { // all are rendered
        	debugger;
        	$('.footable-row-detail').remove();  
        	$(".footable").footable();
        	
            $('.footable').trigger('footable_redraw');
            $('.footable').footable().bind({
                'footable_row_collapsed' : function(e) {
                    //Your code when a row is collapsed
                    // console.log("footable_row_collapsed");
                },
                'footable_row_expanded' : function(e) {
                    //Your code when a row is expanded
                    // console.log('footable_row_expanded');

                    angular.forEach(angular.element('.footable-row-detail-name'), function(value, key){
                        var a = angular.element(value);
                        var t = a[0].innerText;
                        if(t.indexOf('{{') > -1 ){
                            var exp = $interpolate(t);
                            var res = exp($scope);
                            a[0].innerText = res;
                        }
                    });
                },
            });
        }
    }
})*/

getModule().directive("fooRepeatDone", function(){
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
        	console.log('directive called');
            if (scope.$last) {
            	console.log('directive called');
            	scope.$eval(attrs.fooRepeatDone);
            	$('.table').trigger('footable_redraw');
            }
        }
    };
});

// foo table issue and 
// logger configure from john papa

//// exception handling starts
//getModule().config(exceptionConfig);
//
//exceptionConfig.$inject = [ '$provide' ];
//
//function exceptionConfig($provide) {
//	$provide.decorator('$exceptionHandler', extendExceptionHandler);
//}
//
////extendExceptionHandler.$inject = [ '$delegate', 'toastr' ];
//
//function extendExceptionHandler($delegate, toastr) {
//	return function(exception, cause) {
//		$delegate(exception, cause);
//		var errorData = {
//			exception : exception,
//			cause : cause
//		};
//		/**
//		 * Could add the error to a service's collection,
//		 * add errors to $rootScope, log errors to remote web server,
//		 * or log locally. Or throw hard. It is entirely up to you.
//		 * throw exception;
//		 */
//		toastr.error(exception.msg, errorData);
//	};
//}
////exception handling ends

Array.prototype.remove = function(from, to) {
	  var rest = this.slice((to || from) + 1 || this.length);
	  this.length = from < 0 ? this.length + from : from;
	  return this.push.apply(this, rest);
	};
	

	


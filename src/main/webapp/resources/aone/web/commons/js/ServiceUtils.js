/**
 * http://usejsdoc.org/
 */
function createServiceObject($http, module) {

	//var baseUrl = 'rest/' + module;
	var service = {
		save : save,
		getAll : getAll,
		update : update,
		saveOrUpdate : saveOrUpdate,
		saveOrUpdateAll : saveOrUpdateAll,
		deleteData : deleteData,
		getById : getById,
		getUnDeletedData : getUnDeletedData		
	};
	
	service.baseUrl = 'web/rest/' + module;
	
	return service;
	
	function save(entityObj) {
		Logger.logDebug(module, "AngularJs - Service > save...");
		var reqData = {};
		reqData.data = entityObj;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/save');
	}

	function getAll(searchObj) {
		Logger.logDebug(module, "AngularJs - Service > getAll...");
		var reqData = {};
		reqData.data = searchObj;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/getAll');
	}

	function update(entityObj) {
		Logger.logDebug(module, "AngularJs - Service > update...");
		var reqData = {};
		reqData.data = entityObj;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/update');
	}

	function saveOrUpdate(entityObj) {
		Logger.logDebug(module, "AngularJs - Service > saveOrUpdate...");
		var reqData = {};
		reqData.data = entityObj;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/saveOrUpdate');
	}

	// TODO: need to add dataList while sending the request
	function saveOrUpdateAll(dataList) {
		Logger.logDebug(module, "AngularJs - Service > saveOrUpdateAll...");
		var reqData = {};
		reqData.dataList = dataList;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/saveOrUpdateAll');
	}

	function deleteData(deleteId) {
		Logger.logDebug(module, "AngularJs - Service > delete...");
		var reqData = {};
		reqData.data = {};
		reqData.data.id = deleteId;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/delete');
	}

	function getById(id) {
		Logger.logDebug(module, "AngularJs - Service > getById...");
		var reqData = {
			id : id
		};
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/getById');
	}

	function getUnDeletedData(searchObj) {
		Logger.logDebug(module, "AngularJs - Service > getActiveData...");
		var reqData = {};
		reqData.data = searchObj;
		reqData.authToken = "-1";
		return httpPost($http, reqData, service.baseUrl + '/getActiveData');
	}
	
}
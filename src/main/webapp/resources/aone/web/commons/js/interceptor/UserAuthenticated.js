getModule().factory('userAuthenticated', [ '$q', '$injector', '$window', function($q, $injector, $window) {
	var userAuthenticated = {
		request : function(config) {
			return config;
		},

		requestError : function(config) {
			return config;
		},

		response : function(res) {
			return res;
		},

		responseError : function(res) {
			debugger;
			$window.location = getApplicationLoginURL();
			return res;
		}
	};
	return userAuthenticated;
} ]);
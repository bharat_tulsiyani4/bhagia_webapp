/**
 * http://usejsdoc.org/
 */
var Logger = {};
Logger.logLevel = "debug";

Logger.logInfo = function (module, message) {
	console.log(module + ", " + message);
}

Logger.logDebug = function (module, message) {
	console.log(module + ", " + message);
}

Logger.logDebugObject = function (module, data) {
	console.log(module);
	console.log(data);
}
/**
 * http://usejsdoc.org/
 */
var MODULES = {};

MODULES.HOME = "home";

MODULES.COUNTRY = "Country";
MODULES.STATE = "State";
MODULES.CITY = "City";
MODULES.CONTACT = "Contact";
MODULES.USER = "User";
MODULES.USER_ROLE_GROUP = "User Role Group";
MODULES.SALES_ORDER= "Sales Order";
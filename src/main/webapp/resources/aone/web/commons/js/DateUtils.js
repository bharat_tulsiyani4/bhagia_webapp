/**
 * All date related methods
 * @returns
 */

function getFormattedStartDate(dateObj, setTodayDateDefault){
	if(dateObj === undefined || dateObj == null || dateObj == {} || !dateObj){
		if(!setTodayDateDefault){
			return null;
		}
		dateObj = new Date();
	}
	return moment(dateObj).format("YYYY-MM-DDT00:00:00");
}

function getFormattedEndDate(dateObj, setTodayDateDefault){
	if(dateObj === undefined || dateObj == null || dateObj == {} || !dateObj){
		if(!setTodayDateDefault){
			return null;
		} 
		dateObj = new Date();
	}
	return moment(dateObj).format("YYYY-MM-DDT23:59:59");
}

function getDayOfMonth(dateObj, setTodayDateDefault){
	if(dateObj === undefined || dateObj == null || dateObj == {} || !dateObj){
		if(!setTodayDateDefault){
			return null;
		} 
		dateObj = new Date();
	}
	return moment(dateObj).format("D");
}

function getFormattedDate(dateObj, setTodayDateDefault){
	if(dateObj === undefined || dateObj == null || dateObj == {} || !dateObj){
		if(!setTodayDateDefault){
			return null;
		} 
		dateObj = new Date();
	}
	return moment(dateObj).format("DD-MM-YYYY");
}

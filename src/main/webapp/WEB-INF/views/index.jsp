<!--
* INSPINIA - Responsive Admin Theme
* Version 2.7.1
*
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html ng-app="myApp">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Page title set in pageTitle directive -->
<title page-title></title>

<!-- Font awesome -->
<link
	href="<c:url value="resources/font-awesome/css/font-awesome.css" />"
	rel="stylesheet">

<!-- Bootstrap and Fonts -->
<link href="<c:url value="resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- Main Inspinia CSS files -->
<link href="<c:url value="resources/css/animate.css" />" rel="stylesheet">
<link id="loadBefore" href="<c:url value="resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="resources/aone/web/css/custom.css" />" rel="stylesheet">
<link href="<c:url value="resources/css/plugins/angular-notify/angular-notify.min.css" />" rel="stylesheet">
<link href="<c:url value="resources/css/plugins/ladda/ladda-themeless.min.css" />" rel="stylesheet">
<link href="<c:url value="resources/css/plugins/chosen/bootstrap-chosen.css" />" rel="stylesheet">
<link href="<c:url value="resources/css/plugins/daterangepicker/daterangepicker.css" />" rel="stylesheet">
<link href="<c:url value="resources/js/plugins/nggrid/ng-grid.css" />" rel="stylesheet">
<link href="<c:url value="resources/css/plugins/datapicker/angular-datapicker.css" />" rel="stylesheet">

<link href="<c:url value="resources/aone/web/external-lib/duallist/ngduallist.css" />" rel="stylesheet">


</head>

<body class="fixed-sidebar fixed-nav">

	<resolve-loader></resolve-loader>
	<!-- Wrapper-->
	<div id="wrapper">

		<!-- Navigation -->
		<div ng-include="'resources/aone/web/commons/html/navigation.html'"></div>

		<!-- Page wraper -->
		<!-- ng-class with current state name give you the ability to extended customization your view -->
		<div id="page-wrapper" class="gray-bg {{$state.current.name}}">

			<!-- Page wrapper -->
			<div ng-include="'resources/aone/web/commons/html/topnavbar.html'"></div>

			<!-- Main view  -->
			<div ng-view></div>

			<!-- Footer -->
			<div ng-include="'resources/aone/web/commons/html/footer.html'"></div>

		</div>
		<!-- End page wrapper-->

	</div>
	<!-- End wrapper-->

	<!-- jQuery and Bootstrap -->
	<script src="<c:url value="resources/js/jquery/jquery-3.1.1.min.js" />"></script>
	<script src="<c:url value="resources/js/plugins/validate/jquery.validate.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/jquery-ui/jquery-ui.min.js" />"></script>
	<script src="<c:url value="resources/js/bootstrap/bootstrap.min.js" />"></script>

	<!-- MetsiMenu -->
	<script
		src="<c:url value="resources/js/plugins/metisMenu/jquery.metisMenu.js" />"></script>

	<!-- SlimScroll -->
	<script
		src="<c:url value="resources/js/plugins/slimscroll/jquery.slimscroll.min.js" />"></script>

	<!-- Peace JS -->
	<script src="<c:url value="resources/js/plugins/pace/pace.min.js" />"></script>

	<!-- Custom and plugin javascript -->
	<script src="<c:url value="resources/js/inspinia.js" />"></script>

	<!-- Main Angular scripts-->
	
	<script src="<c:url value="resources/js/angular/angular.js" />"></script>
	<script src="<c:url value="resources/aone/web/commons/js/validation/angular-validate.js" />"></script>
	<script src="<c:url value="resources/js/angular/angular-animate.js" />"></script>
	<script src="<c:url value="resources/js/angular/angular-route.js" />"></script>
	<script	src="<c:url value="resources/js/ui-router/angular-ui-router.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/oclazyload/dist/ocLazyLoad.min.js" />"></script>
	<script	src="<c:url value="resources/js/bootstrap/ui-bootstrap-tpls-1.1.2.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/footable/angular-footable.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/ladda/spin.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/ladda/ladda.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/ladda/angular-ladda.min.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/angular-notify/angular-notify.js" />"></script>	
	<script	src="<c:url value="resources/js/plugins/chosen/chosen.jquery.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/chosen/chosen.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/moment/moment.min.js" />"></script>
	<%-- <script	src="<c:url value="resources/js/plugins/daterangepicker/daterangepicker.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/daterangepicker/angular-daterangepicker.js" />"></script> --%>
	
	<script	src="<c:url value="resources/js/plugins/datapicker/angular-datepicker.js" />"></script>
	<script	src="<c:url value="resources/js/plugins/nggrid/ng-grid-2.0.3.min.js" />"></script>
	
	<!-- aone - external libraries -->
	<script src='<c:url value="resources/aone/web/external-lib/FileSaver.js"/>'></script>
	<script src='<c:url value="resources/aone/web/external-lib/json-export-excel.js"/>'></script>
	
	<script src='<c:url value="resources/aone/web/external-lib/duallist/ngduallist.js"/>'></script>
	
	
	
	<!-- Anglar App Script -->
	<script src='<c:url value="resources/aone/web/commons/js/Logger.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/Modules.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/AppUtils.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/HttpUtils.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/ModelsUtils.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/PathUtils.js"/>'></script>
	<script src='<c:url value="resources/aone/web/commons/js/ServiceUtils.js"/>'></script>
	<script	src='<c:url value="resources/aone/web/commons/js/ControllerUtils.js"/>'></script>	  
	<script src='<c:url value="resources/aone/web/commons/js/interceptor/UserAuthenticated.js"/>'></script>
	<script	src='<c:url value="resources/aone/web/directive/spinner-loader-directive/SpinnerLoaderDirective.js"/>'></script>
	<script src='<c:url value="resources/aone/web/directive/confirm-dialog/angular-confirm.js"/>'></script>

	<script src='<c:url value="resources/aone/web/app.js"/>'></script>

	<!-- Anglar App Script -->
	<script src='<c:url value="resources/js/app.js"/>'></script>
	<script src='<c:url value="resources/js/config.js"/>'></script>
	<script src='<c:url value="resources/js/directives.js"/>'></script>
	<script src='<c:url value="resources/js/controllers.js"/>'></script>
	
	
	

	<script	src='<c:url value="resources/aone/web/commons/js/validation/ValidationConfig.js"/>'></script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>A-One | Login</title>

    <link href="<c:url value="resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="resources/font-awesome/css/font-awesome.css" />" rel="stylesheet">

    <link href="<c:url value="resources/css/animate.css" />" rel="stylesheet">
	<link id="loadBefore" href="<c:url value="resources/css/style.css" />" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">A-One TechnoLab</h1>
            </div>
            <h3>A-One TechnoLab</h3>
            <!-- <p>Bom estimate related content
                Continually expanded and constantly improved Inspinia Admin Them (IN+)
            </p> -->
            <c:if test="${param.error != null}">
	            <p>Invalid username and password.</p>
            </c:if>
            <c:url var="loginUrl" value="/login" />
            <form class="m-t" role="form" action="${loginUrl}" method="post">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  />
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<c:url value="resources/js/jquery/jquery-3.1.1.min.js" />"></script>
	<script src="<c:url value="resources/js/bootstrap/bootstrap.min.js" />"></script>

</body>

</html>
